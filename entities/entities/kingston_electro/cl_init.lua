include('shared.lua')

/*---------------------------------------------------------
   Name: Draw
   Desc: Draw it!
---------------------------------------------------------*/
function ENT:Draw()
	local mypos = self:GetPos();
	local dist = LocalPlayer():GetPos():Distance(mypos);
	
	if(dist < 5000) then
	
		self.Entity:DrawModel();
		
		if( LocalPlayer():Alive() and LocalPlayer():IsAdmin() and LocalPlayer():GetActiveWeapon():GetClass() == "weapon_physgun" ) then
		
			render.SetColorMaterial();
			render.DrawSphere( mypos, 200, 50, 50, Color( 255, 0, 0, 100 ) );
		
		end
		
	end
end

function ENT:Initialize()

end

/*---------------------------------------------------------
   Name: IsTranslucent
   Desc: Return whether object is translucent or opaque
---------------------------------------------------------*/
function ENT:IsTranslucent()
	return false
end

function ENT:Think()

	local mypos = self:GetPos()
	local dist = LocalPlayer():GetPos():Distance(mypos)
	
	if(dist < 5000) then

		if !self.ElectroParticleSystem then

			CreateParticleSystem( self.Entity, "electrical_arc_01_system", PATTACH_ABSORIGIN_FOLLOW );
			self.ElectroParticleSystem = true;
			self.ElectroParticleSystemEndTime = CurTime() + 1;

		elseif( self.ElectroParticleSystem and self.ElectroParticleSystemEndTime < CurTime() ) then

			self.ElectroParticleSystem = nil;
			self.ElectroParticleSystemEndTime = nil;
			
		end
		
		for k, v in pairs( ents.FindInSphere( self.Entity:GetPos(), 200 ) ) do	
	
			if( v:IsPlayer() and v:IsValid() and v:GetPos( ):Distance( self:GetPos( ) ) <= 200 ) then
			
				if( !self.NextSoundPlay ) then self.NextSoundPlay = CurTime() end
				if( self.NextSoundPlay > CurTime() ) then return end
				
				local pelvisPos;
				
				if( v:Alive() ) then
				
					pelvisPos = v:GetBonePosition( v:LookupBone( "ValveBiped.Bip01_Pelvis" ) );
					
				else
				
					if( !IsValid( v:GetRagdollEntity() ) ) then return end
				
					pelvisPos = v:GetRagdollEntity():GetBonePosition( v:LookupBone( "ValveBiped.Bip01_Pelvis" ) );
					
				end
				
				util.ParticleTracerEx( "st_elmos_fire", self.Entity:GetPos(), pelvisPos, false, self.Entity:EntIndex(), 0 )
				
				self.NextSoundPlay = CurTime() + 0.5;
			
			end
			
		end
		
	end

end