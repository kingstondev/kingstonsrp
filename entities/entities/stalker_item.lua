AddCSLuaFile();

ENT.Base = "base_anim";

function ENT:Initialize()
	
	if( CLIENT ) then return end
	
	self:SetUseType( SIMPLE_USE );
	
	self:PhysicsInit( SOLID_VPHYSICS );
	self:SetMoveType( MOVETYPE_VPHYSICS );
	self:SetSolid( SOLID_VPHYSICS );
	
	local phys = self:GetPhysicsObject();
	
	if( phys and phys:IsValid() ) then
		
		phys:Wake();
		
	end
	
end

function ENT:SetupDataTables()
	
	self:NetworkVar( "String", 0, "ItemClass" );
	self:NetworkVar( "String", 1, "VarString" );
	
	if( CLIENT ) then return end
	
	self:NetworkVarNotify( "ItemClass", function( self, name, old, new )
		
		local metaitem = GAMEMODE:GetMeta( new );
		
		self:SetModel( metaitem.Model );
		
		local phys = self:GetPhysicsObject();
		if( phys and phys:IsValid() ) then
			
			phys:Wake();
			
		end
		
	end );
	
end

function ENT:SetVar( key, value )

	local s_Vars = util.JSONToTable( self:GetVarString() );
	
	if( s_Vars ) then
	
		s_Vars[key] = value;
		self:SetVarString( util.TableToJSON( s_Vars ) );
		
	end
	
end

function ENT:GetVar( key, fallback )

	local s_Vars = util.JSONToTable( self:GetVarString() );
	
	if( s_Vars[key] ) then
	
		return s_Vars[key];
		
	else
	
		return fallback or false;
		
	end

end

function ENT:GetVars()
	
	return self:GetVarString();
	
end

function ENT:GetItemObject()

	return self.ItemObj;

end

function ENT:Use( ply, caller, type, val )
	
	local metaitem = GAMEMODE:GetMeta( self:GetItemClass() );
	local nX, nY, s_Inventory;
	
	for k, v in next, ply:GetInventories() do
	
		local x,y = v:GetNextAvailableSlot( metaitem.W, metaitem.H );
		
		if( x != -1 or y != -1 ) then
		
			nX,nY = x,y;
			s_Inventory = v;
			break;
			
		end
	
	end
	
	if( !nX or !nY ) then
	
		print("no space");
		return;
		
	end
	
	if( self.ItemObj ) then
	
		if( self.ItemObj.CanPickup ) then
		
			local ret = self.ItemObj:CanPickup( ply, self );
			if( !ret ) then
			
				return;
			
			end
			
		end
		
	end
	
	local ret = hook.Run( "CanPickup", ply, self.ItemObj, self );
	if( !ret ) then
	
		return;
		
	end
	
	if( !self.Used ) then
		
		self.Used = true;

		if( !self.ItemObj ) then
		
			self.ItemObj = s_Inventory:GiveItem( self:GetItemClass(), nX, nY, metaitem.Vars );
		
		else
		
			self.ItemObj.X = nX;
			self.ItemObj.Y = nY;
			self.ItemObj.owner = ply;
			self.ItemObj.InvID = s_Inventory:GetID();
			self.ItemObj.InvObj = s_Inventory;
			self.ItemObj:UpdateSave();
			self.ItemObj:TransmitToOwner();
			s_Inventory:GetContents()[#s_Inventory:GetContents() + 1] = self.ItemObj;
			if( self.ItemObj:GetChildInventory() ) then
				
				self.ItemObj:GetChildInventory().owner = ply;
				self.ItemObj:GetChildInventory().CharID = ply:CharID();
				self.ItemObj:GetChildInventory():UpdateSave();
				self.ItemObj:GetChildInventory():TransmitToOwner()
				for k,v in next, self.ItemObj:GetChildInventory():GetContents() do
				
					v.owner = ply;
					v:TransmitToOwner();
				
				end
				
			end
			
		end
		hook.Run( "ItemPickedUp", ply, self.ItemObj );
		self:Remove();
		
	end
	
end