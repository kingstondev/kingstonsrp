local s_Meta = FindMetaTable( "Player" );

netstream.Hook( "CreateCharacter",  function( ply, ... ) -- refactor please stop using hardcoded fields
	
	printf( COLOR_WHITE, "CreateCharacter received; CreateCharacter calling serverside." );
	
	ply:CreateCharacter( ... );
	
end )

netstream.Hook( "LoadCharacter", function( ply, key )

	ply:LoadCharacter( key );

end );

function s_Meta:CreateCharacter( szName, szOverhead1, szOverhead2, szDesc, szModel, szHead ) -- this could probably be redone. nutscript has a way of registering new vars for the character. might copy

	printf( COLOR_WHITE, "CreateCharacter has been called." );

	local ply = self;
	
	if( !szName ) then return end;
	if( !szOverhead1 or #szOverhead1 > 64 ) then return end;
	if( !szOverhead2 or #szOverhead2 > 64 ) then return end;
	if( !szDesc or #szDesc > 6500 ) then return end;
	if( !szModel ) then return end;
	if( !szHead ) then return end;
 
	local CreateCharacterQuery = SAVE_NEW_CHARACTER;
	function CreateCharacterQuery:onSuccess( data )

		printf( COLOR_WHITE, "Query Succeeded." );

		local s_nID = self:lastInsert();
		local CharacterTable = {};
		CharacterTable["SteamID"] = ply:SteamID();
		CharacterTable["RPName"] = szName;
		CharacterTable["Overhead1"] = szOverhead1;
		CharacterTable["Overhead2"] = szOverhead2;
		CharacterTable["Model"] = szModel;
		CharacterTable["Head"] = szHead;
		CharacterTable["Description"] = szDesc;
		CharacterTable["CharFlags"] = "";
		CharacterTable["Faction"] = "loner";
		CharacterTable["Disabled"] = false;
		CharacterTable["id"] = s_nID;
		
		table.insert( GAMEMODE.g_CharacterTable[ply:SteamID()], CharacterTable );
		
		printf( COLOR_WHITE, "Saved new character %s.", szName );
		
		netstream.Start( ply, "LoadIndividualCharacter", GAMEMODE.g_CharacterTable[ply:SteamID()] );
		
		ply:LoadCharacter( s_nID );

	end
	function CreateCharacterQuery:onError( err )

		print( "Something has gone wrong in our query! "..err );

	end
	
	CreateCharacterQuery:setString( 1, ply:SteamID() );
	CreateCharacterQuery:setString( 2, szName );
	CreateCharacterQuery:setString( 3, szOverhead1 );
	CreateCharacterQuery:setString( 4, szOverhead2 );
	CreateCharacterQuery:setString( 5, szModel );
	CreateCharacterQuery:setString( 6, szHead );
	CreateCharacterQuery:setString( 7, szDesc );
	CreateCharacterQuery:start();
	
end

function s_Meta:LoadCharacter( nID )

	local CharacterData = self:GetCharDataByID( nID );

	if( tobool( CharacterData.Disabled ) ) then return end
	
	self:SetCharID( nID );
	self:LoadCharacterData( CharacterData );
	self:LoadInventoryData( nID );
	self:PostLoadCharacter()

end

function s_Meta:LoadCharacterData( CharacterTable )

	self:SetRPName( CharacterTable.RPName );
	self:SetOverhead1( CharacterTable.Overhead1 );
	self:SetOverhead2( CharacterTable.Overhead2 );
	self:SetDescription( CharacterTable.Description );
	self:SetHead( CharacterTable.Head );
	self:SetCharFlags( CharacterTable.CharFlags );
	CharacterTable.Disabled = tobool( CharacterTable.Disabled );

end

function s_Meta:LoadInventoryData( nID )

	self:RetrieveInventory( nID );

end

function s_Meta:UpdateCharacter( nID ) -- arg is prolly redundant.

	if( self:CharID() > -1 and self:CharID() == nID ) then
	
		local CharacterData = self:GetCharDataByID( nID );
	
		local CharacterUpdateQuery = UPDATE_CHARACTER;
		function CharacterUpdateQuery:onSuccess()
		
			printf( COLOR_WHITE, "Successfully updated character." );
			
		end
		
		CharacterUpdateQuery:setString( 1, self:RPName() );
		CharacterUpdateQuery:setString( 2, self:Description() );
		CharacterUpdateQuery:setString( 3, self:Overhead1() );
		CharacterUpdateQuery:setString( 4, self:Overhead2() );
		CharacterUpdateQuery:setString( 5, self:Head() );
		CharacterUpdateQuery:setNumber( 6, BoolToNumber( CharacterData.Disabled ) );
		CharacterUpdateQuery:setNumber( 7, self:CharID() ); 
		CharacterUpdateQuery:start();
	
	end

end

function s_Meta:UpdateCharacterByField( szField, szValue )

	if( self:CharID() < 1 ) then return end;

	local CharacterUpdateQuery = UPDATE_CHARACTER_BY_FIELD;
	function CharacterUpdateQuery:onSuccess()
	
		printf( COLOR_WHITE, "Successfully updated character field %s with value %s.", szField, szValue );
		
	end
	
	CharacterUpdateQuery:setString( 1, szField );
	CharacterUpdateQuery:setString( 2, szValue );
	CharacterUpdateQuery:setNumber( 3, self:CharID() );
	CharacterUpdateQuery:start();

end

function s_Meta:UnloadCharacter() -- really ghetto huh

	if( self:CharID() > -1 ) then
	
		for k,v in next, GAMEMODE.g_InventoryTable do
		
			if( v.owner == self ) then
			
				v.owner = nil;
				
			end
			
		end
		
		for k,v in next, GAMEMODE.g_ItemTable do
		
			if( v.owner == self ) then
			
				v.owner = nil;
				
			end
			
		end
		
		local CharacterData = GAMEMODE.g_CharacterTable[self:SteamID()];
		
		self:SetCharID( -1 );
		self:SetRPName( "..." );
		self:SetOverhead1( "..." );
		self:SetOverhead2( "..." );
		self:SetDescription( "..." );
		self:SetHead( "" );
		self:SetCharFlags( "" );
		
		netstream.Start( self, "LoadCharacters", CharacterData );
		netstream.Start( self, "UnloadCharacter" );
		
		self.InitialSafeSpawn = false;
		self:Spawn();
		
	end

end

function GM:FindCharacter( nCharID, callback )

	for szSteamID,player in next, GAMEMODE.g_CharacterTable do
	
		for nIndex,character in next, player do
			
			if( character.id == nCharID ) then
			
				if( callback ) then
				
					callback( szSteamID, nIndex );
				
				end
				return szSteamID,nIndex;
			
			end
		
		end
	
	end

end

netstream.Hook( "ChangeName", function( ply, szName )

	if( #szName <= 32 ) then
	
		ply:SetRPName( szName );
		ply:UpdateCharacter( ply:CharID() );
		
	end

end );

netstream.Hook( "ChangeTitle1", function( ply, szTitle )

	if( #szTitle <= 64 ) then
	
		ply:SetOverhead1( szTitle );
		ply:UpdateCharacter( ply:CharID() );
	
	end
	
end );

netstream.Hook( "ChangeTitle2", function( ply, szTitle )

	if( #szTitle <= 64 ) then
	
		ply:SetOverhead2( szTitle );
		ply:UpdateCharacter( ply:CharID() );
	
	end
	
end );

netstream.Hook( "ChangeDescription", function( ply, szDesc )

	if( #szDesc <= 6500 ) then
	
		ply:SetDescription( szDesc );
		ply:UpdateCharacter( ply:CharID() );
		
	end

end );