AddCSLuaFile();

local s_Meta = FindMetaTable( "Entity" );

function s_Meta:SaveNewEntity()

	if( self:IsPlayer() ) then return end
	
	local id = #GAMEMODE.SavedEntitiesCache + 1;
	
	GAMEMODE.SavedEntitiesCache[id] = {
	
		["model"] = self:GetModel(),
		["pos"] = self:GetPos(),
		["angs"] = self:GetAngles(),
		["isfrozen"] = self:GetPhysicsObject():IsMotionEnabled(),
		
	}
	
	if( self:GetInventory() ) then
	
		GAMEMODE.SavedEntitiesCache[id].InvID = self:GetInventory():GetID();
	
	end
	
	GAMEMODE:SaveEntityFile();

end

GM.SavedEntitiesCache = GM.SavedEntitiesCache or {};

function GM:OnLoadedEntities()

end

function GM:OnSavedEntities()

end

function GM:SaveEntityFile()
	
	file.Write( "stalker/entities.txt", pon.encode( self.SavedEntitiesCache ) );
	
	hook.Run( "OnSavedEntities" );

end

function GM:LoadEntityFile()

	local str = file.Read( "stalker/entities.txt", "DATA" );
	self.SavedEntitiesCache = pon.decode( str );
	
	for k,v in next, self.SavedEntitiesCache do
	
		local ent = ents.Create( "prop_physics" );
		ent:SetModel( v.model );
		ent:SetPos( v.pos );
		ent:SetAngles( v.angs );
		
		local phys = self:GetPhysicsObject();
		if( phys and phys:IsValid() ) then
			
			phys:Wake();
			phys:EnableMotion( v.isfrozen );
			
		end
		
		if( v.InvID ) then
		
			ent:LoadInventory( v.InvID );
			
		end
	
	end

	hook.Run( "OnLoadedEntities" );

end