GM.Bans = { };

-- trigger refresh

function GM:LoadBans()
	
	local function onSuccess( result )
		
		for _, v in next, result do
			
			GAMEMODE.Bans[v.SteamID] = { };
			GAMEMODE.Bans[v.SteamID].Date = v.Date;
			GAMEMODE.Bans[v.SteamID].Length = v.Length;
			GAMEMODE.Bans[v.SteamID].Reason = v.Reason;
			GAMEMODE.Bans[v.SteamID].Unban = v.Unban;
			
		end
		
	end
	
	wzsql.Query( "SELECT * FROM Bans", onSuccess );
	
end

function GM:AddBan( sid, duration, reason )
	
	self.Bans[sid] = { };
	self.Bans[sid].Date = os.time();
	self.Bans[sid].Length = duration;
	self.Bans[sid].Reason = reason;
	self.Bans[sid].Unban = 0;
	
	local function onSuccess( result )
		
		MsgC( Color( 128, 128, 128, 255 ), "Saved player ban for SteamID " .. sid .. ".\n" );
		
	end
	
	MsgC( Color( 128, 128, 128, 255 ), "Saving player ban for SteamID " .. sid .. "...\n" );
	wzsql.Query( "INSERT INTO Bans ( SteamID, Length, Reason, Date, Unban ) VALUES ( '" .. sid .. "', '" .. duration .. "', '" .. reason .. "', '" .. os.time() .. "', '0' )", onSuccess );
	
end

function GM:RemoveBan( sid )

	local function onSuccess( result )
	end
	wzsql.Query( Format( "UPDATE Bans SET Unban = 1 WHERE SteamID = '%s'", sid ), onSuccess );

end

function GM:LoadPermissions()

	local szJSON = file.Read( "stalker/ranks.txt", "DATA" );
	
	if( szJSON and #szJSON > 0 ) then
	
		local tbl = util.JSONToTable( szJSON );
		if( table.Count( tbl ) > 0 ) then
		
			self.Ranks = tbl;
			
		else
		
			szJSON = file.Read( "stalker/defaultranks.txt", "DATA" );
			local tbl = util.JSONToTable( szJSON );
			
			self.Ranks = tbl;
			
		end
		
	end
	
	-- add logging

end

function GM:SavePermissions()

	local szJSON = util.TableToJSON( self.Ranks );
	
	file.Write( "stalker/ranks.txt", szJSON );
	netstream.Start( nil, "LoadPermissions", self.Ranks ); -- broadcast change
	
	-- add logging

end

local s_Meta = FindMetaTable( "Player" );
function s_Meta:SetRank( szRank )

	if( !GAMEMODE.Ranks[szRank] ) then return end

	self:SetUserGroup( szRank );
	self:UpdatePlayerField( "Rank", szRank );

end

netstream.Hook( "RetrievePermissions", function( ply )

	netstream.Start( ply, "LoadPermissions", GAMEMODE.Ranks );

end );

netstream.Hook( "RetrieveCommands", function( ply )

	netstream.Start( ply, "LoadCommands", GAMEMODE.Commands );

end );

netstream.Hook( "RequestCharacterInfo", function( ply, page )

	if( !isnumber( page ) ) then return end
	if( page < 1 ) then return end
	if( !ply:IsAdmin() ) then return end
	
	local function onSuccess( data )
	
		local tbl = {};
		for k,v in next, data do
			
			tbl[v.id] = {
				id = v.id,
				SteamID = v.SteamID,
				RPName = v.RPName,
				CharFlags = v.CharFlags,
				Disabled = v.Disabled,
				Faction = v.Faction,
			};
			
		end
	
		netstream.Start( ply, "ReceiveCharacterInfo", tbl );
	
	end
	wzsql.Query( Format( "SELECT * FROM Characters LIMIT %d, %d", ( page - 1 ) * 30, page * 30 ), onSuccess );

end );

netstream.Hook( "RequestInventoryInfo", function( ply, id )

	if( !isnumber( id ) ) then return end
	if( id < 1 ) then return end
	if( !ply:IsAdmin() ) then return end
	
	local function onSuccess( data )

		local tbl = {};
		for k,v in next, data do
			
			tbl[v.id] = {
				id = v.id,
				Name = v.Name,
				w = v.w,
				h = v.h,
				ItemID = v.ItemID,
			};
			
		end
	
		netstream.Start( ply, "ReceiveInventoryInfo", tbl );
	
	end
	wzsql.Query( Format( "SELECT * FROM Inventories WHERE CharID = %d", id ), onSuccess );

end );

function GM:CheckPassword( szSteamID64, szIPAddress, szPassword, szGivenPassword, szPlayerName )

	self:Log( "join", "", Format( "Player %s (%s) is attempting to join.", szPlayerName, szIPAddress ) );
	printf( COLOR_WHITE, "Player %s (%s) is attempting to join.", szPlayerName, szIPAddress );
	
	if( self.Bans ) then
		
		for k,v in next, self.Bans do
			
			if( v.Date + v.Length < os.time() ) then
			
				self.Bans[k].Unban = 1;
				self:RemoveBan( k );
				
			end
			
			if( v.Unban == 0 and util.SteamIDTo64( k ) == szSteamID64 ) then
				
				if( tonumber( v.Length ) == 0 ) then
				
					self:Log( "join", "", Format( "Player %s (%s) has been rejected: Player is permabanned.", szPlayerName, szIPAddress ) );
					printf( COLOR_WHITE, "Player %s (%s) has been rejected: Player is permabanned.", szPlayerName, szIPAddress );

					return false, "You've been permabanned: " .. v.Reason;
					
				else
				
					self:Log( "join", "", Format( "Player %s (%s) has been rejected: Player is banned.", szPlayerName, szIPAddress ) );
					printf( COLOR_WHITE, "Player %s (%s) has been rejected: Player is banned.", szPlayerName, szIPAddress );
					
					return false, "You're banned for " .. math.Round( ( ( v.Date + v.Length ) - os.time() ) / 60 ) .. " more minutes: " .. v.Reason;
					
				end
				
			end
			
		end
		
	end

	if( szPassword != "" ) then
		
		if( szGivenPassword != szPassword ) then
		
			self:Log( "join", "", Format( "Player %s (%s) has been rejected: Bad password. Given Password: %s", szPlayerName, szIPAddress, szGivenPassword ) );
			printf( COLOR_WHITE, "Player %s (%s) has been rejected: Bad password. Given Password: %s", szPlayerName, szIPAddress, szGivenPassword );
			
			return false;
			
		end
		
	end
	
	self:Log( "join", "", Format( "Player %s (%s) has successfully joined.", szPlayerName, szIPAddress ) );
	printf( COLOR_WHITE, "Player %s (%s) has successfully joined.", szPlayerName, szIPAddress );
	
	return true;

end

local function fnKick( ply, szCmd, args )

	if( #args < 1 ) then
		
		ply:Notify( COLOR_WHITE, "You have not provided a target." );
		return;
		
	end
	
	local targ = ply:FindPlayer( args[1] );
	local reason = args[2] or "";
	
	if( targ and targ:IsValid() ) then
		
		ply:Notify( Color( 255, 255, 255 ), "Player %s has been kicked.", targ:GetFormattedName() );
		
		GAMEMODE:Log( "admin", "K", ply:Nick() .. " kicked " .. targ:Nick() .. " (" .. reason .. ").", ply );
		
		if( reason == "" ) then
			
			targ:Kick( "Kicked by " .. ply:GetFormattedName() );
			
		else
			
			targ:Kick( "Kicked by " .. ply:GetFormattedName() .. ": " .. reason );
			
		end
		
	else
		
		ply:Notify( COLOR_WHITE, "Target not found." );
		
	end

end
concommand.AddAdmin( "rpa_kick", fnKick );

local function fnBan( ply, cmd, args )
	
	if( #args == 0 ) then
		
		ply:Notify( COLOR_WHITE, "No target specified." );
		return;
		
	end
	
	if( #args == 1 ) then
		
		ply:Notify( COLOR_WHITE, "No duration specified." );
		return;
		
	end
	
	if( tonumber( args[2] ) < 0 ) then
		
		ply:Notify( COLOR_WHITE, "Negative duration is not valid." );
		return;
		
	end
	
	local targ = ply:FindPlayer( args[1] );
	local reason;
	reason = "Banned for " .. args[2] .. " minutes by " .. ply:Nick();
	
	if( args[3] ) then
		
		reason = "Banned for " .. args[2] .. " minutes by " .. ply:Nick() .. " (" .. args[3] .. ")";
		
		if( args[2] == "0" ) then
			
			reason = "Permabanned by " .. ply:Nick() .. " (" .. args[3] .. ")";
			
		end
		
	end
	
	local reasonin = args[3] or "";
	
	if( targ and targ:IsValid() ) then
		
		if( !targ:IsBot() ) then
			
			GAMEMODE:AddBan( targ:SteamID(), args[2] * 60, reasonin, os.date( "!%m/%d/%y %H:%M:%S" ) );
			
		end
		
		targ:Kick( reason );
		
	elseif( string.find( args[1], "STEAM_" ) ) then
		
		GAMEMODE:AddBan( args[1], args[2], reasonin, os.date( "!%m/%d/%y %H:%M:%S" ) );
		
	end
	
end
concommand.AddAdmin( "rpa_ban", fnBan );

local function fnGoto( ply, cmd, args )
	
	if( #args < 1 ) then
		
		ply:Notify( COLOR_WHITE, "You have not provided a target." );
		return;
		
	end
	
	local targ = ply:FindPlayer( args[1] );
	
	if( targ and targ:IsValid() ) then
		
		ply:SetPos( targ:GetPos() + Vector( 0, 0, 64 ) );
		ply:Notify( COLOR_WHITE, "You've gone to %s.", targ:GetFormattedName() );
		
	else
		
		ply:Notify( COLOR_WHITE, "Target not found." );
		
	end
	
end
concommand.AddAdmin( "rpa_goto", fnGoto );

local function fnBring( ply, cmd, args )
	
	if( #args < 1 ) then
		
		ply:Notify( COLOR_WHITE, "You have not provided a target." );
		return;
		
	end
	
	local targ = ply:FindPlayer( args[1] );
	
	if( targ and targ:IsValid() ) then
		
		ply:Notify( COLOR_WHITE, "You've brought %s.", targ:GetFormattedName() );
		targ:SetPos( ply:GetPos() + Vector( ply:GetAimVector().x, ply:GetAimVector().y, 0 ) * 50 );
		
	else
		
		ply:Notify( COLOR_WHITE, "Target not found." );
		
	end
	
end
concommand.AddAdmin( "rpa_bring", fnBring );

local GoodTraceVectors = {
	Vector( 40, 0, 0 ),
	Vector( -40, 0, 0 ),
	Vector( 0, 40, 0 ),
	Vector( 0, -40, 0 ),
	Vector( 0, 0, 40 )
}

local function FindGoodTeleportPos( ply ) -- combinecontrol code
	
	local trace = {}
	trace.start = ply:GetShootPos()
	trace.endpos = trace.start + ply:GetAimVector() * 50;
	trace.mins = Vector( -16, -16, 0 )
	trace.maxs = Vector( 16, 16, 72 )
	trace.filter = ply;
	local tr = util.TraceHull( trace )
	
	if( !tr.Hit ) then
		
		return tr.HitPos;
		
	end
	
	local pos = ply:GetPos()
	
	for _, v in pairs( GoodTraceVectors ) do
		
		local trace = {}
		trace.start = ply:GetPos()
		trace.endpos = trace.start + v;
		trace.mins = Vector( -16, -16, 0 )
		trace.maxs = Vector( 16, 16, 72 )
		trace.filter = ply;
		local tr = util.TraceHull( trace )
		
		if( tr.Fraction == 1.0 ) then
			
			pos = ply:GetPos() + v;
			break;
			
		end
		
	end
	
	return pos;
	
end

local function fnSend( ply, cmd, args )
	
	if( #args < 1 ) then
		
		ply:Notify( COLOR_WHITE, "You have not provided a target." );
		return;
		
	end
	
	if( #args < 2 ) then
	
		ply:Notify( COLOR_WHITE, "You have not provided a second argument." );
		return;
		
	end
	
	local targ = ply:FindPlayer( args[1] );
	local destination = ply:FindPlayer( args[2] );
	
	if( targ and targ:IsValid() and destination and destination:IsValid() ) then
		
		ply:Notify( COLOR_WHITE, "You've sent %s to %s.", targ:GetFormattedName(), destination:GetFormattedName() );
		targ:SetPos( FindGoodTeleportPos( destination ) );
		
	else
		
		ply:Notify( COLOR_WHITE, "Target/Destination not found." );
		
	end
	
end
concommand.AddAdmin( "rpa_send", fnSend );

local function fnSetRank( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You have no target." );
		return;
	
	end
	
	if( #args < 2 ) then
	
		ply:Notify( COLOR_WHITE, "You have no rank argument." );
		return;
		
	end
	
	if( !GAMEMODE.Ranks[args[2]] ) then
	
		ply:Notify( COLOR_WHITE, "Invalid rank specified." );
		return;
		
	end
	
	local target = ply:FindPlayer( args[1] );
	if( target ) then
	
		ply:SetRank( args[2] );
		ply:Notify( COLOR_WHITE, "%s has had their rank set to %s", target:GetFormattedName(), args[2] );
	
	else
	
		ply:Notify( COLOR_WHITE, "Invalid target/target not found." );
		
	end

end
concommand.AddAdmin( "rpa_setrank", fnSetRank );

local function fnSetCharName( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You have no target." );
		return;
	
	end
	
	if( #args < 2 ) then
	
		ply:Notify( COLOR_WHITE, "You have no name specified." );
		return;
		
	end
	
	if( #args[2] < 3 ) then
	
		ply:Notify( COLOR_WHITE, "Name is not long enough." );
		return;
		
	end
	
	local targ = ply:FindPlayer( args[1] );
	if( targ ) then
	
		local OldName = targ:GetFormattedName();
		targ:SetRPName( args[2] );
		targ:UpdateCharacter( targ:CharID() );
		ply:Notify( COLOR_WHITE, "%s has had their name set to %s", OldName, args[2] );
		
	else
	
		ply:Notify( COLOR_WHITE, "Target is not valid." );
		return;
		
	end

end
concommand.AddAdmin( "rpa_setcharname", fnSetCharName );

local function fnAddAllCommands( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You have no rank argument." );
		return;
		
	end
	
	local permissions = {}
	for k,v in next, GAMEMODE.Commands do
	
		permissions[k] = true;
		
	end
	
	GAMEMODE.Ranks[args[1]] = {
	
		Permissions = permissions,
	
	};
	GAMEMODE:SavePermissions();
	
	ply:Notify( COLOR_WHITE, "Usergroup %s now has access to all commands.", args[1] );

end
concommand.AddAdmin( "rpa_addallcommands", fnAddAllCommands );

local function fnAddUsergroup( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You have no rank argument." );
		return;
	
	end
	
	local permissions = {};
	if( #args > 1 ) then
	
		for k,v in next, args do
		
			if( k > 1 ) then
			
				permissions[v] = true;
				
			end
			
		end
	
	end
	
	GAMEMODE.Ranks[args[1]] = {
	
		Permissions = permissions,
		
	}
	GAMEMODE:SavePermissions();
	
	ply:Notify( COLOR_WHITE, "New usergroup %s has been added. Saving to server...", args[1] );

end
concommand.AddAdmin( "rpa_addusergroup", fnAddUsergroup );

local function fnRemoveUsergroup( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You have no rank argument." );
		return;
		
	end
	
	GAMEMODE.Ranks[args[1]] = nil;
	GAMEMODE:SavePermissions();
	
	ply:Notify( COLOR_WHITE, "Usergroup %s has been removed.", args[1] );

end
concommand.AddAdmin( "rpa_removeusergroup", fnRemoveUsergroup );

local function fnAddPermission( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a rank." );
		return;
		
	end
	
	if( #args < 2 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a permission to add." );
		return;
		
	end

	local permissions = {};
	if( #args > 1 ) then
	
		for k,v in next, args do
		
			if( k > 1 ) then
			
				permissions[v] = true;
				
			end
			
		end
	
	end
	
	if( !GAMEMODE.Ranks[args[1]] ) then
	
		ply:Notify( COLOR_WHITE, "This is not a valid rank." );
		return;
		
	end
	
	local curPermissions = GAMEMODE.Ranks[args[1]].Permissions or {};
	for k,v in next, permissions do
	
		curPermissions[k] = true;
		
	end
	GAMEMODE:SavePermissions();
	
	ply:Notify( COLOR_WHITE, "Permissions successfully updated." );

end
concommand.AddAdmin( "rpa_addpermission", fnAddPermission );

local function fnTakePermission( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a rank." );
		return;
		
	end
	
	if( #args < 2 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a permission to remove." );
		return;
		
	end
	
	if( !GAMEMODE.Ranks[args[1]] ) then
	
		ply:Notify( COLOR_WHITE, "This is not a valid rank." );
		return;
		
	end
	
	local curPermissions = GAMEMODE.Ranks[args[1]].Permissions or {};
	curPermissions[args[2]] = nil;
	GAMEMODE:SavePermissions();
	
	ply:Notify( COLOR_WHITE, "Permissions successfully updated." );

end
concommand.AddAdmin( "rpa_takepermission", fnTakePermission );

local function fnSetRankAdmin( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a rank." );
		return;
		
	end
	
	local rank = GAMEMODE.Ranks[args[1]];
	if( rank ) then
	
		rank.IsAdmin = !rank.IsAdmin;
		GAMEMODE:SavePermissions();
		ply:Notify( COLOR_WHITE, "Permissions successfully updated." );
		
	else
	
		ply:Notify( COLOR_WHITE, "This is not a valid rank." );
		return;
		
	end

end
concommand.AddAdmin( "rpa_setrankadmin", fnSetRankAdmin );

local function fnSetRankSuperAdmin( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a rank." );
		return;
		
	end
	
	local rank = GAMEMODE.Ranks[args[1]];
	if( rank ) then
	
		rank.IsSuperAdmin = !rank.IsSuperAdmin;
		GAMEMODE:SavePermissions();
		ply:Notify( COLOR_WHITE, "Permissions successfully updated." );
		
	else
	
		ply:Notify( COLOR_WHITE, "This is not a valid rank." );
		return;
		
	end

end
concommand.AddAdmin( "rpa_setranksuperadmin", fnSetRankSuperAdmin );

local function fnSetCharDisabled( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a player." );
		return;
	
	end
	
	local target = ply:FindPlayer( args[1] );
	
	if( args[2] ) then
	
		args[2] = tobool( args[2] );
		
	end
	
	if( target and target:CharID() > -1 ) then
	
		local CharacterData = target:GetCharDataByID( target:CharID() );
		CharacterData.Disabled = tobool( args[2] ) or !CharacterData.Disabled;
		target:UpdateCharacter( target:CharID() );
		target:UnloadCharacter();
		ply:Notify( COLOR_WHITE, "Target's current character's disabled state has changed to %s.", tostring( CharacterData.Disabled ) );
	
	else
	
		ply:Notify( COLOR_WHITE, "Target not found or is not valid." );
		return;
		
	end

end
concommand.AddAdmin( "rpa_setchardisabled", fnSetCharDisabled );

local function fnSetFaction( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a player." );
		return;
		
	end
	
	if( #args < 2 ) then
	
		args[2] = "loner";
		
	end
	
	local target = ply:FindPlayer( args[1] );
	local faction = GAMEMODE.Factions[ args[2] ];
	
	if( !target or !IsValid( target ) ) then
	
		ply:Notify( COLOR_WHITE, "Target not found." );
		return;
		
	end
	
	if( !faction ) then
	
		ply:Notify( COLOR_WHITE, "Invalid faction." );
		return;
		
	end
	
	target:SetTeam( faction.index );
	target:UpdateCharacterByField( "Faction", faction.identifier );
	-- call relevant functions
	target:Kill();
	
	ply:Notify( COLOR_WHITE, "%s has had their faction changed to %s.", target:GetFormattedName(), faction.Name );

end
concommand.AddAdmin( "rpa_setfaction", fnSetFaction );

local function fnCreateItem( ply, cmd, args )

	if( #args < 1 ) then
	
		-- network entire item list
		
	end
	
	local metaitem = GAMEMODE:GetMeta( args[1] );
	if( metaitem ) then
	
		if( args[2] ) then
	
			local ent = GAMEMODE:CreateNewItemEntity( args[1], ply:EyePos() + ply:GetAimVector() * 50, Angle() );
			hook.Run( "OnAdminCreatedItemEntity", ply, ent );
	
		else
		
			local ItemObj = ply:GetInventory():GiveItem( args[1] );
			if( ItemObj ) then
			
				hook.Run( "OnAdminCreatedItem", ply, ItemObj );
				
			end
			
		end
	
	else
	
		ply:Notify( COLOR_WHITE, "This is not a valid item." );
		return;
		
	end

end
concommand.AddAdmin( "rpa_createitem", fnCreateItem );

local function fnCreateArtifact( ply, cmd, args )

	if( #args < 1 ) then
	
		-- network entire artifact list
		
	end
	
	local item = GAMEMODE:GetMeta( args[1] );
	if( item and item.Artifact ) then
	
		local ent = GAMEMODE:CreateNewItemEntity( args[1], ply:EyePos() + ply:GetAimVector() * 50, Angle() );
		ent:SetNoDraw( true );
		hook.Run( "OnAdminCreatedItemEntity", ply, ent );
	
	else
	
		ply:Notify( COLOR_WHITE, "This is not a valid artifact." );
		return;
		
	end

end
concommand.AddAdmin( "rpa_createartifact", fnCreateArtifact );

local function fnSetFactionSpawnPos( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "You need to specify a faction." );
		return;
		
	end

	local faction = GAMEMODE.Factions[args[1]];
	
	if( faction ) then
	
		GAMEMODE:SetFactionSpawnPos( args[1], ply:GetEyeTraceNoCursor().HitPos );
		ply:Notify( COLOR_WHITE, "Spawn position for faction %s has been set.", faction.Name );
	
	else
	
		ply:Notify( COLOR_WHITE, "This is not a valid faction." );
		return;
		
	end

end
concommand.AddAdmin( "rpa_setfactionspawnpos", fnSetFactionSpawnPos );

local function fnNoclip( ply, cmd, args )

	-- really just so you can give ranks ability to noclip

end
concommand.AddAdmin( "rpa_noclip", fnNoclip );

-- uh oh, alot of these commands make regular queries!!
-- very insecure. come back and fix!

local function fnDeleteCharacter( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "Missing character ID argument." );
		return;
		
	end
	
	local id = args[1];
	
	local function onSuccess()
	
		ply:Notify( COLOR_WHITE, "Character successfully removed." );
		return;
	
	end
	wzsql.Query( Format( "DELETE FROM Characters WHERE id = %d", id ), onSuccess );

end
concommand.AddAdmin( "rpa_deletecharacter", fnDeleteCharacter );

local function fnSetOfflineCharDisabled( ply, cmd, args )

	if( #args < 1 ) then
	
		ply:Notify( COLOR_WHITE, "Missing character ID argument." );
		return;
		
	end
	
	if( #args < 2 ) then
	
		ply:Notify( COLOR_WHITE, "Missing character disabled state." );
		return;
		
	end
	
	args[2] = math.floor( tonumber( args[2] ) );
	args[1] = math.floor( tonumber( args[1] ) );
	
	local function onSuccess( data )
		
		ply:Notify( COLOR_WHITE, "Character %d disabled state set to %d", args[1], args[2] );
		GAMEMODE:FindCharacter( args[1], function( szSteamID, nIndex )
			
			local target = ply:FindPlayer( szSteamID );
			GAMEMODE.g_CharacterTable[szSteamID][nIndex].Disabled = tobool( args[2] );
			netstream.Start( target, "LoadIndividualCharacter", GAMEMODE.g_CharacterTable[target:SteamID()] );
			
		end );
		
	end
	wzsql.Query( Format( "UPDATE Characters SET Disabled = %d WHERE id = %d", args[2], args[1] ), onSuccess );

end
concommand.AddAdmin( "rpa_setofflinechardisabled", fnSetOfflineCharDisabled );

-- inventory management
-- remove item (remove child inventory and all child items)
-- add item

-- character management
-- delete character
-- add character
-- duplicate character
-- edit character
