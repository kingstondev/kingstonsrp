FACTION.Name = "Free Stalker";
FACTION.Desc = "An unaffiliated Zone explorer.";
FACTION.UsesFactionSpawn = true;
FACTION.Color = Color( 128, 128, 128 );
FACTION.Models = {
	"models/kingston/male_big_test.mdl",
	"models/cultist/stalker/merc.mdl"
}
FACTION.GearSelection = {
	{ "test_detector", 1000 },
}
FACTION_LONER = FACTION.index;