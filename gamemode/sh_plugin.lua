AddCSLuaFile();

/*
	Kingston isn't necessarily supposed to be a framework, but a plugin system is helpful for
	changing functionality when you want to move premise to premise.
	First we start with detouring hook.Call, then looping through all folders inside of plugins folder,
	loading all files.
	
	An issue with nutscript was that plugins that attempted to do database interaction on loading
	would error out due to plugins being loaded before the database connects. This is why we have:
	
	OnDatabaseConnected
	OnDatabaseConnectFailed
	OnQueryPrepared
	OnQuerySuccess
	OnQueryFailed
	
	You can add new tables to the gamemode by adding new entries like so:
	
	wzsql.m_rgSQLStruct["TableName"] = {
		{ "ColumnName", "DataType"[, "Default"] },
	};
	
	Preparing queries is similar:
	
	wzsql.PreparedQueries["GlobalVarName"] = "szQuery";
*/

/*
	New method for plugins:
	
	Instead of detouring hook.Call, we can simply loop through all functions in our gamemode table after loading them
	and adding them with hook.Add to the gamemode table.
*/

GM.Plugins = GM.Plugins or {};
local OldHookCall = hook.Call;

function hook.Call( szEventName, GamemodeTable, ... )

	/*
		Returning any value besides nil from the hook's function will stop other hooks of the same event down the loop 
		from being executed. Only return a value when absolutely necessary and when you know what you are doing.
		
		It WILL break other addons.
	*/

	// First, we'll call all plugin hooks first.
	if( GamemodeTable ) then
	
		for _,plugin in next, GamemodeTable.Plugins do
		
			if( !plugin[szEventName] ) then continue end
			
			local result = plugin[szEventName]( plugin, ... );
			if( result != nil ) then
			
				return result;
				
			end
		
		end
		
	end
	
	// then we'll call old hook.Call.
	
	local result = OldHookCall( szEventName, GamemodeTable or gmod.GetGamemode(), ... );
	if( result != nil ) then
	
		return result;
		
	end

end

local s_tPluginFiles,s_tPluginDirs = file.Find( GM.FolderName.."/plugins/*", "LUA", "namedesc" );
if( #s_tPluginFiles > 0 ) then

	for k,v in next, s_tPluginFiles do
	
		PLUGIN = {};
	
		util.include( v );
		
		GM.Plugins[string.StripExtension( v )] = PLUGIN;
		
	end

end
if( #s_tPluginDirs > 0 ) then

	for k,v in next, s_tPluginDirs do
	
		PLUGIN = {};
	
		util.IncludeDir( "kingstonsrp/plugins/"..v, true, true );
		
		GM.Plugins[v] = PLUGIN;
		
	end

end