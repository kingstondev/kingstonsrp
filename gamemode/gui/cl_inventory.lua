local PANEL = { };

function PANEL:Paint( w, h )

end

function PANEL:OnMousePressed()
	
end

derma.DefineControl( "CInventoryEquip", "", PANEL, "EditablePanel" );

PANEL = { };

function PANEL:Paint( w, h )
	
	return true;

end

derma.DefineControl( "CInventoryBack", "", PANEL, "EditablePanel" );

PANEL = {};

function PANEL:Init()

	GAMEMODE.Inventory = self; -- maybe use return value from vgui.Create instead
	
	self.w, self.h = ScrW(), ScrH();
	self.x, self.y = ScrW( ) / 2 - self.w / 2, ScrH( ) / 2 - self.h / 2;
	
	self:SetSize( self.w, self.h );
	self:SetPos( self.x, self.y );
	self:SetTitle( "" );
	self:MakePopup( );
	self:ShowCloseButton( false );
	self:SetAlpha( 0 );
	self:AlphaTo( 255, 0.2, 0 );
	self:SetDraggable( false );
	
	if( LocalPlayer():CharacterHasFlag( "T" ) ) then
	
		self.TechMenu = vgui.Create( "CTechMenu", self );
	
	end
	
	self.InventoryBack = vgui.Create( "DScrollPanel", self );
	self.InventoryBack:SetSize( ScrW() / 3.7, ScrH() );
	self.InventoryBack:SetPos( ScrW() / 1.575, 0 );
	self.InventoryBack:SetSkin( "StalkerSkin" );
	function self.InventoryBack:Paint( w, h )
	
		kingston.gui.FindFunc( self, "Paint", "InventoryFrame" );
		
		surface.SetFont( "InventoryNameDisplay" );
		
		local tW,tH = surface.GetTextSize( LocalPlayer():RPName() );
		
		surface.SetTextColor( Color( 216, 195, 177 ) );
		surface.SetTextPos( self:GetWide() / 20, self:GetTall() / 20 );
		surface.DrawText( LocalPlayer():RPName() );
		
		surface.SetFont( "InventoryFactionDisplay" );
		
		surface.SetTextColor( Color( 100, 100, 100, 175 ) );
		surface.SetTextPos( self:GetWide() / 20, self:GetTall() / 20 + tH );
		surface.DrawText( GAMEMODE.Factions[LocalPlayer():GetFaction()].Name );
		
		surface.SetFont( "InventoryWeight" );
		
		surface.SetTextColor( Color( 100, 100, 100, 175 ) );
		surface.SetTextPos( self:GetWide() / 3.33, ScrH() / 1.042 );
		surface.DrawText( "Total weight:" );
		
		local tW,tH = surface.GetTextSize( "Total weight:" );
		
		surface.SetTextColor( Color( 255, 255, 255 ) );
		surface.SetTextPos( self:GetWide() / 3.33 + tW + 5, ScrH() / 1.042 );
		surface.DrawText( LocalPlayer():GetWeight().." kg" );
		
		local tW,tH = surface.GetTextSize( "Total weight: "..LocalPlayer():GetWeight().." kg" );
		
		surface.SetTextColor( Color( 100, 100, 100, 175 ) );
		surface.SetTextPos( self:GetWide() / 3.33 + tW + 10, ScrH() / 1.042 );
		surface.DrawText( "(max "..LocalPlayer():GetMaxWeight().." kg)" );
	
	end
	
	self.InventoryScroll = vgui.Create( "DScrollPanel", self.InventoryBack );
	self.InventoryScroll:SetSize( ScrW() / 4.04, self.InventoryBack:GetTall() - ( self.InventoryBack:GetTall() / 4.5 ) );
	self.InventoryScroll:SetPos( 24, self.InventoryBack:GetTall() / 6.8 );
	self.InventoryScroll:SetSkin( "StalkerSkin" );
	function self.InventoryScroll:Paint( w, h )
	
	end
	self.InventoryScroll.Containers = {};
	
	self.EquipBack = vgui.Create( "DPanel", self );
	self.EquipBack:SetSize( ScrW() / 3.7, ScrH() );
	self.EquipBack:SetPos( ( ScrW() / 2 ) - ( ( ScrW() / 3.7 ) / 2 ), 0 );
	self.EquipBack:SetSkin( "StalkerSkin" );
	function self.EquipBack:Paint( w, h )
	
		kingston.gui.FindFunc( self, "Paint", "EquipFrame" );
	
	end
	
	self.Back = vgui.Create( "CImageButton", self );
	self.Back:SetSize( ScrW() / 4, ScrH() / 31.764 );
	self.Back:SetPos( ( ScrW() / 2 ) - ( self.Back:GetWide() / 2 ) - 1, ( ScrH() - self.Back:GetTall() ) - 10 );
	self.Back:SetImage( "kingston/ui_actor_menu_btn.png" );
	self.Back:SetDownImage( "kingston/ui_actor_menu_btn_down.png" );
	self.Back:SetTextColor( Color( 241, 218, 184, 255 ) );
	self.Back:SetFont( "Stalker2-ButtonText" );
	self.Back:SetText( "Back" );
	function self.Back:DoClick()
	
		self:AlphaTo( 0, 0.2, 0 );
		self:GetParent():Close();
	
	end
	
	self.EquipBack.Clothing = vgui.Create( "CInventoryEquip", self.EquipBack );
	self.EquipBack.Clothing:SetSize( ScreenScale( 52 ) + 2, ScreenScale( 78 ) );
	self.EquipBack.Clothing:SetPos( ( self.EquipBack:GetWide() / 2 ) - ( ScreenScale( 52 ) / 2 ), self.EquipBack:GetTall() / 5.5 );
	self.EquipBack.Clothing.AcceptingItemType = CATEGORY_CLOTHING;
	self.EquipBack.Clothing:Receiver( "Items", function( Receiver, Dropped, bIsDropped )
	
		if( bIsDropped ) then
		
			local ToSlot = Receiver; -- spaghetti here
			local DraggedItem = Dropped[1];
			local ItemObj = DraggedItem.Item;
			local ItemMeta = GAMEMODE:GetMeta( ItemObj.Class );
			if( ToSlot.AcceptingItemType == ItemMeta.Type ) then

				netstream.Start( "ItemMove", ItemObj:GetID(), -1, -1, ItemObj:GetInventory():GetID(), true );
				ItemObj:CallFunction( "Equip" );
				netstream.Start( "ItemCallFunction", ItemObj:GetID(), "Equip" );
				self:RefreshInventory();
				
			end
		
		end
	
	end );
	
	self.EquipBack.Primary = vgui.Create( "CInventoryEquip", self.EquipBack );
	self.EquipBack.Primary:SetSize( ScreenScale( 46 ) + 2, ScreenScale( 176 ) );
	self.EquipBack.Primary:SetPos( ( self.EquipBack:GetWide() / 1.195 ) - ( ScreenScale( 46 ) / 2 ), self.EquipBack:GetTall() / 75 );
	self.EquipBack.Primary.AcceptingItemType = CATEGORY_PRIMARYWEAPON;
	self.EquipBack.Primary:Receiver( "Items", function( Receiver, Dropped, bIsDropped )
	
		if( bIsDropped ) then

			local ToSlot = Receiver; -- spaghetti here
			local DraggedItem = Dropped[1];
			local ItemObj = DraggedItem.Item;
			local ItemMeta = GAMEMODE:GetMeta( ItemObj.Class );
			if( ToSlot.AcceptingItemType == ItemMeta.Type ) then

				netstream.Start( "ItemMove", ItemObj:GetID(), -1, -1, ItemObj:GetInventory():GetID(), true );
				ItemObj:CallFunction( "Equip" );
				netstream.Start( "ItemCallFunction", ItemObj:GetID(), "Equip" );
				self:RefreshInventory();
				
			end
		
		end
	
	end );
	
	self.EquipBack.Secondary = vgui.Create( "CInventoryEquip", self.EquipBack );
	self.EquipBack.Secondary:SetSize( ScreenScale( 46 ) + 2, ScreenScale( 176 ) );
	self.EquipBack.Secondary:SetPos( ( self.EquipBack:GetWide() / 6.4 ) - ( ScreenScale( 46 ) / 2 ), self.EquipBack:GetTall() / 75 );
	self.EquipBack.Secondary.AcceptingItemType = CATEGORY_SECONDARYWEAPON;
	self.EquipBack.Secondary:Receiver( "Items", function( Receiver, Dropped, bIsDropped )

		if( bIsDropped ) then
		
			local ToSlot = Receiver; -- spaghetti here
			local DraggedItem = Dropped[1];
			local ItemObj = DraggedItem.Item;
			local ItemMeta = GAMEMODE:GetMeta( ItemObj.Class );
			if( ToSlot.AcceptingItemType == ItemMeta.Type ) then

				netstream.Start( "ItemMove", ItemObj:GetID(), -1, -1, ItemObj:GetInventory():GetID(), true );
				ItemObj:CallFunction( "Equip" );
				netstream.Start( "ItemCallFunction", ItemObj:GetID(), "Equip" );
				self:RefreshInventory();
				
			end
		
		end
	
	end );
	
	self.EquipBack.Detector = vgui.Create( "CInventoryEquip", self.EquipBack );
	self.EquipBack.Detector:SetSize( ScreenScale( 52 ) + 2, ScreenScale( 26 ) );
	self.EquipBack.Detector:SetPos( ( self.EquipBack:GetWide() / 2 ) - ( ScreenScale( 52 ) / 2 ), self.EquipBack:GetTall() / 2.35 );
	self.EquipBack.Detector.AcceptingItemType = CATEGORY_DETECTOR;
	self.EquipBack.Detector:Receiver( "Items", function( Receiver, Dropped, bIsDropped )

		if( bIsDropped ) then
		
			local ToSlot = Receiver; -- spaghetti here
			local DraggedItem = Dropped[1];
			local ItemObj = DraggedItem.Item;
			local ItemMeta = GAMEMODE:GetMeta( ItemObj.Class );
			if( ToSlot.AcceptingItemType == ItemMeta.Type ) then

				netstream.Start( "ItemMove", ItemObj:GetID(), -1, -1, ItemObj:GetInventory():GetID(), true );
				ItemObj:CallFunction( "Equip" );
				netstream.Start( "ItemCallFunction", ItemObj:GetID(), "Equip" );
				self:RefreshInventory();
				
			end
		
		end
	
	end );
	
	hook.Run( "CreateEquipSlots", self.EquipBack );
	
	self:RefreshInventory();
	
end

function PANEL:RefreshInventory()

	local y = 0;

	for k,v in next, self.EquipBack:GetChildren() do
	
		if( v.Item ) then
		
			v.Item:Remove();
			
		end
		
	end
	
	for k,v in next, self.InventoryScroll.Containers do
	
		self.InventoryScroll.Containers[k]:Remove();
	
	end

	for k,v in next, LocalPlayer():GetInventories() do -- refactor these loops

		if( self.InventoryScroll.Containers[k] ) then
		
			self.InventoryScroll.Containers[k]:Remove();
			
		end
	
		self.InventoryScroll.Containers[k] = vgui.Create( "DFrame", self.InventoryScroll );
		self.InventoryScroll.Containers[k]:SetSize( self.InventoryScroll:GetWide(), ( v:GetHeight() * ScreenScale( 22 ) ) + 24 );
		self.InventoryScroll.Containers[k]:SetPos( 0, y );
		self.InventoryScroll.Containers[k]:ShowCloseButton( false );
		self.InventoryScroll.Containers[k]:SetTitle( v:GetName() );
		self.InventoryScroll.Containers[k]:SetSkin( "StalkerSkin" );
		self.InventoryScroll.Containers[k]:SetZPos( 99 );
		self.InventoryScroll.Containers[k].Inventory = v;
		self.InventoryScroll.Containers[k].Slots = {};
		self.InventoryScroll.Containers[k].Paint = function( self, w, h )
		
		end
		y = y + ( ( v:GetHeight() * ScreenScale( 22 ) ) + 24 );
	
		for i = 1, v:GetWidth() do
			
			self.InventoryScroll.Containers[k].Slots[i] = {};
		
			for m = 1, v:GetHeight() do

				self.InventoryScroll.Containers[k].Slots[i][m] = vgui.Create( "CInventorySquare", self.InventoryScroll.Containers[k] );
				self.InventoryScroll.Containers[k].Slots[i][m]:SetSize( ScreenScale( 22 ), ScreenScale( 22 ) );
				self.InventoryScroll.Containers[k].Slots[i][m]:SetPos( ( i * ScreenScale( 22 ) ) - ScreenScale( 22 ), ( ( m * ScreenScale( 22 ) ) - ScreenScale( 22 ) ) + 24 );
				self.InventoryScroll.Containers[k].Slots[i][m].nX = i;
				self.InventoryScroll.Containers[k].Slots[i][m].nY = m;
			
			end
			
		end
		
		local FilledSlots = {};
		
		for m,n in next, v:GetContents() do -- suck
		
			local IsUsed = false;
			local metaitem = GAMEMODE:GetMeta( n.Class );
			
			if( n:GetVar( "Equipped", false ) ) then

				for f,g in next, self.EquipBack:GetChildren() do

					if( g.AcceptingItemType == metaitem.Type and !IsUsed and !FilledSlots[f] ) then

						local item = vgui.Create( "CItem", g );
						item:SetPos( 0, 0 );
						item:SetSize( g:GetWide(), g:GetTall() );
						item.Item = n;
						item:SetZPos( 99 );
						item:SetModel( metaitem.Model );
						item:Droppable( "Items" );
						g.Item = item;
						IsUsed = true;
						FilledSlots[f] = true;
						
					end
					
				end

				continue;
				
			end
			
			if( !self.InventoryScroll.Containers[k].Slots[n.X] ) then continue end
			
			local item = vgui.Create( "CItem", self.InventoryScroll.Containers[k] );
			item:SetPos( ( n.X - 1 ) * ScreenScale( 22 ), ( n.Y - 1 ) * ScreenScale( 22 ) + 24 );
			item:SetSize( metaitem.W * ScreenScale( 22 ), metaitem.H * ScreenScale( 22 ) );
			item.Item = n;
			item:SetZPos( 99 );
			item:SetModel( metaitem.Model );
			item:Droppable( "Items" );
			
			self.InventoryScroll.Containers[k].Slots[n.X][n.Y].Item = item;

		end
		
	end

end

function PANEL:Paint( w, h )

end

function PANEL:OnClose()

	GAMEMODE.Inventory = nil;

end

vgui.Register( "DInventoryPanel", PANEL, "DFrame" );