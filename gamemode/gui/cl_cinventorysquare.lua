local PANEL = { };

local function InitDrop( panel )

	panel:Receiver( "Items", function( Receiver, Dropped, bIsDropped )
		
		if( bIsDropped ) then

			local ToSlot = Receiver; -- spaghetti here
			local DraggedItem = Dropped[1];
			local ReceivingInventory = ToSlot:GetParent().Inventory;
			local SendingInventory = DraggedItem.Item:GetInventory();
			local SendingPanel = DraggedItem:GetParent();
			local ItemObj = DraggedItem.Item;
			local ItemMeta = GAMEMODE:GetMeta( ItemObj.Class );
			if( !ReceivingInventory:IsInventorySlotOccupiedItem( panel.nX, panel.nY, ItemMeta.W, ItemMeta.H ) ) then
			
				if( SendingPanel.Slots ) then

					SendingPanel.Slots[ItemObj.X][ItemObj.Y].Item:Remove();
					SendingPanel.Slots[ItemObj.X][ItemObj.Y].Item = nil;
					
				end
				
				ItemObj:MoveItem( panel.nX, panel.nY, ReceivingInventory:GetID() );
				netstream.Start( "ItemMove", ItemObj.id, panel.nX, panel.nY, ReceivingInventory:GetID(), false );

				hook.Run( "ItemMovedSlot", panel, ItemObj, panel.nX, panel.nY );
				GAMEMODE.Inventory:RefreshInventory();
				
			end
			
		end
		
	end );

end

function PANEL:Init()

	InitDrop( self );
	
end

function PANEL:Paint( w, h )

	surface.SetDrawColor( Color( 255, 255, 255, 40 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	return true;

end

function PANEL:OnMousePressed()
	
end

derma.DefineControl( "CInventorySquare", "", PANEL, "EditablePanel" );