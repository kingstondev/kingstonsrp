magazineselect = magazineselect or {}

if (CLIENT) then

	surface.CreateFont("SignalFont", {
		font = "Trebuchet18",
		size = 25,
		weight = 400,
		shadow = true,
	})

	surface.CreateFont("SignalFontBlur", {
		font = "Trebuchet18",
		size = 25,
		weight = 400,
		shadow = true,
		blursize = 4,
	})

	local w, h = ScrW(), ScrH()
	local selected = 0
	magazineselect.open = false

	function magazineselect.opensignal()
		if( LocalPlayer():CharID() <= 0 or !LocalPlayer():Alive() ) then
		
			return;
			
		end

		magazineselect.open = true
		magazineselect.ammo = {};
		
		local wep = ply:GetActiveWeapon();
		if wep.ShotgunReload then return end;
		
		if( wep and wep:IsValid() and GAMEMODE.WeaponMagazines[wep:GetClass()] ) then
		
			for g,h in next, GAMEMODE.g_ItemTable do -- looping through a table instead of using associative array, not good.
			
				if( h.Type == CATEGORY_MAGPOUCH ) then
				
					for m,n in next, h:GetChildInventory():GetContents() do
				
						if( table.HasValue( GAMEMODE.WeaponMagazines[wep:GetClass()], n:GetClass() ) ) then
					
							magazineselect.ammo[#magazineselect.ammo + 1] = {
								ref = n
							};
							
						end
						
					end
				
				end
			
			end
			
		end
		
		for k, v in pairs(magazineselect.ammo) do
			v.curpos = {w/2, h/2}
			v.curalpha = 0
		end

		gui.EnableScreenClicker(true)
	end

	function magazineselect.closesignal()
	
		magazineselect.open = false;		
		local wep = ply:GetActiveWeapon();
		if( !magazineselect.ammo ) then
		
			gui.EnableScreenClicker( false );
			return;
			
		end
		if( !magazineselect.ammo[selected] ) then
		
			gui.EnableScreenClicker( false );
			return;
			
		end
		local item = magazineselect.ammo[selected].ref;
		if( !item ) then 
		
			gui.EnableScreenClicker( false );
			return;
			
		end
		local metaitem = GAMEMODE:GetMeta( item.Class );

		local x, y = gui.MousePos();
		x, y = ( x-w/2 ), ( y-h/2 );
		if( x == 0 and y == 0 ) then 
		
			gui.EnableScreenClicker( false );
			return;
			
		end
		
		if( !wep or !wep:IsValid() ) then return end
	
		for m,n in next, GAMEMODE.g_ItemTable do
	
			if( n.Base == "magazine" and table.HasValue( GAMEMODE.WeaponMagazines[wep:GetClass()], n:GetClass() ) ) then
			
				if( n.IsLoaded ) then
				
					if( n:GetID() == item:GetID() ) then 
					
						gui.EnableScreenClicker( false );
						return;
					
					end
				
					n.IsLoaded = false;
					break;
					
				end
				
			end
			
		end
		
		hook.Run( "WeaponReloaded", wep );
		if( metaitem.OnLoaded ) then
		
			metaitem.OnLoaded( item, wep );
			
		end
		
		item.IsLoaded = true;
		netstream.Start( "Reload", item:GetID() );
		LocalPlayer():SetAmmo( item:GetVar( "Rounds", 0 ), wep:GetPrimaryAmmoType() );
		wep:Reload();

		gui.EnableScreenClicker(false)
		
	end

	local signalalpha = 0
	local distance = w*.1
	local icons = {
		[1] = surface.GetTextureID("vgui/notices/error"),
		[2] = surface.GetTextureID("vgui/notices/generic"),
		[3] = surface.GetTextureID("vgui/notices/hint"),
		[4] = surface.GetTextureID("vgui/notices/undo"),
		[5] = surface.GetTextureID("vgui/notices/cleanup"),
	}

	function magazineselect.draw()
	
		if magazineselect.open then
			signalalpha = Lerp(FrameTime()*9, signalalpha, 255)
		else
			signalalpha = Lerp(FrameTime()*20, signalalpha, 0)
		end

		if signalalpha > 0 then
			local rad = math.rad(360/#magazineselect.ammo)
			local deg = math.deg(rad)

			local x, y = gui.MousePos( )
			x, y = (x-w/2), (y-h/2)

			local seldeg = -math.deg(math.atan2(x, y))
			if seldeg < 0 then
				seldeg = 360+seldeg
			end

			for k, v in ipairs(magazineselect.ammo) do
				local rdeg = deg*k
				local min = (rdeg - deg/2)
				local max = (rdeg + deg/2)
				if max > 360 then
					max = max - 360
					if min < seldeg or max > seldeg then
						selected = k
					end 
				else
					if min < seldeg and max > seldeg then
						selected = k
					end 
				end

				local szText = v.ref:GetName().." "..v.ref:GetVar( "Rounds", 0 ).."/"..v.ref["Capacity"];
				
				surface.SetFont("SignalFont")
				local tw, th = surface.GetTextSize(szText)
				surface.SetTextColor(Color(255, 255, 255, signalalpha))
				
				v.curpos[1] = Lerp(FrameTime()*8, v.curpos[1], w/2-tw/2+math.sin(rad*-k)*distance)
				v.curpos[2] = Lerp(FrameTime()*8, v.curpos[2], h/2-th/2+math.cos(rad*k)*distance)

				if (selected == k and x ~= 0 and y ~= 0) then
					surface.SetFont("SignalFontBlur")
					local tw, th = surface.GetTextSize(szText)
					surface.SetTextPos(math.Round(v.curpos[1]), math.Round(v.curpos[2]))
					surface.DrawText(szText)
				end

				surface.SetFont("SignalFont")
				local tw, th = surface.GetTextSize(szText)
				surface.SetTextPos(math.Round(v.curpos[1]), math.Round(v.curpos[2]))
				surface.DrawText(szText)

			end
		end

	end

end