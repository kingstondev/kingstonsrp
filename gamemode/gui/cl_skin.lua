AddCSLuaFile();

local SKIN = { };

SKIN.PrintName 		= "STALKER";
SKIN.Author			= "rusty";
SKIN.DermaVersion	= 1;

SKIN.Colours = {}

SKIN.Colours.Window = {}
SKIN.Colours.Window.TitleActive			= GWEN.TextureColor( 4 + 8 * 0, 508 )
SKIN.Colours.Window.TitleInactive		= GWEN.TextureColor( 4 + 8 * 1, 508 )

SKIN.Colours.Button = { }
SKIN.Colours.Button.Normal				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Hover				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Down				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Disabled			= Color( 128, 128, 128, 255 );

SKIN.Colours.Tab = {}
SKIN.Colours.Tab.Active = {}
SKIN.Colours.Tab.Active.Normal			= GWEN.TextureColor( 4 + 8 * 4, 508 )
SKIN.Colours.Tab.Active.Hover			= GWEN.TextureColor( 4 + 8 * 5, 508 )
SKIN.Colours.Tab.Active.Down			= GWEN.TextureColor( 4 + 8 * 4, 500 )
SKIN.Colours.Tab.Active.Disabled		= GWEN.TextureColor( 4 + 8 * 5, 500 )

SKIN.Colours.Tab.Inactive = {}
SKIN.Colours.Tab.Inactive.Normal		= GWEN.TextureColor( 4 + 8 * 6, 508 )
SKIN.Colours.Tab.Inactive.Hover			= GWEN.TextureColor( 4 + 8 * 7, 508 )
SKIN.Colours.Tab.Inactive.Down			= GWEN.TextureColor( 4 + 8 * 6, 500 )
SKIN.Colours.Tab.Inactive.Disabled		= GWEN.TextureColor( 4 + 8 * 7, 500 )

SKIN.Colours.Label = { }
SKIN.Colours.Label.Default				= Color( 255, 255, 255, 255 );
SKIN.Colours.Label.Bright				= Color( 255, 255, 255, 255 );
SKIN.Colours.Label.Dark					= Color( 200, 200, 200, 255 );
SKIN.Colours.Label.Highlight			= Color( 255, 255, 255, 255 );

SKIN.Colours.Tree = {}
SKIN.Colours.Tree.Lines					= GWEN.TextureColor( 4 + 8 * 10, 508 ) ---- !!!
SKIN.Colours.Tree.Normal				= GWEN.TextureColor( 4 + 8 * 11, 508 )
SKIN.Colours.Tree.Hover					= GWEN.TextureColor( 4 + 8 * 10, 500 )
SKIN.Colours.Tree.Selected				= GWEN.TextureColor( 4 + 8 * 11, 500 )

SKIN.Colours.Properties = {}
SKIN.Colours.Properties.Line_Normal			= GWEN.TextureColor( 4 + 8 * 12, 508 )
SKIN.Colours.Properties.Line_Selected		= GWEN.TextureColor( 4 + 8 * 13, 508 )
SKIN.Colours.Properties.Line_Hover			= GWEN.TextureColor( 4 + 8 * 12, 500 )
SKIN.Colours.Properties.Title				= GWEN.TextureColor( 4 + 8 * 13, 500 )
SKIN.Colours.Properties.Column_Normal		= GWEN.TextureColor( 4 + 8 * 14, 508 )
SKIN.Colours.Properties.Column_Selected		= GWEN.TextureColor( 4 + 8 * 15, 508 )
SKIN.Colours.Properties.Column_Hover		= GWEN.TextureColor( 4 + 8 * 14, 500 )
SKIN.Colours.Properties.Border				= GWEN.TextureColor( 4 + 8 * 15, 500 )
SKIN.Colours.Properties.Label_Normal		= GWEN.TextureColor( 4 + 8 * 16, 508 )
SKIN.Colours.Properties.Label_Selected		= GWEN.TextureColor( 4 + 8 * 17, 508 )
SKIN.Colours.Properties.Label_Hover			= GWEN.TextureColor( 4 + 8 * 16, 500 )

SKIN.Colours.Category = {}
SKIN.Colours.Category.Header				= GWEN.TextureColor( 4 + 8 * 18, 500 )
SKIN.Colours.Category.Header_Closed			= GWEN.TextureColor( 4 + 8 * 19, 500 )
SKIN.Colours.Category.Line = {}
SKIN.Colours.Category.Line.Text				= GWEN.TextureColor( 4 + 8 * 20, 508 )
SKIN.Colours.Category.Line.Text_Hover		= GWEN.TextureColor( 4 + 8 * 21, 508 )
SKIN.Colours.Category.Line.Text_Selected	= GWEN.TextureColor( 4 + 8 * 20, 500 )
SKIN.Colours.Category.Line.Button			= GWEN.TextureColor( 4 + 8 * 21, 500 )
SKIN.Colours.Category.Line.Button_Hover		= GWEN.TextureColor( 4 + 8 * 22, 508 )
SKIN.Colours.Category.Line.Button_Selected	= GWEN.TextureColor( 4 + 8 * 23, 508 )
SKIN.Colours.Category.LineAlt = {}
SKIN.Colours.Category.LineAlt.Text				= GWEN.TextureColor( 4 + 8 * 22, 500 )
SKIN.Colours.Category.LineAlt.Text_Hover		= GWEN.TextureColor( 4 + 8 * 23, 500 )
SKIN.Colours.Category.LineAlt.Text_Selected		= GWEN.TextureColor( 4 + 8 * 24, 508 )
SKIN.Colours.Category.LineAlt.Button			= GWEN.TextureColor( 4 + 8 * 25, 508 )
SKIN.Colours.Category.LineAlt.Button_Hover		= GWEN.TextureColor( 4 + 8 * 24, 500 )
SKIN.Colours.Category.LineAlt.Button_Selected	= GWEN.TextureColor( 4 + 8 * 25, 500 )

SKIN.Colours.TooltipText = GWEN.TextureColor( 4 + 8 * 26, 500 )

SKIN.colTextEntryText			= Color( 255, 255, 255, 255 );
SKIN.colTextEntryTextHighlight	= Color( 255, 255, 255, 10 );
SKIN.colTextEntryTextCursor		= Color( 255, 255, 255, 255 );

function SKIN:PaintPanel( panel, w, h )

end

--[[ local function FramePerformLayout( self )
	
	if( self.lblTitle and self.lblTitle:IsValid() ) then
		
		self.lblTitle:NoClipping( false )
		self.lblTitle:SetPos( 24, -11 )
		self.lblTitle:SetSize( self:GetWide() - 25, 22 )
		self.lblTitle:SetTextColor(Color(252, 178, 69, 255))
		
	end
	
end ]]

function SKIN:PaintFrame( panel, w, h )

	kingston.gui.FindFunc( panel, "Paint", "DFrame" );
	
end

local contextMenuBg = Material( "kingston/ui_common_contextmenu.png" );

function SKIN:LayoutMenu( panel, w, h ) -- PerformLayout is called every frame.

end

function SKIN:PaintMenu( panel, w, h )

	surface.SetDrawColor( Color( 255, 255, 255, 255 ) );
	surface.SetMaterial( contextMenuBg );
	surface.DrawTexturedRect( 0, 0, w, h );

end

local function PaintNotches( x, y, w, h, num )

	if ( !num ) then return end

	local space = w / num

	for i=0, num do

		surface.DrawRect( x + i * space, y + 4, 1, 5 )

	end

end

function SKIN:PaintNumSlider( panel, w, h )

	if( !panel.bSetup ) then
	
		-- fuck you garry
		panel.bSetup = true;
		panel:SetEnabled( false );
		panel.OnMousePressed = function() end;
		panel.OnMouseReleased = function() end;
		panel:GetParent().Scratch.OnMousePressed = function() end;
		panel:GetParent().Scratch.OnMouseReleased = function() end;
		
	end
		

	surface.SetDrawColor( Color( 255, 255, 255, 100 ) )
	surface.DrawRect( 8, h / 2 - 1, w - 15, 1 )

	PaintNotches( 8, h / 2 - 1, w - 16, 1, panel.m_iNotches )

end

--[[ function SKIN:PaintSlider( panel, w, h )

end ]]

-- i dont think DMenuOption has derma.SkinHook for PerformLayout. Neither does DButton or DLabel.
-- https://github.com/Facepunch/garrysmod/blob/master/garrysmod/lua/vgui/dmenuoption.lua#L120

function SKIN:PaintMenuOption( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetTextColor( Color( 172, 172, 172 ) );
		panel:SetFont( "Trebuchet18" );
		panel:SizeToContents();
		panel:Center();
	
	end
	
	if( panel:IsHovered() ) then
	
		surface.SetDrawColor( Color( 152, 152, 152, 32 ) );
		surface.SetFont( panel:GetFont() );
		local tW,tH = surface.GetTextSize( panel:GetText() );
		surface.DrawRect( 30, 2, tW, tH );
		
	end

end

function SKIN:PaintButtonDown( panel, w, h )
	
	if( !panel.LaidOut ) then
		
		panel.LaidOut = true;
		panel:SetText( "" );
		panel:SetFont( "marlett" );
		panel:SetSize( 6, 6 );
		panel:SetPos( 0, panel:GetParent():GetTall() - 6 );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintButtonUp( panel, w, h )
	
	if( !panel.LaidOut ) then
		
		panel.LaidOut = true;
		panel:SetText( "" );
		panel:SetFont( "marlett" );
		panel:SetSize( 6, 6 );
		panel:SetPos( 0, 0 );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintVScrollBar( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetSize( 10, panel:GetParent():GetTall() );
		
	end

end

function SKIN:PaintScrollBarGrip( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetSize( 10, panel:GetParent():GetTall() );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintButton( panel, w, h )

	if ( !panel.m_bBackground ) then return end
	
	if( panel.m_bTitleButton ) then
	
		kingston.gui.FindFunc( panel, "Paint", "TitleButton" );
		return;
	
	end

	kingston.gui.FindFunc( panel, "Paint", "Button" );
	
	/*surface.SetDrawColor( 255, 255, 255, 255 );
	surface.DrawOutlinedRect( 0, 0, w, h );*/
end

derma.DefineSkin( "StalkerSkin", "STALKER Skin", SKIN );

SKIN = {};

SKIN.PrintName 		= "STALKER UI";
SKIN.Author			= "rusty";
SKIN.DermaVersion	= 1;

SKIN.Colours = {}

SKIN.Colours.Window = {}
SKIN.Colours.Window.TitleActive			= GWEN.TextureColor( 4 + 8 * 0, 508 )
SKIN.Colours.Window.TitleInactive		= GWEN.TextureColor( 4 + 8 * 1, 508 )

SKIN.Colours.Button = { }
SKIN.Colours.Button.Normal				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Hover				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Down				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Disabled			= Color( 128, 128, 128, 255 );

SKIN.Colours.Tab = {}
SKIN.Colours.Tab.Active = {}
SKIN.Colours.Tab.Active.Normal			= GWEN.TextureColor( 4 + 8 * 4, 508 )
SKIN.Colours.Tab.Active.Hover			= GWEN.TextureColor( 4 + 8 * 5, 508 )
SKIN.Colours.Tab.Active.Down			= GWEN.TextureColor( 4 + 8 * 4, 500 )
SKIN.Colours.Tab.Active.Disabled		= GWEN.TextureColor( 4 + 8 * 5, 500 )

SKIN.Colours.Tab.Inactive = {}
SKIN.Colours.Tab.Inactive.Normal		= GWEN.TextureColor( 4 + 8 * 6, 508 )
SKIN.Colours.Tab.Inactive.Hover			= GWEN.TextureColor( 4 + 8 * 7, 508 )
SKIN.Colours.Tab.Inactive.Down			= GWEN.TextureColor( 4 + 8 * 6, 500 )
SKIN.Colours.Tab.Inactive.Disabled		= GWEN.TextureColor( 4 + 8 * 7, 500 )

SKIN.Colours.Label = { }
SKIN.Colours.Label.Default				= Color( 255, 255, 255, 255 );
SKIN.Colours.Label.Bright				= Color( 255, 255, 255, 255 );
SKIN.Colours.Label.Dark					= Color( 200, 200, 200, 255 );
SKIN.Colours.Label.Highlight			= Color( 255, 255, 255, 255 );

SKIN.Colours.Tree = {}
SKIN.Colours.Tree.Lines					= GWEN.TextureColor( 4 + 8 * 10, 508 ) ---- !!!
SKIN.Colours.Tree.Normal				= GWEN.TextureColor( 4 + 8 * 11, 508 )
SKIN.Colours.Tree.Hover					= GWEN.TextureColor( 4 + 8 * 10, 500 )
SKIN.Colours.Tree.Selected				= GWEN.TextureColor( 4 + 8 * 11, 500 )

SKIN.Colours.Properties = {}
SKIN.Colours.Properties.Line_Normal			= GWEN.TextureColor( 4 + 8 * 12, 508 )
SKIN.Colours.Properties.Line_Selected		= GWEN.TextureColor( 4 + 8 * 13, 508 )
SKIN.Colours.Properties.Line_Hover			= GWEN.TextureColor( 4 + 8 * 12, 500 )
SKIN.Colours.Properties.Title				= GWEN.TextureColor( 4 + 8 * 13, 500 )
SKIN.Colours.Properties.Column_Normal		= GWEN.TextureColor( 4 + 8 * 14, 508 )
SKIN.Colours.Properties.Column_Selected		= GWEN.TextureColor( 4 + 8 * 15, 508 )
SKIN.Colours.Properties.Column_Hover		= GWEN.TextureColor( 4 + 8 * 14, 500 )
SKIN.Colours.Properties.Border				= GWEN.TextureColor( 4 + 8 * 15, 500 )
SKIN.Colours.Properties.Label_Normal		= GWEN.TextureColor( 4 + 8 * 16, 508 )
SKIN.Colours.Properties.Label_Selected		= GWEN.TextureColor( 4 + 8 * 17, 508 )
SKIN.Colours.Properties.Label_Hover			= GWEN.TextureColor( 4 + 8 * 16, 500 )

SKIN.Colours.Category = {}
SKIN.Colours.Category.Header				= GWEN.TextureColor( 4 + 8 * 18, 500 )
SKIN.Colours.Category.Header_Closed			= GWEN.TextureColor( 4 + 8 * 19, 500 )
SKIN.Colours.Category.Line = {}
SKIN.Colours.Category.Line.Text				= GWEN.TextureColor( 4 + 8 * 20, 508 )
SKIN.Colours.Category.Line.Text_Hover		= GWEN.TextureColor( 4 + 8 * 21, 508 )
SKIN.Colours.Category.Line.Text_Selected	= GWEN.TextureColor( 4 + 8 * 20, 500 )
SKIN.Colours.Category.Line.Button			= GWEN.TextureColor( 4 + 8 * 21, 500 )
SKIN.Colours.Category.Line.Button_Hover		= GWEN.TextureColor( 4 + 8 * 22, 508 )
SKIN.Colours.Category.Line.Button_Selected	= GWEN.TextureColor( 4 + 8 * 23, 508 )
SKIN.Colours.Category.LineAlt = {}
SKIN.Colours.Category.LineAlt.Text				= GWEN.TextureColor( 4 + 8 * 22, 500 )
SKIN.Colours.Category.LineAlt.Text_Hover		= GWEN.TextureColor( 4 + 8 * 23, 500 )
SKIN.Colours.Category.LineAlt.Text_Selected		= GWEN.TextureColor( 4 + 8 * 24, 508 )
SKIN.Colours.Category.LineAlt.Button			= GWEN.TextureColor( 4 + 8 * 25, 508 )
SKIN.Colours.Category.LineAlt.Button_Hover		= GWEN.TextureColor( 4 + 8 * 24, 500 )
SKIN.Colours.Category.LineAlt.Button_Selected	= GWEN.TextureColor( 4 + 8 * 25, 500 )

SKIN.Colours.TooltipText = GWEN.TextureColor( 4 + 8 * 26, 500 )

SKIN.colTextEntryText			= Color( 0, 0, 0, 255 );
SKIN.colTextEntryTextHighlight	= Color( 255, 255, 255, 10 );
SKIN.colTextEntryTextCursor		= Color( 255, 255, 255, 255 );

function SKIN:PaintPanel( panel, w, h )

end

--[[ local function FramePerformLayout( self )
	
	if( self.lblTitle and self.lblTitle:IsValid() ) then
		
		self.lblTitle:NoClipping( false )
		self.lblTitle:SetPos( 24, -11 )
		self.lblTitle:SetSize( self:GetWide() - 25, 22 )
		self.lblTitle:SetTextColor(Color(252, 178, 69, 255))
		
	end
	
end ]]

function SKIN:PaintFrame( panel, w, h )

 	surface.SetDrawColor( 15, 15, 15 );
	surface.DrawRect( 0, 0, w, h );

	surface.SetDrawColor( 100, 100, 100, 40 );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( 128, 128, 128, 128 );
	surface.DrawOutlinedRect( 0, 24, w, h - 24 );
	
	local nPaintFunc = kingston.gui.FindPaintFunc( panel );
	if( nPaintFunc ) then
	
		nPaintFunc( panel, w, h )
		
	end
	
end

local contextMenuBg = Material( "kingston/ui_common_contextmenu.png" );

function SKIN:LayoutMenu( panel, w, h ) -- PerformLayout is called every frame.

end

function SKIN:PaintMenu( panel, w, h )

	surface.SetDrawColor( Color( 255, 255, 255, 255 ) );
	surface.SetMaterial( contextMenuBg );
	surface.DrawTexturedRect( 0, 0, w, h );

end

local function PaintNotches( x, y, w, h, num )

	if ( !num ) then return end

	local space = w / num

	for i=0, num do

		surface.DrawRect( x + i * space, y + 4, 1, 5 )

	end

end

function SKIN:PaintNumSlider( panel, w, h )

	if( !panel.bSetup ) then
	
		-- fuck you garry
		panel.bSetup = true;
		panel:SetEnabled( false );
		panel.OnMousePressed = function() end;
		panel.OnMouseReleased = function() end;
		panel:GetParent().Scratch.OnMousePressed = function() end;
		panel:GetParent().Scratch.OnMouseReleased = function() end;
		
	end
		

	surface.SetDrawColor( Color( 255, 255, 255, 100 ) )
	surface.DrawRect( 8, h / 2 - 1, w - 15, 1 )

	PaintNotches( 8, h / 2 - 1, w - 16, 1, panel.m_iNotches )

end

--[[ function SKIN:PaintSlider( panel, w, h )

end ]]

-- i dont think DMenuOption has derma.SkinHook for PerformLayout. Neither does DButton or DLabel.
-- https://github.com/Facepunch/garrysmod/blob/master/garrysmod/lua/vgui/dmenuoption.lua#L120

function SKIN:PaintMenuOption( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetTextColor( Color( 172, 172, 172 ) );
		panel:SetFont( "Trebuchet18" );
		panel:SizeToContents();
		panel:Center();
	
	end
	
	if( panel:IsHovered() ) then
	
		surface.SetDrawColor( Color( 152, 152, 152, 32 ) );
		surface.SetFont( panel:GetFont() );
		local tW,tH = surface.GetTextSize( panel:GetText() );
		surface.DrawRect( 30, 2, tW, tH );
		
	end

end

local matBlurScreen = Material( "pp/blurscreen" );

function draw.DrawBlur( x, y, w, h, us, vs, ue, ve, frac )
	
	if( frac == 0 ) then return end
	
	surface.SetMaterial( matBlurScreen );
	surface.SetDrawColor( 255, 255, 255, 255 );
	
	for i = 1, 3 do
		
		matBlurScreen:SetFloat( "$blur", frac * 5 * ( i / 3 ) )
		matBlurScreen:Recompute();
		render.UpdateScreenEffectTexture();
		surface.DrawTexturedRectUV( x, y, w, h, us, vs, ue, ve );
		
	end

end

function draw.DrawDiagonals( col, x, y, w, h, density )
	
	surface.SetDrawColor( col );
	
	for i = 0, w + h, density do
		
		if( i < h ) then
			
			surface.DrawLine( x + i, y, x, y + i );
			
		elseif( i > w ) then
			
			surface.DrawLine( x + w, y + i - w, x - h + i, y + h );
			
		else
			
			surface.DrawLine( x + i, y, x + i - h, y + h );
			
		end
		
	end
	
end

function SKIN:PaintButtonDown( panel, w, h )
	
	if( !panel.LaidOut ) then
		
		panel.LaidOut = true;
		panel:SetText( "" );
		panel:SetFont( "marlett" );
		panel:SetSize( 6, 6 );
		panel:SetPos( 0, panel:GetParent():GetTall() - 6 );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintButtonUp( panel, w, h )
	
	if( !panel.LaidOut ) then
		
		panel.LaidOut = true;
		panel:SetText( "" );
		panel:SetFont( "marlett" );
		panel:SetSize( 6, 6 );
		panel:SetPos( 0, 0 );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintVScrollBar( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetSize( 10, panel:GetParent():GetTall() );
		
	end

end

function SKIN:PaintScrollBarGrip( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetSize( 10, panel:GetParent():GetTall() );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintTextEntry( panel, w, h )
	
	local w, h = panel:GetSize();
	
	if( panel:GetDrawBackground() ) then
		
		surface.SetDrawColor( 30, 30, 30, 255 );
		surface.DrawRect( 0, 0, w, h );
		
		surface.SetDrawColor( 40, 40, 40, 255 );
		surface.DrawRect( 1, 1, w - 2, h - 2 );
		
	end
	
	panel:DrawTextEntryText( panel:GetTextColor(), panel:GetHighlightColor(), panel:GetCursorColor() );
	
end

derma.DefineSkin( "StalkerUI", "STALKER UI Skin", SKIN );

local UVSkin = {};
UVSkin.InventoryFrame = {
	{
		mat = Material( "kingston/ui_actor_main" ),
		x = 0,
		y = 0,
		u0 = 0.667,
		v0 = 0,
		u1 = 1,
		v1 = 0.75,
	}
};

UVSkin.EquipFrame = {
	{ 	
		mat = Material( "kingston/ui_actor_main" ),
		x = 0,
		y = 0,
		u0 = 0.333,
		v0 = 0,
		u1 = 0.667,
		v1 = 0.75,
	}
};

UVSkin.CharCreateFrame = {
	{ 	
		mat = Material( "kingston/frame_panels" ),
		disableclip = true,
		x = 0,
		y = -20,
		u0 = 0.3,
		v0 = 0.42,
		u1 = 0.9,
		v1 = 0.82,
	},
};

UVSkin.TitleButtonUp = {
	{
		mat = Material( "kingston/hint_panels" ),
		disableclip = true,
		x = 0,
		y = 0,
		u0 = 0.095,
		v0 = 0.53,
		u1 = 0.218,
		v1 = 0.556,
	},
};

UVSkin.TitleButtonDown = {
	{
		mat = Material( "kingston/hint_panels" ),
		disableclip = true,
		x = 0,
		y = 0,
		u0 = 0.095,
		v0 = 0.561,
		u1 = 0.218,
		v1 = 0.586,
	},
};

UVSkin.Button = {
	{
		mat = Material( "kingston/multiplayer_panels" ),
		x = 0,
		y = 0,
		u0 = 0.42,
		v0 = 0.55,
		u1 = 0.523,
		v1 = 0.571,
	},
};

UVSkin.TechMenu = {
	{
		mat = Material( "kingston/inventory_panels" ),
		x = 0,
		y = 0,
		u0 = 0,
		v0 = 0,
		u1 = 0.33,
		v1 = 0.75,
	},
}

-- instead of having traditional like above
-- have 6 indices with each a precache of upgrade mats
UVSkin.UpgradeButton = {
	{
		mat = Material( "kingston/ui_actor_upgrades_1" ),
		x = 0,
		y = 0,
		u0 = 0,
		v0 = 0,
		u1 = 0.088,
		v1 = 0.043,
	},
}

UVSkin.UpgradeStates = {
	{ -- green has
		mat = Material( "kingston/ui_actor_upgrades_1" ),
		x = 3,
		y = 3,
		w = 8,
		u0 = 0.801,
		v0 = 0,
		u1 = 0.806,
		v1 = 0.0418,
	},
	{ -- yellow hover
		mat = Material( "kingston/ui_actor_upgrades_1" ),
		x = 3,
		y = 3,
		w = 8,
		u0 = 0.811,
		v0 = 0,
		u1 = 0.816,
		v1 = 0.0419,
	},
	{ -- red incompat
		mat = Material( "kingston/ui_actor_upgrades_1" ),
		x = 3,
		y = 3,
		w = 8,
		u0 = 0.807,
		v0 = 0,
		u1 = 0.809,
		v1 = 0.0419,
	},
}

function UVSkin:PaintDFrame( panel, w, h )

end

function UVSkin:PaintEquipFrame( panel, w, h )

	local skin = kingston.gui.classes[panel:GetSkin().Name];
	for k,v in next, skin.EquipFrame do
	
		surface.SetDrawColor( 255, 255, 255, 255 );
		
		if( v.mat ) then
			
			surface.SetMaterial( v.mat );
			
		end
		
		surface.DrawTexturedRectUV( v.x or 0, v.y or 0, w, h, v.u0, v.v0, v.u1, v.v1 );
	
	end

end

function UVSkin:PaintInventoryFrame( panel, w, h )

	local skin = kingston.gui.classes[panel:GetSkin().Name];
	for k,v in next, skin.InventoryFrame do
	
		surface.SetDrawColor( 255, 255, 255, 255 );
		
		if( v.mat ) then
			
			surface.SetMaterial( v.mat );
			
		end
		
		surface.DrawTexturedRectUV( v.x or 0, v.y or 0, w, h, v.u0, v.v0, v.u1, v.v1 );
	
	end

end

function UVSkin:PaintCharCreateFrame( panel, w, h )

	local skin = kingston.gui.classes[panel:GetSkin().Name];
	for k,v in next, skin.CharCreateFrame do
	
		surface.SetDrawColor( 255, 255, 255, 255 );
		
		if( v.mat ) then
			
			surface.SetMaterial( v.mat );
			
		end
		
		if( v.disableclip ) then
		
			DisableClipping( true );
			
		end
		
		surface.DrawTexturedRectUV( v.x or 0, v.y or 0, w + ( addw or 0 ), h + ( addh or 0 ), v.u0, v.v0, v.u1, v.v1 );
		
		if( v.disableclip ) then
		
			DisableClipping( false );
			
		end
	
	end

end

function UVSkin:PaintTitleButton( panel, w, h )

	local skin = kingston.gui.classes[panel:GetSkin().Name];
	if( !panel.m_bIsDown ) then
	
		for k,v in next, skin.TitleButtonUp do
		
			surface.SetDrawColor( 255, 255, 255, 255 );
			
			if( v.mat ) then
				
				surface.SetMaterial( v.mat, "noclamp smooth" );
				
			end
			
			if( v.disableclip ) then
			
				DisableClipping( true );
				
			end
			
			surface.DrawTexturedRectUV( v.x or 0, v.y or 0, v.w or w, v.h or h, v.u0, v.v0, v.u1, v.v1 );
			
			if( v.disableclip ) then
			
				DisableClipping( false );
				
			end
		
		end
		
	else
	
		for k,v in next, skin.TitleButtonDown do
		
			surface.SetDrawColor( 255, 255, 255, 255 );
			
			if( v.mat ) then
				
				surface.SetMaterial( v.mat, "noclamp smooth" );
				
			end
			
			if( v.disableclip ) then
			
				DisableClipping( true );
				
			end
			
			surface.DrawTexturedRectUV( v.x or 0, v.y or 0, v.w or w, v.h or h, v.u0, v.v0, v.u1, v.v1 );
			
			if( v.disableclip ) then
			
				DisableClipping( false );
				
			end
		
		end
	
	end

end

function UVSkin:PaintButton( panel, w, h )

	local skin = kingston.gui.classes[panel:GetSkin().Name];
	for k,v in next, skin.Button do
	
		surface.SetDrawColor( 255, 255, 255, 255 );
		
		if( v.mat ) then
			
			surface.SetMaterial( v.mat );
			
		end
		
		if( v.disableclip ) then
		
			DisableClipping( true );
			
		end
		
		surface.DrawTexturedRectUV( v.x or 0, v.y or 0, w + ( addw or 0 ), h + ( addh or 0 ), v.u0, v.v0, v.u1, v.v1 );
		
		if( v.disableclip ) then
		
			DisableClipping( false );
			
		end
	
	end

end

function UVSkin:PaintTechMenuFrame( panel, w, h )

	local skin = kingston.gui.classes[panel:GetSkin().Name];
	for k,v in next, skin.TechMenu do
	
		surface.SetDrawColor( 255, 255, 255, 255 );
		
		if( v.mat ) then
			
			surface.SetMaterial( v.mat );
			
		end
		
		surface.DrawTexturedRectUV( v.x or 0, v.y or 0, w, h, v.u0, v.v0, v.u1, v.v1 );
	
	end
	
end

local matUpgrades1 = Material( "kingston/ui_actor_upgrades_1" );
function UVSkin:PaintUpgradeButton( panel, w, h )

	local skin = kingston.gui.classes[panel:GetSkin().Name];
	local v = skin.UpgradeButton[1];
	local u0 = ( ( 0.088 * panel.nUpgradeX ) - ( ( 0.088 * panel.nUpgradeX ) / panel.nUpgradeX ) );
	local v0 = ( ( 0.043 * panel.nUpgradeY ) - ( ( 0.043 * panel.nUpgradeY ) / panel.nUpgradeY ) );
	local u1 = 0.088 * panel.nUpgradeX;
	local v1 = 0.043 * panel.nUpgradeY;

	surface.SetDrawColor( 255, 255, 255, 255 );
	
	if( v.mat ) then
		
		surface.SetMaterial( v.mat );
		
	end
	
	if( v.disableclip ) then
	
		DisableClipping( true );
		
	end
	
	surface.DrawTexturedRectUV( v.x or 0, v.y or 0, w + ( addw or 0 ), h + ( addh or 0 ), u0, v0, u1, v1 );
	
	if( v.disableclip ) then
	
		DisableClipping( false );
		
	end
	
	if( !panel:IsEnabled() and !panel.ItemHasUpgrade ) then
	
		surface.SetDrawColor( 0, 0, 0, 200 );
		surface.DrawRect( v.x or 0, v.y or 0, w + ( addw or 0 ), h + ( addh or 0 ) );
		
	end
	
	if( panel:IsHovered() and panel:IsEnabled() and !panel.ItemHasUpgrade ) then
	
		local v = skin.UpgradeStates[2];
		surface.SetDrawColor( 255, 255, 255, 255 );
	
		if( v.mat ) then
			
			surface.SetMaterial( v.mat );
			
		end
		
		if( v.disableclip ) then
		
			DisableClipping( true );
			
		end
		
		surface.DrawTexturedRectUV( v.x or 0, v.y or 0, v.w or w, v.h or h, v.u0, v.v0, v.u1, v.v1 );
		
		if( v.disableclip ) then
		
			DisableClipping( false );
			
		end
	
	end
	
	if( panel.ItemHasUpgrade ) then
	
		local v = skin.UpgradeStates[1];
		surface.SetDrawColor( 255, 255, 255, 255 );
	
		if( v.mat ) then
			
			surface.SetMaterial( v.mat );
			
		end
		
		if( v.disableclip ) then
		
			DisableClipping( true );
			
		end
		
		surface.DrawTexturedRectUV( v.x or 0, v.y or 0, v.w or w, v.h or h, v.u0, v.v0, v.u1, v.v1 );
		
		if( v.disableclip ) then
		
			DisableClipping( false );
			
		end
	
	end
	
	if( panel.UpgradeIncompatible ) then
	
		local v = skin.UpgradeStates[3];
		surface.SetDrawColor( 255, 255, 255, 255 );
	
		if( v.mat ) then
			
			surface.SetMaterial( v.mat );
			
		end
		
		if( v.disableclip ) then
		
			DisableClipping( true );
			
		end
		
		surface.DrawTexturedRectUV( v.x or 0, v.y or 0, v.w or w, v.h or h, v.u0, v.v0, v.u1, v.v1 );
		
		if( v.disableclip ) then
		
			DisableClipping( false );
			
		end
		
	end

end

kingston.gui.RegisterSkin( "StalkerSkin", UVSkin );