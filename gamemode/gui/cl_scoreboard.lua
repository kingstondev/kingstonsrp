local PANEL = { };

function PANEL:Init()
	
	local y = 50;
	for _, v in pairs( player.GetAll() ) do
	
		if( v:CharID() > -1 ) then
		
			local a = vgui.Create( "CCharPanel", self );
			a:SetPos( 0, y );
			a:SetSize( 84, 84 );
			a.Entity:SetAngles( Angle() );
			a:SetCamPos( Vector( 50, 20, 67 ) );
			a:SetLookAtEyes();
			a:SetFOV( 15 );
			a:SetModel( "models/error.mdl" );
			a:SetModel( v:GetModel() );
			a:LayoutEntity( a.Entity );
			a.Head = a:InitializeModel( v:Head(), a.Entity );
			a.NoMouseWheel = true;
			
			function a:DoClick()
				
				if( v:SteamID64() ) then
					
					gui.OpenURL( "http://steamcommunity.com/profiles/" .. v:SteamID64() .. "/" );
					
				end
				
			end
			
			y = y + 84;
			
		end
		
	end
	
end

function PANEL:Paint( w, h )
	
	local y = 50;
	local i = 0;
	
	for _, v in pairs( player.GetAll() ) do
	
		if( v:CharID() > -1 ) then
		
			if( i % 2 == 0 ) then
				
				draw.RoundedBox( 2, 0, y, w - 2, 84, Color( 0, 0, 0, 120 ) );
				
			end
			
			i = i + 1;
			
			surface.SetFont( "Stalker2-24" );
			surface.SetTextColor( Color( 255, 255, 255, 255 ) );
			surface.SetTextPos( 84 + 10, y + 4 );
			surface.DrawText( v:RPName() );
			
			local wh, hh = surface.GetTextSize( v:Ping() );
			surface.SetTextPos( 500 - wh - 12 - 10, y + 24 );
			surface.DrawText( v:Ping() );
			
			surface.SetFont( "Stalker2-16" );
			surface.SetTextPos( 84 + 10, y + 4 + 30 );
			surface.DrawText( v:Overhead1() );
			
			surface.SetTextPos( 84 + 10, y + 4 + 30 + 20 );
			surface.DrawText( v:Nick() );

			
			y = y + 84;
			
		end
		
	end
	
	self:SetTall( y );
	GAMEMODE.Scoreboard.Scroll:PerformLayout();

end

derma.DefineControl( "IScoreboard", "", PANEL, "EditablePanel" );