local PANEL = { };

function PANEL:Init()
	
	self.ContentScroll = vgui.Create( "DScrollPanel", self );
	self.ContentScroll:SetPos( 10, 40 );
	self.ContentScroll:SetSize( GAMEMODE.ChatWidth - 20, GAMEMODE.ChatHeight - 80 );
	function self.ContentScroll:Paint( w, h )
		
		surface.SetDrawColor( 0, 0, 0, 70 );
		surface.DrawRect( 0, 0, w, h );
		
		surface.SetDrawColor( 0, 0, 0, 100 );
		surface.DrawOutlinedRect( 0, 0, w, h );
		
	end
	
	self.RadioSelection = vgui.Create( "DScrollPanel" )
	self.RadioSelection:SetPos( 20 + GAMEMODE.ChatWidth + 4, ScrH() - 200 - 34 - GAMEMODE.ChatHeight );
	self.RadioSelection:SetSize( ScrW() / 11, ScrH() / 4 );
	self.RadioSelection.Buttons = {};
	
	if( LocalPlayer().ActiveRadios ) then
		
		local y = 2;
		for k,v in next, LocalPlayer().ActiveRadios do
		
			local btn = vgui.Create( "DButton", self.RadioSelection );
			btn:SetSize( 128, 32 );
			btn:SetPos( ( ScrW() / 11 )/2 - ( 128/2 ), y );
			btn:SetText( v:GetVar( "Frequency", 100.1 ) );
			btn:SetIsToggle( true );
			btn:SetToggle( v:GetVar( "PrimaryRadio", false ) );
			btn.DoClick = function( panel )
			
				panel:Toggle();
				-- this is pretty rough nested crap
				for _,btn in next, self.RadioSelection.Buttons do
				
					if( btn != panel and btn:GetToggle() ) then
						
						btn:SetToggle( false );
						LocalPlayer().ActiveRadios[btn.item]:SetVar( "PrimaryRadio", false );
						netstream.Start( "nSetPrimaryRadio", btn.item, false );
						
					end
					
				end

				v:SetVar( "PrimaryRadio", panel:GetToggle() );
				netstream.Start( "nSetPrimaryRadio", v:GetID(), panel:GetToggle() );
			
			end
			
			y = y + 34;
			btn.item = k;
			self.RadioSelection.Buttons[k] = btn;
		
		end
		
	end
	
	self.Content = vgui.Create( "EditablePanel" );
	self.Content:SetPos( 0, 0 );
	self.Content:SetSize( GAMEMODE.ChatWidth - 20, 20 );
	self.Content.Paint = function( content, pw, ph )
		
		local y = 0;
		
		for k, v in next, GAMEMODE.ChatLines do
			
			local start = v[1];
			local filter = v[2];
			local font = v[3];
			local text = v[4];
			
			if( GAMEMODE.ChatboxFilter[filter] ) then
				
				if( !v.MarkupObjCreated ) then
				
					v.MarkupObjCreated = true;
					v.obj = king.markup.parse( text, GAMEMODE.ChatWidth - 20 - 16 );
					v.obj.onDrawText = function( text, font, x, y, colour, halign, valign, alphaoverride, blk )
					
						if (valign == TEXT_ALIGN_CENTER) then	
						
							y = y - (v.obj.totalHeight / 2)
							
						elseif (valign == TEXT_ALIGN_BOTTOM) then	
						
							y = y - (v.obj.totalHeight)
							
						end
						
						local alpha = blk.colour.a
						if (alphaoverride) then alpha = alphaoverride end

						surface.SetFont( font )
						surface.SetTextColor( 0, 0, 0, alpha );
						surface.SetTextPos( x + 1, y + 1 );
						surface.DrawText( text );
						surface.SetTextColor( colour.r, colour.g, colour.b, alpha )
						surface.SetTextPos( x, y )
						surface.DrawText( text )
					
					end;
					
				end
				if( !v.obj ) then return end
				v.obj:draw( 2, y, nil, nil );
				
				y = y + v.obj:getHeight();
				
			end
			
		end
		
		if( ph != math.max( y, 20 ) ) then
			
			content:SetTall( math.max( y, 20 ) );
			self.ContentScroll:PerformLayout();
			
			if( self.ContentScroll.VBar ) then
				
				self.ContentScroll.VBar:SetScroll( math.huge );
				
			end
			
		end
		
	end
	
	self.ContentScroll:AddItem( self.Content );
	
end

function PANEL:Paint( w, h )
	
	local x1, y1, w1, h1 = self:GetBounds();
	
	local us = x1 / ScrW();
	local vs = y1 / ScrH();
	local ue = ( x1 + w1 ) / ScrW();
	local ve = ( y1 + h1 ) / ScrH();
	
	draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 200 ) );
	
	return true

end

derma.DefineControl( "CChatPanel", "", PANEL, "EditablePanel" );