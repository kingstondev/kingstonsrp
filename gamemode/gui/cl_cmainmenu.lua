--- MainMenuVGUI ---

local PANEL = {}
local stalkerLogo = Material( "kingston/stalker_logo.png" );
local bgVideos = {
	//"http://kingston-rp.org/files/videos/killer_menu.webm",
	"http://kingston-rp.org/files/videos/blood_menu.webm",
	//"http://kingston-rp.org/files/videos/stalker_menu.webm",
};

function PANEL:Init()

	GAMEMODE.MainMenu = self;

	local link = table.Random( bgVideos );
	local service = medialib.load( "media" ).guessService( link );
	self.mediaclip = service:load( link );
	self.mediaclip:setQuality( "veryhigh" );
	self.mediaclip:play();
	
	self.w, self.h = ScrW( ), ScrH( );
	self.x, self.y = ScrW( ) / 2 - self.w / 2, ScrH( ) / 2 - self.h / 2;
	
	self:SetSize( self.w, self.h );
	self:SetPos( self.x, self.y );
	self:SetTitle( "" );
	self:MakePopup( );
	self:ShowCloseButton( false );
	self:SetAlpha( 0 );
	self:AlphaTo( 255, 0.2, 0 );
	self:SetDraggable( false );
	self:SetPopupStayAtBack( true );
	
	self.MainPanel = vgui.Create( "DFrame", self );
	self.MainPanel:SetSize( ScrW() * 0.18, ScrH() );
	self.MainPanel:SetPos( ScrW() * 0.65, 0 );
	self.MainPanel:SetTitle( "" );
	self.MainPanel:MakePopup( );
	self.MainPanel:ShowCloseButton( false );
	self.MainPanel:SetAlpha( 0 );
	self.MainPanel:AlphaTo( 255, 0.2, 0 );
	self.MainPanel:SetDraggable( false );
	
	surface.SetFont( "AmazStalker128" );
	local iPanelW = ScrW() * 0.18;
	local iTextW, iTextH = surface.GetTextSize( "S.T.A.L.K.E.R" );
	local x = iPanelW / 2 - iTextW / 2;
	
	function self.MainPanel:Paint( w, h )
	
		surface.SetDrawColor( 20, 20, 20, 125 );
		surface.DrawRect( 0, 0, w, h );
		surface.SetDrawColor( 255, 255, 255, 10 );
		surface.DrawLine( 0, 0, 0, h );
		surface.DrawLine( w - 1, 0, w - 1, h );
		surface.SetTextColor( 255, 255, 255 );
		surface.SetFont( "AmazStalker128" );
		surface.SetTextPos( x, 20 );
		surface.DrawText( "S.T.A.L.K.E.R" );
	
	end
	
	self:PopulateMainMenu();
	
end

function PANEL:PopulateCharacterLoad()

	local panel = self.MainPanel;
	local y = panel:GetTall() * 0.4;
	local CharButtons = {};
	
	for k,v in next, GAMEMODE.g_CharacterTable[LocalPlayer():SteamID()] do
	
		local button = vgui.Create( "DButton", panel );
		button:SetSize( ScrW() * 0.18, 32 );
		button:SetPos( 0, y );
		button:SetFont( "Stalker2-MainMenuFont" );
		button:SetTextColor( Color( 255, 255, 255, 255 ) );
		button:SetText( v.RPName );
		function button:DoClick()
		
			netstream.Start( "LoadCharacter", v.id );
			panel:GetParent():Close();
			
		end
		function button:Paint()
		
		end
		if( tobool( v.Disabled ) ) then
		
			button:SetEnabled( false );
			
		end
		if( v.id == LocalPlayer():CharID() ) then
		
			button:SetEnabled( false );
			
		end
		
		y = y + 48;
		
		table.insert( CharButtons, button );
		
	end
	
	local backButton = vgui.Create( "DButton", panel );
	backButton:SetSize( ScrW() * 0.18, 32 );
	backButton:SetPos( 0, y );
	backButton:SetFont( "Stalker2-MainMenuFont" );
	backButton:SetTextColor( Color( 255, 255, 255, 255 ) );
	backButton:SetText( "Cancel" );
	function backButton:DoClick()
	
		for m,n in next, CharButtons do
		
			n:Remove();
		
		end
		
		self:Remove();
		panel:GetParent():PopulateMainMenu();
		
	end
	function backButton:Paint()
	
	end


end

function PANEL:PopulateMainMenu()

	local panel = self.MainPanel;
	local y = panel:GetTall() * 0.4;
	
	if( LocalPlayer():CharID() > -1 ) then
	
		panel.Continue = vgui.Create( "DButton", self.MainPanel );
		panel.Continue:SetSize( ScrW() * 0.18, 32 );
		panel.Continue:SetPos( 0, y );
		panel.Continue:SetFont( "Stalker2-MainMenuFont" );
		panel.Continue:SetTextColor( Color( 255, 255, 255, 255 ) );
		panel.Continue:SetText( "Continue" );
		function panel.Continue:DoClick()
		
			GAMEMODE.MainMenu:Close();
			
		end
		function panel.Continue:Paint()
		
		end
		
		y = y + 32 + 16;
		
	end
	
	panel.New = vgui.Create( "DButton", self.MainPanel );
	panel.New:SetSize( ScrW() * 0.18, 32 );
	panel.New:SetPos( 0, y );
	panel.New:SetFont( "Stalker2-MainMenuFont" );
	panel.New:SetTextColor( Color( 255, 255, 255, 255 ) );
	panel.New:SetText( "New character" );
	function panel.New:DoClick()
	
		GAMEMODE.MainMenu:Close();
		vgui.Create( "CharacterCreateVGUI" );
		
	end
	function panel.New:Paint()
	
	end
	
	y = y + 32 + 16;
	
	panel.Load = vgui.Create( "DButton", panel );
	panel.Load:SetSize( ScrW() * 0.18, 32 );
	panel.Load:SetPos( 0, y );
	panel.Load:SetFont( "Stalker2-MainMenuFont" );
	panel.Load:SetTextColor( Color( 255, 255, 255, 255 ) );
	panel.Load:SetText( "Load character" );
	function panel.Load:DoClick()
		
		self:GetParent().New:Remove();
		if( self:GetParent().Continue ) then
		
			self:GetParent().Continue:Remove();
			
		end
		self:Remove();
		panel:GetParent():PopulateCharacterLoad();
		
	end
	function panel.Load:Paint()
	
	end
	if( #GAMEMODE.g_CharacterTable[LocalPlayer():SteamID()] == 0 ) then
		
		panel.Load:SetDisabled( true );
		
	end

end

function PANEL:Paint( w, h )

	if( self.mediaclip and IsValid( self.mediaclip ) ) then

		self.mediaclip:draw( 0, 0, w, h );
		
	end

	surface.SetDrawColor( 255, 255, 255 );
	surface.DrawRect( 0, h, w, 25 );
	
--[[ 	surface.SetMaterial( stalkerLogo );
	surface.DrawTexturedRect( 148, 20, 512, 256 ); ]]
	
end

function PANEL:Close()

	GAMEMODE.MainMenu = nil;
	self.mediaclip:stop();
	self:Remove();
	self = nil;

end

vgui.Register( "MainMenuVGUI", PANEL, "DFrame" );