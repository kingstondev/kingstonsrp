local PANEL = {};

function PANEL:Init()

	self:SetText( "" );

end

function PANEL:DoClick()

	-- parent is TechMenu.Container
	local ItemObj = self.CurrentItem;
	if( ItemObj ) then
	
		local upgrade = GAMEMODE.Upgrades[self.UpgradeID];
		if( upgrade.CanUpgrade( upgrade, ItemObj ) ) then

			upgrade.OnUpgrade( upgrade, ItemObj );
			netstream.Start( "RequestItemUpgrade", ItemObj:GetID(), self.UpgradeID );
			self.ItemHasUpgrade = true;
			GAMEMODE.Inventory.TechMenu:SendToMenu( ItemObj ); -- ghetto refresh technique.
			
			-- loop through all upgrade buttons and update as upgrade circumstances change
			
		end
	
	end

end

-- since all the upgrades are a bunch of images stitched together in a single file.
function PANEL:SetUpgradeImage( nX, nY )

	self.nUpgradeX = nX;
	self.nUpgradeY = nY;

end

function PANEL:Paint( w, h )

	kingston.gui.FindFunc( self, "Paint", "UpgradeButton" );
	
	if( self:IsHovered() ) then
	
		hook.Run( "PaintUpgradeToolTip", self, w, h );
	
	end

end

vgui.Register( "CUpgradeButton", PANEL, "DButton" );