local PANEL = {};

function PANEL:Init()

	GAMEMODE.CharacterCard = self;
	
	self:SetSize( ScrW() / 2.5, ScrH() / 1.2 );
	self:Center();
	self:SetTitle( "" );
	self:MakePopup( );
	self:ShowCloseButton( true );
	self:SetAlpha( 0 );
	self:AlphaTo( 255, 0.2, 0 );
	self:SetDraggable( false );

	self.CP = vgui.Create( "CCharPanel", self );
	self.CP:SetPos( 0, 24 );
	self.CP:SetSize( self:GetWide() / 2.4, self:GetTall() / 1.4 );
	self.CP:SetModel( "models/error.mdl" );
	self.CP:SetFOV( 30 );
	self.CP.HeadFollowMouse = true;
	self.CP:SetCamPos( Vector( 50, 0, 64 ) );
	self.CP:SetLookAt( Vector( 0, 0, 50 ) );
	
	self.BodygroupTemp = vgui.Create( "DLabel", self );
	self.BodygroupTemp:SetPos( 4, self:GetTall() / 1.4 + 26 );
	self.BodygroupTemp:SetSize( 256, 64 );
	self.BodygroupTemp:SetFont( "Stalker2-24" );
	self.BodygroupTemp:SetTextColor( Color( 255, 255, 255, 255 ) );
	self.BodygroupTemp:SetText( "BODYGROUPS" );
	
	local y = 32;
	
	self.RPName = vgui.Create( "DTextEntry", self );
	self.RPName:SetSize( self:GetWide() - self:GetWide() / 2.4 - 8, 32 );
	self.RPName:SetPos( self:GetWide() / 2.4 + 2, y );
	self.RPName:SetFont( "Stalker2-16" );
	self.RPName:SetValue( "Name" );
	self.RPName:SetTextColor( Color( 0, 0, 0, 255 ) );
	function self.RPName:OnEnter()
	
		netstream.Start( "ChangeName", self:GetValue() );
	
	end
	
--[[ 	self.Title1Lbl = vgui.Create( "DLabel", self );
	self.Title1Lbl:SetPos( self:GetWide() / 2.4 + 8, 24 );
	self.Title1Lbl:SetSize( 256, 64 );
	self.Title1Lbl:SetFont( "Stalker2-24" );
	self.Title1Lbl:SetTextColor( Color( 255, 255, 255, 255 ) );
	self.Title1Lbl:SetText( "Title 1:" ); ]]
	
	y = y + 32 + 8; -- why add the two figures together when now you can easily edit the distance?
	
	self.Title1 = vgui.Create( "DTextEntry", self );
	self.Title1:SetSize( self:GetWide() - self:GetWide() / 2.4 - 8, 32 );
	self.Title1:SetPos( self:GetWide() / 2.4 + 2, y );
	self.Title1:SetFont( "Stalker2-16" );
	self.Title1:SetValue( "Title1" );
	self.Title1:SetTextColor( Color( 0, 0, 0, 255 ) );
	function self.Title1:OnTextChanged()
	
		local txt = self:GetValue()
		if( #txt > 64 ) then

			self:SetValue( self.OldText );
			self:SetText( self.OldText );
			
		else
		
			self.OldText = txt;
			
		end
		
	end
	function self.Title1:OnEnter()
	
		if( #self:GetValue() <= 64 ) then
	
			netstream.Start( "ChangeTitle1", self:GetValue() );
			
		end
	
	end
	
	y = y + 32 + 8;
	
	self.Title2 = vgui.Create( "DTextEntry", self );
	self.Title2:SetSize( self:GetWide() - self:GetWide() / 2.4 - 8, 32 );
	self.Title2:SetPos( self:GetWide() / 2.4 + 2, y );
	self.Title2:SetFont( "Stalker2-16" );
	self.Title2:SetValue( "Title2" );
	self.Title2:SetTextColor( Color( 0, 0, 0, 255 ) );
	function self.Title2:OnTextChanged()
	
		local txt = self:GetValue()
		if( #txt > 64 ) then

			self:SetValue( self.OldText );
			self:SetText( self.OldText );
			
		else
		
			self.OldText = txt;
			
		end
		
	end
	function self.Title2:OnEnter()
	
		if( #self:GetValue() <= 64 ) then
	
			netstream.Start( "ChangeTitle2", self:GetValue() );
			
		end
	
	end
	
	y = y + 32 + 8;
	
	self.Description = vgui.Create( "DTextEntry", self );
	self.Description:SetSize( self:GetWide() - self:GetWide() / 2.4 - 8, ScrW() / 3.9 );
	self.Description:SetPos( self:GetWide() / 2.4 + 2, y );
	self.Description:SetMultiline( true );
	self.Description:SetFont( "Stalker2-16" );
	self.Description:SetTextColor( Color( 0, 0, 0, 255 ) );
	self.Description:SetValue( "Description" );
	function self.Description:OnEnter()

		netstream.Start( "ChangeDescription", self:GetValue() );
	
	end
	
	y = y + ScrW() / 3.9 + 2;
	
	self.MainMenu = vgui.Create( "DButton", self );
	self.MainMenu:SetSize( self:GetWide() - self:GetWide() / 2.4 - 8, 32 );
	self.MainMenu:SetPos( self:GetWide() / 2.4 + 2, y );
	self.MainMenu:SetFont( "Stalker2-24" );
	self.MainMenu:SetText( "Main Menu" );
	self.MainMenu.DoClick = function( pnl )
	
		self:Close();
		vgui.Create( "MainMenuVGUI" );
	
	end
	
end

function PANEL:SetPlayer( ply )

	self.Player = ply;
	self.CP:SetModel( ply:GetModel() );
	self.Head = self.CP:InitializeModel( ply:Head(), self.CP.Entity );
	self.CP:LayoutEntity( self.CP.Entity );
	self.CP:StartAnimation( "idle" );
	self.RPName:SetText( ply:RPName() );
	self.Title1:SetText( ply:Overhead1() );
	self.Title2:SetText( ply:Overhead2() );
	self.Description:SetText( ply:Description() );
	self.Description.OriginalText = ply:Description();
	if( ply != LocalPlayer() ) then
	
		self.RPName:SetEditable( false );
		self.Title1:SetEditable( false );
		self.Title2:SetEditable( false );
		self.Description:SetEditable( false );
		
	end

end

function PANEL:OnClose()

	if( self.Player == LocalPlayer() ) then
	
		if( self.Description.OriginalText != self.Description:GetValue() ) then

			netstream.Start( "ChangeDescription", self.Description:GetValue() );
			
		end
	
	end

	GAMEMODE.CharacterCard = nil;

end

derma.DefineControl( "CCharacterCard", "", PANEL, "DFrame" );