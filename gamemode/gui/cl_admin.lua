local PANEL = {};

local function PanelPaint( panel, w, h )

end

GM.AdminMenuContexts = {};

function PANEL:Init()

	self:SetSize( ScrW() / 1.5, ScrH() / 1.5 );
	self:SetTitle( "Admin Menu" );
	self:Center();
	self:MakePopup();
	//self:SetSkin( "AdminUI" );
	
	self.SelectionSheet = vgui.Create( "DPropertySheet", self );
	self.SelectionSheet:Dock( FILL );
	
	self.General = vgui.Create( "DPanel", self.SelectionSheet );
	self.General.Paint = PanelPaint; -- yuck
	self.Players = vgui.Create( "DPanel", self.SelectionSheet );
	self.Players.Paint = PanelPaint;
	self.Character = vgui.Create( "DPanel", self.SelectionSheet );
	self.Character.Paint = PanelPaint;
	self.Environment = vgui.Create( "DPanel", self.SelectionSheet );
	self.Environment.Paint = PanelPaint;
	self.Permissions = vgui.Create( "DPanel", self.SelectionSheet );
	self.Permissions.Paint = PanelPaint;
	self.Server = vgui.Create( "DPanel", self.SelectionSheet );
	self.Server.Paint = PanelPaint;
	
	-- AddSheet Area. We create all panels first.
	self.SelectionSheet:AddSheet( "General", self.General, "icon16/computer.png" );
	self:CreatePlayerMenu();
	self:CreateCharacterMenu();
	self.SelectionSheet:AddSheet( "Environment", self.Environment, "icon16/world.png" );
	self:CreatePermissionsMenu();
	self.SelectionSheet:AddSheet( "Server", self.Server, "icon16/server.png" );

end

/*
	Player menu start
*/

GM.AdminMenuContexts["Players"] = {
	{
		text = "Kick",
		icon = "icon16/controller_delete.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:KickPlayer( pnl.Players.List:GetSelectedLine() );
		end,
	},
	{
		text = "Ban",
		icon = "icon16/clock_error.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:BanPlayer( pnl.Players.List:GetSelectedLine() );
		end,
	},
	{
		text = "Change character name",
		icon = "icon16/pencil.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:ChangeCharName( pnl.Players.List:GetSelectedLine() );
		end,
	},
	{
		text = "Bring player",
		icon = "icon16/user_go.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:BringPlayer( pnl.Players.List:GetSelectedLine() );
		end,
	},
	{
		text = "Go to player",
		icon = "icon16/user_go.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:GotoPlayer( pnl.Players.List:GetSelectedLine() );
		end,
	},
	{
		text = "Set rank",
		icon = "icon16/group_add.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:SetRank( pnl.Players.List:GetSelectedLine() );
		end,
	},
	{
		text = "Set faction",
		icon = "icon16/package_add.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:SetFaction( pnl.Players.List:GetSelectedLine() );
		end,
	},
	{
		text = "Disable Current Character",
		icon = "icon16/delete.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:DisableCharacter( pnl.Players.List:GetSelectedLine() );
		end,
	},
	-- set phystrust
	-- set tooltrust
	-- set proptrust
	-- give item
};

function PANEL:CreatePlayerMenu()

	self.SelectionSheet:AddSheet( "Players", self.Players, "icon16/status_online.png" );
	self.Players:Clear();
	
	self.Players.List = vgui.Create( "DListView", self.Players );
	self.Players.List:SetWide( self:GetWide() / 2 );
	self.Players.List:Dock( FILL );
	self.Players.List:SetMultiSelect( false );
	self.Players.List:AddColumn( "Name" );
	self.Players.List:AddColumn( "Character" );
	self.Players.List:AddColumn( "SteamID" );
	self.Players.List:AddColumn( "Flags" );
	self.Players.List:AddColumn( "Phys Trust" );
	self.Players.List:AddColumn( "Prop Trust" );
	self.Players.List:AddColumn( "Tool Trust" );
	self.Players.List:AddColumn( "Rank" );
	self.Players.List.OnRowRightClick = function( panel, nLineID, LinePanel )
	
		self:CreatePlayerContextMenu();
	
	end
	
	for k,v in next, player.GetAll() do
	
		local line = self.Players.List:AddLine(
			v:Nick(),
			v:RPName(),
			v:SteamID(),
			v:PlayerGetFlags(),
			v:PhysTrust(),
			v:PropTrust(),
			v:ToolTrust(),
			v:GetUserGroup()
		);
		line.Player = v;
	
	end
	
	self.Players.ActiveContainer = vgui.Create( "DPanel", self.Players );
	self.Players.ActiveContainer:SetWide( self:GetWide() / 2 );
	self.Players.ActiveContainer.Paint = PanelPaint;
	self.Players.ActiveContainer:DockMargin( 4, 0, 0, 0 );
	self.Players.ActiveContainer:Dock( RIGHT );

end

function PANEL:KickPlayer( nLineID )

	local line = self.Players.List:GetLine( nLineID );
	RunConsoleCommand( "rpa_kick", line.Player:Nick() );

end

function PANEL:SetRank( nLineID )

	local panel = self.Players.ActiveContainer;
	local line = self.Players.List:GetLine( nLineID );
	panel:Clear();
	
	panel.Ranks = vgui.Create( "DListView", panel );
	panel.Ranks:Dock( FILL );
	panel.Ranks:AddColumn( "Rank" );
	panel.Ranks:SetMultiSelect( false );
	
	for k,v in next, GAMEMODE.Ranks do
	
		panel.Ranks:AddLine( k );
		
	end
	
	panel.Done = vgui.Create( "DButton", panel );
	panel.Done:DockMargin( 0, 4, 0, 0 );
	panel.Done:Dock( BOTTOM );
	panel.Done:SetText( "Set Rank" );
	function panel.Done:DoClick()
	
		local rank = panel.Ranks:GetLine( panel.Ranks:GetSelectedLine() ):GetColumnText( 1 );
		RunConsoleCommand( "rpa_setrank", line.Player:Nick(), rank );
		panel:Clear();
	
	end

end

function PANEL:GotoPlayer( nLineID )

	local line = self.Players.List:GetLine( nLineID );
	RunConsoleCommand( "rpa_goto", line.Player:Nick() );

end

function PANEL:BringPlayer( nLineID )

	local line = self.Players.List:GetLine( nLineID );
	RunConsoleCommand( "rpa_bring", line.Player:Nick() );

end

function PANEL:ChangeCharName( nLineID )

	local panel = self.Players.ActiveContainer;
	local line = self.Players.List:GetLine( nLineID );
	panel:Clear();
	
	panel.ChangeName = vgui.Create( "DTextEntry", panel );
	panel.ChangeName:Dock( TOP );
	panel.ChangeName:SetText( line.Player:RPName() );
	
	panel.Done = vgui.Create( "DButton", panel );
	panel.Done:DockMargin( 0, 4, 0, 0 );
	panel.Done:Dock( TOP );
	panel.Done:SetText( "Change Name" );
	function panel.Done:DoClick()
	
		RunConsoleCommand( "rpa_setcharname", line.Player:Nick(), panel.ChangeName:GetText() );
		panel:Clear();
	
	end
	
end

function PANEL:BanPlayer( nLineID )

	local line = self.Players.List:GetLine( nLineID );
	local panel = self.Players.ActiveContainer;
	panel:Clear();
	
	panel.BanReason = vgui.Create( "DTextEntry", panel );
	panel.BanReason:Dock( TOP );
	panel.BanReason:SetText( "Ban Reason" );
	
	panel.BanLength = vgui.Create( "DNumberWang", panel );
	panel.BanLength:DockMargin( 0, 4, 0, 0 );
	panel.BanLength:SetWide( 128 );
	panel.BanLength:Dock( TOP );
	panel.BanLength:SetDecimals( 0 );
	panel.BanLength:SetMin( 0 );
	panel.BanLength:SetValue( 1 );
	
	panel.Done = vgui.Create( "DButton", panel );
	panel.Done:DockMargin( 0, 4, 0, 0 );
	panel.Done:Dock( TOP );
	panel.Done:SetText( "Ban" );
	function panel.Done:DoClick()
	
		local ply = line.Player;
		RunConsoleCommand( "rpa_ban", ply:Name(), panel.BanLength:GetValue(), panel.BanReason:GetText() );
		panel:Clear();
	
	end
	
	panel.Cancel = vgui.Create( "DButton", panel );
	panel.Cancel:DockMargin( 0, 4, 0, 0 );
	panel.Cancel:Dock( TOP );
	panel.Cancel:SetText( "Cancel" );
	function panel.Cancel:DoClick()
	
		panel:Clear();
	
	end
	
end

function PANEL:SetFaction( nLineID )

	local panel = self.Players.ActiveContainer;
	local line = self.Players.List:GetLine( nLineID );
	panel:Clear();
	
	panel.Factions = vgui.Create( "DListView", panel );
	panel.Factions:Dock( FILL );
	panel.Factions:AddColumn( "Faction" );
	panel.Factions:SetMultiSelect( false );
	
	for k,v in next, GAMEMODE.Factions do
	
		panel.Factions:AddLine( v.identifier );
		
	end
	
	panel.Done = vgui.Create( "DButton", panel );
	panel.Done:DockMargin( 0, 4, 0, 0 );
	panel.Done:Dock( BOTTOM );
	panel.Done:SetText( "Set faction" );
	function panel.Done:DoClick()
	
		local faction = panel.Factions:GetLine( panel.Factions:GetSelectedLine() ):GetColumnText( 1 );
		RunConsoleCommand( "rpa_setfaction", line.Player:Nick(), faction );
		panel:Clear();
	
	end

end

function PANEL:DisableCharacter( nLineID )

	local line = self.Players.List:GetLine( nLineID );
	RunConsoleCommand( "rpa_setchardisabled", line.Player:Nick() );

end

function PANEL:CreatePlayerContextMenu()

	local ContextMenu = DermaMenu();
	for k,v in next, GAMEMODE.AdminMenuContexts["Players"] do
	
		if( v.submenu ) then
		
			local submenu,parent = ContextMenu:AddSubMenu( v.text );
			parent:SetIcon( v.icon );
			for m,n in next, v.options do
			
				local option = submenu:AddOption( n.text, n.func );
				option:SetIcon( n.icon );
				
			end
			
			continue;
			
		end
		
		if( v.display and !v.display() ) then continue end
	
		local option = ContextMenu:AddOption( v.text, v.func );
		option:SetIcon( v.icon );
		
	end
	ContextMenu:Open();
	
end

/* 
	Player menu end
*/

/* 
	Character menu start
*/

GM.AdminMenuContexts["Characters"] = {
	{
		text = "Disable Character",
		icon = "icon16/delete.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:SetCharacterDisable( pnl.Character.List:GetSelectedLine(), 1 );
		end,
		display = function()
			local pnl = GAMEMODE.AdminMenu;
			return ( tonumber( pnl.Character.List:GetLine( pnl.Character.List:GetSelectedLine() ):GetColumnText( 5 ) ) == 0 ); -- puke
		end,
	},
	{
		text = "Enable Character",
		icon = "icon16/add.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:SetCharacterDisable( pnl.Character.List:GetSelectedLine(), 0 );
		end,
		display = function()
			local pnl = GAMEMODE.AdminMenu;
			return ( tonumber( pnl.Character.List:GetLine( pnl.Character.List:GetSelectedLine() ):GetColumnText( 5 ) ) == 1 );
		end,
	},
	{
		text = "Delete Character",
		icon = "icon16/delete.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:DeleteCharacter( pnl.Character.List:GetSelectedLine() );
		end,
	},
};

-- change character name
-- change character head
-- duplicate character
-- change character owner

GM.AdminMenuContexts["Inventories"] = {
	{
		text = "Add Inventory",
		icon = "icon16/package_add.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:AddInventory( pnl.Character.List:GetSelectedLine() );
		end,
	},
	{
		text = "Delete Inventory",
		icon = "icon16/package_delete.png",
		func = function()
			local pnl = GAMEMODE.AdminMenu;
			pnl:DeleteInventory( pnl.Character.List:GetSelectedLine(), pnl.Character.Inventories:GetSelectedLine() );
		end,
	},
};

-- add inventory
-- view inventory
-- assign inventory to item

function PANEL:CreateCharacterMenu()

	self.SelectionSheet:AddSheet( "Characters", self.Character, "icon16/user.png" );
	self.Character:Clear();
	
	self.Character.List = vgui.Create( "DListView", self.Character );
	self.Character.List:SetWide( self:GetWide() / 2.7 );
	self.Character.List:Dock( LEFT );
	self.Character.List:SetMultiSelect( false );
	self.Character.List:AddColumn( "ID" );
	self.Character.List:AddColumn( "SteamID" );
	self.Character.List:AddColumn( "Name" );
	self.Character.List:AddColumn( "CharFlags" );
	self.Character.List:AddColumn( "Disabled" );
	self.Character.List:AddColumn( "Faction" );
	
	self.Character.Inventories = vgui.Create( "DListView", self.Character );
	self.Character.Inventories:SetWide( self:GetWide() / 3.333 );
	self.Character.Inventories:DockMargin( 4, 0, 0, 0 );
	self.Character.Inventories:Dock( LEFT );
	self.Character.Inventories:SetMultiSelect( false );
	self.Character.Inventories:AddColumn( "ID" );
	self.Character.Inventories:AddColumn( "Name" );
	self.Character.Inventories:AddColumn( "Width" );
	self.Character.Inventories:AddColumn( "Height" );
	self.Character.Inventories:AddColumn( "ItemID" );
	
	self.Character.ActiveContainer = vgui.Create( "DPanel", self.Character );
	self.Character.ActiveContainer.Paint = PanelPaint;
	self.Character.ActiveContainer:DockMargin( 4, 0, 0, 0 );
	self.Character.ActiveContainer:SetWide( self:GetWide() / 3.333 );
	self.Character.ActiveContainer:Dock( LEFT );
	
	netstream.Start( "RequestCharacterInfo", 1 );

end

netstream.Hook( "ReceiveCharacterInfo", function( data )

	local panel = GAMEMODE.AdminMenu;
	
	if( !panel ) then return end
	if( !panel.Character.List ) then return end

	panel:ReceivedCharacterInfo( data );

end );

netstream.Hook( "ReceiveInventoryInfo", function( data )

	local panel = GAMEMODE.AdminMenu;
	
	if( !panel ) then return end
	if( !panel.Character.Inventories ) then return end

	panel:ReceivedInventoryInfo( data );


end );

function PANEL:ReceivedCharacterInfo( data )

	self.Character.List:Clear();
	for k,v in next, data do
	
		local line = self.Character.List:AddLine( 
			v.id,
			v.SteamID,
			v.RPName,
			v.CharFlags,
			v.Disabled,
			v.Faction
		);
		line.CharID = v.id;
	
	end

	self.Character.List.OnRowSelected = function( panel, nLineID, LinePanel )

		netstream.Start( "RequestInventoryInfo", tonumber( LinePanel.CharID ) );
	
	end
	self.Character.List.OnRowRightClick = function( panel, nLineID, LinePanel )
	
		self:CreateCharacterContextMenu();
	
	end

end

function PANEL:ReceivedInventoryInfo( data )

	self.Character.Inventories:Clear();
	for k,v in next, data do
	
		local line = self.Character.Inventories:AddLine( 
			v.id,
			v.Name,
			v.w,
			v.h,
			v.ItemID
		);
		line.InvID = v.id;
	
	end
	
	self.Character.Inventories.OnRowRightClick = function( panel, nLineID, LinePanel )
	
		self:CreateInventoryContextMenu();
	
	end

end

function PANEL:AddInventory( nLineID )

	local panel = self.Character.ActiveContainer;
	local line = self.Character.List:GetLine( nLineID );
	local id = line.CharID;
	
	panel:Clear();
	
	panel.InventoryName = vgui.Create( "DTextEntry", panel );
	panel.InventoryName:SetText( "Inventory Name" );
	panel.InventoryName:Dock( TOP );
	
	panel.WidthLabel = vgui.Create( "DLabel", panel );
	panel.WidthLabel:SetTextColor( Color( 0, 0, 0, 255 ) );
	panel.WidthLabel:SetText( "Inventory Width (DO NOT GO ABOVE 7 OR BELOW 1)" );
	panel.WidthLabel:DockMargin( 0, 4, 0, 0 );
	panel.WidthLabel:Dock( TOP );
	panel.WidthLabel:SizeToContents();

	panel.InventoryWidth = vgui.Create( "DNumberWang", panel );
	panel.InventoryWidth:SetDecimals( 0 );
	panel.InventoryWidth:SetValue( 7 );
	panel.InventoryWidth:SetMinMax( 1, 7 );
	panel.InventoryWidth:DockMargin( 0, 4, 0, 0 );
	panel.InventoryWidth:Dock( TOP );
	panel.InventoryWidth:SetWide( panel:GetWide() / 2 );
	
	panel.HeightLabel = vgui.Create( "DLabel", panel );
	panel.HeightLabel:SetTextColor( Color( 0, 0, 0, 255 ) );
	panel.HeightLabel:SetText( "Inventory Height (DO NOT GO BELOW 1)" );
	panel.HeightLabel:DockMargin( 0, 4, 0, 0 );
	panel.HeightLabel:Dock( TOP );
	panel.HeightLabel:SizeToContents();
	
	panel.InventoryHeight = vgui.Create( "DNumberWang", panel );
	panel.InventoryHeight:SetDecimals( 0 );
	panel.InventoryHeight:SetValue( 2 );
	panel.InventoryHeight:SetMin( 1 );
	panel.InventoryHeight:DockMargin( 0, 4, 0, 0 );
	panel.InventoryHeight:Dock( TOP );
	panel.InventoryHeight:SetWide( panel:GetWide() / 2 );
	
	panel.Done = vgui.Create( "DButton", panel );
	panel.Done:SetText( "Add Inventory" );
	panel.Done:DockMargin( 0, 4, 0, 0 );
	panel.Done:Dock( TOP );
	function panel.Done:DoClick()
		
		local szName = panel.InventoryName:GetText();
		local nWidth = panel.InventoryWidth:GetValue();
		local nHeight = panel.InventoryHeight:GetValue();
		
		RunConsoleCommand( "rpa_addinventory", id, szName, nWidth, nHeight );
		panel:Clear();
	
	end
	
end

function PANEL:DeleteInventory( nCharLineID, nInvLineID )

	local character = self.Character.List:GetLine( nCharLineID );
	local line = self.Character.Inventories:GetLine( nInvLineID );
	local id = line.InvID;
	
	RunConsoleCommand( "rpa_deleteinventory", character.CharID, line.InvID );
	self.Character.Inventories:RemoveLine( nInvLineID );

end

function PANEL:SetCharacterDisable( nLineID, nState )

	local line = self.Character.List:GetLine( nLineID );
	local character = line.CharID;
	
	RunConsoleCommand( "rpa_setofflinechardisabled", character, nState );
	line:SetColumnText( 5, nState );

end

function PANEL:DeleteCharacter( nLineID )

	local line = self.Character.List:GetLine( nLineID );
	local character = line.CharID;
	
	RunConsoleCommand( "rpa_deletecharacter", character );
	self.Character.List:RemoveLine( nLineID );

end

function PANEL:CreateCharacterContextMenu()

	local ContextMenu = DermaMenu();
	for k,v in next, GAMEMODE.AdminMenuContexts["Characters"] do
	
		if( v.submenu ) then
		
			local submenu,parent = ContextMenu:AddSubMenu( v.text );
			parent:SetIcon( v.icon );
			for m,n in next, v.options do
			
				local option = submenu:AddOption( n.text, n.func );
				option:SetIcon( n.icon );
				
			end
			
			continue;
			
		end
		
		if( v.display and !v.display() ) then continue end
	
		local option = ContextMenu:AddOption( v.text, v.func );
		option:SetIcon( v.icon );
		
	end
	ContextMenu:Open();

end

function PANEL:CreateInventoryContextMenu()

	local ContextMenu = DermaMenu();
	for k,v in next, GAMEMODE.AdminMenuContexts["Inventories"] do
	
		if( v.submenu ) then
		
			local submenu,parent = ContextMenu:AddSubMenu( v.text );
			parent:SetIcon( v.icon );
			for m,n in next, v.options do
			
				local option = submenu:AddOption( n.text, n.func );
				option:SetIcon( n.icon );
				
			end
			
			continue;
			
		end
		
		if( v.display and !v.display() ) then continue end
	
		local option = ContextMenu:AddOption( v.text, v.func );
		option:SetIcon( v.icon );
		
	end
	ContextMenu:Open();

end

/* 
	Character menu end
*/

/* 
	Permissions Menu start
*/

GM.AdminMenuContexts["Permissions"] = {
	["Ranks"] = {
		{
			text = "Remove usergroup",
			icon = "icon16/vcard_delete.png",
			func = function()
				local pnl = GAMEMODE.AdminMenu;
				pnl:RemoveUsergroup( pnl.Permissions.Ranks:GetSelectedLine() );
			end,
		},
		{
			text = "Make usergroup admin",
			icon = "icon16/lightning_add.png",
			func = function()
				local pnl = GAMEMODE.AdminMenu;
				pnl:MakeUsergroupAdmin( pnl.Permissions.Ranks:GetSelectedLine() );
			end,
			display = function()
				local pnl = GAMEMODE.AdminMenu;
				local line = pnl.Permissions.Ranks:GetLine( pnl.Permissions.Ranks:GetSelectedLine() );
				return ( !GAMEMODE.Ranks[line:GetColumnText(1)].IsSuperAdmin and !GAMEMODE.Ranks[line:GetColumnText(1)].IsAdmin );
			end,
		},
		{
			text = "Take usergroup admin",
			icon = "icon16/lightning_delete.png",
			func = function()
				local pnl = GAMEMODE.AdminMenu;
				pnl:MakeUsergroupAdmin( pnl.Permissions.Ranks:GetSelectedLine() );
			end,
			display = function()
				local pnl = GAMEMODE.AdminMenu;
				local line = pnl.Permissions.Ranks:GetLine( pnl.Permissions.Ranks:GetSelectedLine() );
				return GAMEMODE.Ranks[line:GetColumnText(1)].IsAdmin;
			end,
		},
		{
			text = "Make usergroup superadmin",
			icon = "icon16/shield_add.png",
			func = function()
				local pnl = GAMEMODE.AdminMenu;
				pnl:MakeUsergroupSuperAdmin( pnl.Permissions.Ranks:GetSelectedLine() );
			end,
			display = function()
				local pnl = GAMEMODE.AdminMenu;
				local line = pnl.Permissions.Ranks:GetLine( pnl.Permissions.Ranks:GetSelectedLine() );
				return ( !GAMEMODE.Ranks[line:GetColumnText(1)].IsSuperAdmin and !GAMEMODE.Ranks[line:GetColumnText(1)].IsAdmin );
			end,
		},
		{
			text = "Take usergroup superadmin",
			icon = "icon16/shield_delete.png",
			func = function()
				local pnl = GAMEMODE.AdminMenu;
				pnl:MakeUsergroupSuperAdmin( pnl.Permissions.Ranks:GetSelectedLine() );
			end,
			display = function()
				local pnl = GAMEMODE.AdminMenu;
				local line = pnl.Permissions.Ranks:GetLine( pnl.Permissions.Ranks:GetSelectedLine() );
				return GAMEMODE.Ranks[line:GetColumnText(1)].IsSuperAdmin;
			end,
		},
		{
			submenu = true,
			text = "New",
			icon = "icon16/group_add.png",
			options = {
				{
					text = "Add new usergroup",
					icon = "icon16/vcard_add.png",
					func = function() 
						local pnl = GAMEMODE.AdminMenu;
						pnl:AddNewUsergroup( pnl.Permissions.Ranks:GetSelectedLine() );
					end,
				},
			},
		},
	},
	["Commands"] = {
		{
			text = "Add permission",
			icon =  "icon16/script_add.png",
			func = function()
				local pnl = GAMEMODE.AdminMenu;
				pnl:AddPermission( pnl.Permissions.Ranks:GetSelectedLine() );
			end,
		},
		{
			text = "Remove permission",
			icon = "icon16/script_delete.png",
			func = function()
				local pnl = GAMEMODE.AdminMenu;
				pnl:RemovePermission( pnl.Permissions.Ranks:GetSelectedLine(), pnl.Permissions.Commands:GetSelectedLine() );
			end,
		},
	},
};

function PANEL:CreatePermissionsMenu()

	self.SelectionSheet:AddSheet( "Permissions", self.Permissions, "icon16/script.png" );
	self.Permissions:Clear();
	
	self.Permissions.Ranks = vgui.Create( "DListView", self.Permissions );
	self.Permissions.Ranks:Dock( LEFT );
	self.Permissions.Ranks:SetWide( self:GetWide() / 6 );
	self.Permissions.Ranks:SetMultiSelect( false );
	self.Permissions.Ranks:AddColumn( "Rank" );
	self.Permissions.Ranks.OnRowSelected = function( panel, nRowIndex, RowPanel )
	
		self.Permissions.Commands:Clear();
	
		for k,v in next, GAMEMODE.Ranks[RowPanel:GetValue( 1 )].Permissions do
		
			self.Permissions.Commands:AddLine( k );
		
		end
	
	end
	self.Permissions.Ranks.OnRowRightClick = function( panel, nLineID, LinePanel )
	
		self:CreateRankContextMenu();
	
	end
	
	self.Permissions.Commands = vgui.Create( "DListView", self.Permissions );
	self.Permissions.Commands:Dock( LEFT );
	self.Permissions.Commands:SetWide( self:GetWide() / 6 );
	self.Permissions.Commands:SetMultiSelect( false ); -- might want to have it as true so we can quickly manip. permissions.
	self.Permissions.Commands:AddColumn( "Command" );
	self.Permissions.Commands.OnRowRightClick = function( panel, nLineID, LinePanel )
	
		self:CreateCommandContextMenu();
	
	end
	
	
	for k,v in next, GAMEMODE.Ranks do
	
		self.Permissions.Ranks:AddLine( k );
		
	end
	
	self.Permissions.ActiveContainer = vgui.Create( "DPanel", self.Permissions );
	self.Permissions.ActiveContainer:DockMargin( 4, 0, 0, 0 );
	self.Permissions.ActiveContainer:Dock( FILL );
	self.Permissions.ActiveContainer.Paint = PanelPaint;

end

function PANEL:RemoveUsergroup( nLineID )

	local panel = self.Permissions.Ranks:GetLine( nLineID );
	
	RunConsoleCommand( "rpa_removeusergroup", panel:GetColumnText( 1 ) );
	self.Permissions.Ranks:RemoveLine( nLineID );
	self.Permissions.Commands:Clear();

end

function PANEL:RemovePermission( nRankLineID, nCommandLineID )

	local rank = self.Permissions.Ranks:GetLine( nRankLineID );
	local command = self.Permissions.Commands:GetLine( nCommandLineID );
	
	RunConsoleCommand( "rpa_takepermission", rank:GetColumnText( 1 ), command:GetColumnText( 1 ) );
	self.Permissions.Commands:RemoveLine( nCommandLineID );

end

function PANEL:MakeUsergroupAdmin( nLineID )

	local line = self.Permissions.Ranks:GetLine( nLineID );
	RunConsoleCommand( "rpa_setrankadmin", line:GetColumnText( 1 ) );
	
end

function PANEL:MakeUsergroupSuperAdmin( nLineID )

	local line = self.Permissions.Ranks:GetLine( nLineID );
	RunConsoleCommand( "rpa_setranksuperadmin", line:GetColumnText( 1 ) );

end

function PANEL:AddNewUsergroup( nLineID )

	local panel = self.Permissions.ActiveContainer;
	panel:Clear();
	
	panel.RankName = vgui.Create( "DTextEntry", panel );
	panel.RankName:Dock( TOP );
	panel.RankName:SetTall( 24 );
	panel.RankName:SetText( "Usergroup" );
	panel.RankName:SetToolTip( "Usergroup name, has to be unique, no spaces." );
	
	panel.Admin = vgui.Create( "DCheckBoxLabel", panel );
	panel.Admin:DockMargin( 0, 4, 0, 0 );
	panel.Admin:Dock( TOP );
	panel.Admin:SetText( "Is Admin" );
	panel.Admin:SetValue( 0 );
	panel.Admin:SizeToContents();
	panel.Admin:SetTooltip( "Whether or not this usergroup returns true in Player:IsAdmin. Redundant if superadmin is set to true." );
	
	panel.SuperAdmin = vgui.Create( "DCheckBoxLabel", panel );
	panel.SuperAdmin:DockMargin( 0, 4, 0, 0 );
	panel.SuperAdmin:Dock( TOP );
	panel.SuperAdmin:SetText( "Is Superadmin" );
	panel.SuperAdmin:SetValue( 0 );
	panel.SuperAdmin:SizeToContents();
	panel.SuperAdmin:SetTooltip( "Whether or not this usergroup returns true in Player:IsSuperAdmin." );
	
	panel.Commands = vgui.Create( "DListView", panel );
	panel.Commands:DockMargin( 0, 4, 0, 0 );
	panel.Commands:Dock( FILL );
	panel.Commands:SetMultiSelect( true )
	panel.Commands:AddColumn( "Command" );
	
	panel.Cancel = vgui.Create( "DButton", panel );
	panel.Cancel:DockMargin( 0, 4, 0, 0 );
	panel.Cancel:Dock( BOTTOM );
	panel.Cancel:SetText( "Cancel" );
	panel.Cancel.DoClick = function()

		panel:Clear();
	
	end
	
	panel.Done = vgui.Create( "DButton", panel );
	panel.Done:DockMargin( 0, 4, 0, 0 );
	panel.Done:Dock( BOTTOM );
	panel.Done:SetText( "Create" );
	panel.Done.DoClick = function()
	
		local CommandTbl = {};
		for k,v in next, panel.Commands:GetSelected() do
		
			CommandTbl[#CommandTbl + 1] = v:GetColumnText( 1 );
		
		end
	
		RunConsoleCommand( "rpa_addusergroup", panel.RankName:GetText(), unpack( CommandTbl ) );
		self.Permissions.Ranks:AddLine( panel.RankName:GetText() );
		panel:Clear();
	
	end
	
	for k,v in next, GAMEMODE.Commands do
	
		panel.Commands:AddLine( k );
	
	end
	
end

function PANEL:AddPermission( nLineID )

	local panel = self.Permissions.ActiveContainer;
	panel:Clear();
	
	local line = self.Permissions.Ranks:GetLine( nLineID );
	local rank = line:GetColumnText( 1 );
	
	panel.Commands = vgui.Create( "DListView", panel );
	panel.Commands:DockMargin( 0, 0, 0, 0 );
	panel.Commands:Dock( FILL );
	panel.Commands:SetMultiSelect( true )
	panel.Commands:AddColumn( "Command" );
	
	for k,v in next, GAMEMODE.Commands do
	
		if( GAMEMODE.Ranks[rank]["Permissions"][k] ) then continue end
	
		panel.Commands:AddLine( k );
	
	end
	
	panel.Cancel = vgui.Create( "DButton", panel );
	panel.Cancel:DockMargin( 0, 4, 0, 0 );
	panel.Cancel:Dock( BOTTOM );
	panel.Cancel:SetText( "Cancel" );
	panel.Cancel.DoClick = function()

		panel:Clear();
	
	end
	
	panel.Done = vgui.Create( "DButton", panel );
	panel.Done:DockMargin( 0, 4, 0, 0 );
	panel.Done:Dock( BOTTOM );
	panel.Done:SetText( "Add" );
	panel.Done.DoClick = function()
	
		local CommandTbl = {};
		for k,v in next, panel.Commands:GetSelected() do
		
			CommandTbl[#CommandTbl + 1] = v:GetColumnText( 1 );
		
		end
	
		RunConsoleCommand( "rpa_addpermission", rank, unpack( CommandTbl ) );
		for k,v in next, CommandTbl do
		
			self.Permissions.Commands:AddLine( v );
			
		end
		panel:Clear();
	
	end
	
end

function PANEL:CreateRankContextMenu()

	local ContextMenu = DermaMenu();
	for k,v in next, GAMEMODE.AdminMenuContexts["Permissions"]["Ranks"] do
	
		if( v.submenu ) then
		
			local submenu,parent = ContextMenu:AddSubMenu( v.text );
			parent:SetIcon( v.icon );
			for m,n in next, v.options do
			
				local option = submenu:AddOption( n.text, n.func );
				option:SetIcon( n.icon );
				
			end
			
			continue;
			
		end
		
		if( v.display and !v.display() ) then continue end
	
		local option = ContextMenu:AddOption( v.text, v.func );
		option:SetIcon( v.icon );
		
	end
	ContextMenu:Open();

end

function PANEL:CreateCommandContextMenu()

	local ContextMenu = DermaMenu();
	for k,v in next, GAMEMODE.AdminMenuContexts["Permissions"]["Commands"] do
	
		if( v.submenu ) then
		
			local submenu,parent = ContextMenu:AddSubMenu( v.text );
			parent:SetIcon( v.icon );
			for m,n in next, v.options do
			
				local option = submenu:AddOption( n.text, n.func );
				option:SetIcon( n.icon );
				
			end
			
			continue;
			
		end
	
		local option = ContextMenu:AddOption( v.text, v.func );
		option:SetIcon( v.icon );
		
	end
	ContextMenu:Open();
	
end

/* 
	Permissions Menu end
*/

function PANEL:Update()

	if( self.Permissions.Ranks ) then
	
		self.Permissions.Ranks:Clear();
		self.Permissions.Commands:Clear();
		for k,v in next, GAMEMODE.Ranks do
		
			self.Permissions.Ranks:AddLine( k );
			
		end
		
	end

end

vgui.Register( "CAdminMenu", PANEL, "DFrame" );