local SKIN = {};

SKIN.PrintName 		= "Admin UI";
SKIN.Author			= "rusty";
SKIN.DermaVersion	= 1;

SKIN.Colours = {}

SKIN.Colours.Window = {}
SKIN.Colours.Window.TitleActive			= GWEN.TextureColor( 4 + 8 * 0, 508 )
SKIN.Colours.Window.TitleInactive		= GWEN.TextureColor( 4 + 8 * 1, 508 )

SKIN.Colours.Button = { }
SKIN.Colours.Button.Normal				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Hover				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Down				= Color( 255, 255, 255, 255 );
SKIN.Colours.Button.Disabled			= Color( 128, 128, 128, 255 );

SKIN.Colours.Tab = {}
SKIN.Colours.Tab.Active = {}
SKIN.Colours.Tab.Active.Normal			= GWEN.TextureColor( 4 + 8 * 4, 508 )
SKIN.Colours.Tab.Active.Hover			= GWEN.TextureColor( 4 + 8 * 5, 508 )
SKIN.Colours.Tab.Active.Down			= GWEN.TextureColor( 4 + 8 * 4, 500 )
SKIN.Colours.Tab.Active.Disabled		= GWEN.TextureColor( 4 + 8 * 5, 500 )

SKIN.Colours.Tab.Inactive = {}
SKIN.Colours.Tab.Inactive.Normal		= GWEN.TextureColor( 4 + 8 * 6, 508 )
SKIN.Colours.Tab.Inactive.Hover			= GWEN.TextureColor( 4 + 8 * 7, 508 )
SKIN.Colours.Tab.Inactive.Down			= GWEN.TextureColor( 4 + 8 * 6, 500 )
SKIN.Colours.Tab.Inactive.Disabled		= GWEN.TextureColor( 4 + 8 * 7, 500 )

SKIN.Colours.Label = { }
SKIN.Colours.Label.Default				= Color( 255, 255, 255, 255 );
SKIN.Colours.Label.Bright				= Color( 255, 255, 255, 255 );
SKIN.Colours.Label.Dark					= Color( 200, 200, 200, 255 );
SKIN.Colours.Label.Highlight			= Color( 255, 255, 255, 255 );

SKIN.Colours.Tree = {}
SKIN.Colours.Tree.Lines					= GWEN.TextureColor( 4 + 8 * 10, 508 ) ---- !!!
SKIN.Colours.Tree.Normal				= GWEN.TextureColor( 4 + 8 * 11, 508 )
SKIN.Colours.Tree.Hover					= GWEN.TextureColor( 4 + 8 * 10, 500 )
SKIN.Colours.Tree.Selected				= GWEN.TextureColor( 4 + 8 * 11, 500 )

SKIN.Colours.Properties = {}
SKIN.Colours.Properties.Line_Normal			= GWEN.TextureColor( 4 + 8 * 12, 508 )
SKIN.Colours.Properties.Line_Selected		= GWEN.TextureColor( 4 + 8 * 13, 508 )
SKIN.Colours.Properties.Line_Hover			= GWEN.TextureColor( 4 + 8 * 12, 500 )
SKIN.Colours.Properties.Title				= GWEN.TextureColor( 4 + 8 * 13, 500 )
SKIN.Colours.Properties.Column_Normal		= GWEN.TextureColor( 4 + 8 * 14, 508 )
SKIN.Colours.Properties.Column_Selected		= GWEN.TextureColor( 4 + 8 * 15, 508 )
SKIN.Colours.Properties.Column_Hover		= GWEN.TextureColor( 4 + 8 * 14, 500 )
SKIN.Colours.Properties.Border				= GWEN.TextureColor( 4 + 8 * 15, 500 )
SKIN.Colours.Properties.Label_Normal		= GWEN.TextureColor( 4 + 8 * 16, 508 )
SKIN.Colours.Properties.Label_Selected		= GWEN.TextureColor( 4 + 8 * 17, 508 )
SKIN.Colours.Properties.Label_Hover			= GWEN.TextureColor( 4 + 8 * 16, 500 )

SKIN.Colours.Category = {}
SKIN.Colours.Category.Header				= GWEN.TextureColor( 4 + 8 * 18, 500 )
SKIN.Colours.Category.Header_Closed			= GWEN.TextureColor( 4 + 8 * 19, 500 )
SKIN.Colours.Category.Line = {}
SKIN.Colours.Category.Line.Text				= GWEN.TextureColor( 4 + 8 * 20, 508 )
SKIN.Colours.Category.Line.Text_Hover		= GWEN.TextureColor( 4 + 8 * 21, 508 )
SKIN.Colours.Category.Line.Text_Selected	= GWEN.TextureColor( 4 + 8 * 20, 500 )
SKIN.Colours.Category.Line.Button			= GWEN.TextureColor( 4 + 8 * 21, 500 )
SKIN.Colours.Category.Line.Button_Hover		= GWEN.TextureColor( 4 + 8 * 22, 508 )
SKIN.Colours.Category.Line.Button_Selected	= GWEN.TextureColor( 4 + 8 * 23, 508 )
SKIN.Colours.Category.LineAlt = {}
SKIN.Colours.Category.LineAlt.Text				= GWEN.TextureColor( 4 + 8 * 22, 500 )
SKIN.Colours.Category.LineAlt.Text_Hover		= GWEN.TextureColor( 4 + 8 * 23, 500 )
SKIN.Colours.Category.LineAlt.Text_Selected		= GWEN.TextureColor( 4 + 8 * 24, 508 )
SKIN.Colours.Category.LineAlt.Button			= GWEN.TextureColor( 4 + 8 * 25, 508 )
SKIN.Colours.Category.LineAlt.Button_Hover		= GWEN.TextureColor( 4 + 8 * 24, 500 )
SKIN.Colours.Category.LineAlt.Button_Selected	= GWEN.TextureColor( 4 + 8 * 25, 500 )

SKIN.Colours.TooltipText = GWEN.TextureColor( 4 + 8 * 26, 500 )

SKIN.colTextEntryText			= Color( 0, 0, 0, 255 );
SKIN.colTextEntryTextHighlight	= Color( 255, 255, 255, 10 );
SKIN.colTextEntryTextCursor		= Color( 255, 255, 255, 255 );

function SKIN:PaintPanel( panel, w, h )

end

/*function SKIN:PaintFrame( panel, w, h )

 	surface.SetDrawColor( 15, 15, 15 );
	surface.DrawRect( 0, 0, w, h );

	surface.SetDrawColor( 100, 100, 100, 40 );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( 128, 128, 128, 128 );
	surface.DrawOutlinedRect( 0, 24, w, h - 24 );
	
end*/

local function PaintNotches( x, y, w, h, num )

	if ( !num ) then return end

	local space = w / num

	for i=0, num do

		surface.DrawRect( x + i * space, y + 4, 1, 5 )

	end

end

function SKIN:PaintNumSlider( panel, w, h )

	if( !panel.bSetup ) then
	
		-- fuck you garry
		panel.bSetup = true;
		panel:SetEnabled( false );
		panel.OnMousePressed = function() end;
		panel.OnMouseReleased = function() end;
		panel:GetParent().Scratch.OnMousePressed = function() end;
		panel:GetParent().Scratch.OnMouseReleased = function() end;
		
	end
		

	surface.SetDrawColor( Color( 255, 255, 255, 100 ) )
	surface.DrawRect( 8, h / 2 - 1, w - 15, 1 )

	PaintNotches( 8, h / 2 - 1, w - 16, 1, panel.m_iNotches )

end

--[[ function SKIN:PaintSlider( panel, w, h )

end ]]

-- DMenuOption does not have derma.SkinHook for PerformLayout. Neither does DButton or DLabel.
-- https://github.com/Facepunch/garrysmod/blob/master/garrysmod/lua/vgui/dmenuoption.lua#L120

function SKIN:PaintMenuOption( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetTextColor( Color( 172, 172, 172 ) );
		panel:SetFont( "Trebuchet18" );
		panel:SizeToContents();
		panel:Center();
	
	end
	
	if( panel:IsHovered() ) then
	
		surface.SetDrawColor( Color( 152, 152, 152, 32 ) );
		surface.SetFont( panel:GetFont() );
		local tW,tH = surface.GetTextSize( panel:GetText() );
		surface.DrawRect( 30, 2, tW, tH );
		
	end

end

local matBlurScreen = Material( "pp/blurscreen" );

function draw.DrawBlur( x, y, w, h, us, vs, ue, ve, frac )
	
	if( frac == 0 ) then return end
	
	surface.SetMaterial( matBlurScreen );
	surface.SetDrawColor( 255, 255, 255, 255 );
	
	for i = 1, 3 do
		
		matBlurScreen:SetFloat( "$blur", frac * 5 * ( i / 3 ) )
		matBlurScreen:Recompute();
		render.UpdateScreenEffectTexture();
		surface.DrawTexturedRectUV( x, y, w, h, us, vs, ue, ve );
		
	end

end

function draw.DrawDiagonals( col, x, y, w, h, density )
	
	surface.SetDrawColor( col );
	
	for i = 0, w + h, density do
		
		if( i < h ) then
			
			surface.DrawLine( x + i, y, x, y + i );
			
		elseif( i > w ) then
			
			surface.DrawLine( x + w, y + i - w, x - h + i, y + h );
			
		else
			
			surface.DrawLine( x + i, y, x + i - h, y + h );
			
		end
		
	end
	
end

function SKIN:PaintButtonDown( panel, w, h )
	
	if( !panel.LaidOut ) then
		
		panel.LaidOut = true;
		panel:SetText( "" );
		panel:SetFont( "marlett" );
		panel:SetSize( 6, 6 );
		panel:SetPos( 0, panel:GetParent():GetTall() - 6 );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintButtonUp( panel, w, h )
	
	if( !panel.LaidOut ) then
		
		panel.LaidOut = true;
		panel:SetText( "" );
		panel:SetFont( "marlett" );
		panel:SetSize( 6, 6 );
		panel:SetPos( 0, 0 );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintVScrollBar( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetSize( 10, panel:GetParent():GetTall() );
		
	end

end

function SKIN:PaintScrollBarGrip( panel, w, h )

	if( !panel.LaidOut ) then
	
		panel.LaidOut = true;
		panel:SetSize( 10, panel:GetParent():GetTall() );
		
	end
	
	surface.SetDrawColor( Color( 126, 126, 126 ) );
	surface.DrawOutlinedRect( 0, 0, w, h );
	surface.SetDrawColor( Color( 96, 96, 96 ) );
	surface.DrawOutlinedRect( 1, 1, w - 2, h - 2 );
	surface.SetDrawColor( Color( 102, 102, 102 ) );
	surface.DrawRect( 2, 2, w - 3, h - 3 );

end

function SKIN:PaintTextEntry( panel, w, h )
	
	local w, h = panel:GetSize();
	
	if( panel:GetDrawBackground() ) then
		
		surface.SetDrawColor( 30, 30, 30, 255 );
		surface.DrawRect( 0, 0, w, h );
		
		surface.SetDrawColor( 40, 40, 40, 255 );
		surface.DrawRect( 1, 1, w - 2, h - 2 );
		
	end
	
	panel:DrawTextEntryText( panel:GetTextColor(), panel:GetHighlightColor(), panel:GetCursorColor() );
	
end

/*function SKIN:PaintTab( panel, w, h )

	surface.SetDrawColor( 255, 255, 255 );
	surface.DrawOutlinedRect( 0, 0, w, h );

end*/

derma.DefineSkin( "AdminUI", "Admin UI Skin", SKIN );