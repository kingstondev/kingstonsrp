--- CharacterCreateVGUI ---

local PANEL = {}

function PANEL:Page1()

	self.LastPage = 1;
	self.LastPageCallback = function() 
	
		self.szModel = self.ContentPane.CP.Entity:GetModel();
		self.szHead = self.Head:GetModel();
		
	end

	function self.ContentPane:Paint( w, h )
	end
	
	local faction = GAMEMODE.Factions["loner"];
	self.ContentPane.CP = vgui.Create( "CCharPanel", self.ContentPane );
	self.ContentPane.CP:SetPos( 0, 0 );
	self.ContentPane.CP:SetSize( 324, self.ContentPane:GetTall() );
	self.ContentPane.CP:SetModel( "models/error.mdl" );
	self.ContentPane.CP:SetModel( self.szModel or faction.Models[1] );
	self.ContentPane.CP:SetFOV( 30 );
	self.ContentPane.CP.HeadFollowMouse = true;
	self.ContentPane.CP:LayoutEntity( self.ContentPane.CP.Entity );
	self.Head = self.ContentPane.CP:InitializeModel( self.szHead or GAMEMODE.LonerHeads[1], self.ContentPane.CP.Entity );
	self.CurrentIndex = 1;
	self.ContentPane.CP:StartAnimation( "idle" );
	
	local y = 2;
	
	self.ContentPane.Model = vgui.Create( "DLabel", self.ContentPane );
	self.ContentPane.Model:SetSize( 256, 32 );
	self.ContentPane.Model:SetPos( 340, y );
	self.ContentPane.Model:SetFont( "Stalker2-24" );
	self.ContentPane.Model:SetTextColor( Color( 255, 255, 255, 255 ) );
	self.ContentPane.Model:SetText( "Model" );
	
	y = y + 34;
	
	self.ContentPane.E1 = vgui.Create( "DButton", self.ContentPane );
	self.ContentPane.E1:SetSize( 32, 32 );
	self.ContentPane.E1:SetPos( 340, y );
	self.ContentPane.E1:SetFont( "marlett" );
	self.ContentPane.E1:SetText( "3" );
	function self.ContentPane.E1:DoClick()
	
	end
	
	self.ContentPane.E2 = vgui.Create( "DButton", self.ContentPane );
	self.ContentPane.E2:SetSize( 32, 32 );
	self.ContentPane.E2:SetPos( 376, y );
	self.ContentPane.E2:SetFont( "marlett" );
	self.ContentPane.E2:SetText( "4" );
	function self.ContentPane.E2:DoClick()
	
	end
	
	y = y + 40;
	
	self.ContentPane.Head = vgui.Create( "DLabel", self.ContentPane );
	self.ContentPane.Head:SetSize( 256, 32 );
	self.ContentPane.Head:SetPos( 340, y );
	self.ContentPane.Head:SetFont( "Stalker2-24" );
	self.ContentPane.Head:SetTextColor( Color( 255, 255, 255, 255 ) );
	self.ContentPane.Head:SetText( "Head" );
	
	y = y + 34;
	
	self.ContentPane.E3 = vgui.Create( "DButton", self.ContentPane );
	self.ContentPane.E3:SetSize( 32, 32 );
	self.ContentPane.E3:SetPos( 340, y );
	self.ContentPane.E3:SetFont( "marlett" );
	self.ContentPane.E3:SetText( "3" );
	self.ContentPane.E3.DoClick = function( pnl )
	
		self.CurrentIndex = self.CurrentIndex + 1;
		if( self.CurrentIndex > #GAMEMODE.LonerHeads ) then
		
			self.CurrentIndex = 1;
			
		end
		
		self.Head:SetModel( GAMEMODE.LonerHeads[self.CurrentIndex] );
	
	end
	
	self.ContentPane.E4 = vgui.Create( "DButton", self.ContentPane );
	self.ContentPane.E4:SetSize( 32, 32 );
	self.ContentPane.E4:SetPos( 376, y );
	self.ContentPane.E4:SetFont( "marlett" );
	self.ContentPane.E4:SetText( "4" );
	self.ContentPane.E4.DoClick = function( pnl )
	
		self.CurrentIndex = self.CurrentIndex - 1;
		if( self.CurrentIndex <= 0 ) then
		
			self.CurrentIndex = #GAMEMODE.LonerHeads;
			
		end
		
		self.Head:SetModel( GAMEMODE.LonerHeads[self.CurrentIndex] );
	
	end
	
end

function PANEL:Page2()

	self.LastPage = 2;
	self.LastPageCallback = function()
		
		self.szName = self.ContentPane.E1:GetValue();
		self.szTitleOne = self.ContentPane.E2:GetValue();
		self.szTitleTwo = self.ContentPane.E3:GetValue();
		self.szDescription = self.ContentPane.E4:GetValue();
		
	end

	function self.ContentPane:Paint( w, h )
	
		surface.SetFont( "Stalker2-16" );
		if( #self:GetParent().ContentPane.E2:GetValue() <= 64 ) then
		
			surface.SetTextColor( Color( 255, 255, 255, 255 ) );
			
		else
		
			surface.SetTextColor( Color( 255, 0, 0, 255 ) );
			
		end
		local x,y = self.E2:GetPos();
		local nW,nH = surface.GetTextSize( #self.E2:GetValue().."/64" );
		surface.SetTextPos( x + self.E2:GetWide(), y + self.E2:GetTall() / 2 - nH / 2 );
		surface.DrawText( #self.E2:GetValue().."/64" );
		if( #self.E3:GetValue() <= 64 ) then
		
			surface.SetTextColor( Color( 255, 255, 255, 255 ) );
		
		else
		
			surface.SetTextColor( Color( 255, 0, 0, 255 ) );
			
		end
		x,y = self.E3:GetPos();
		nW,nH = surface.GetTextSize( #self.E3:GetValue().."/64" );
		surface.SetTextPos( x + self.E2:GetWide(), y + self.E3:GetTall() / 2 - nH / 2 );
		surface.DrawText( #self.E3:GetValue().."/64" );

	end
	
	local y = 2;
	
	self.ContentPane.E1 = vgui.Create( "DTextEntry", self.ContentPane );
	self.ContentPane.E1:SetSize( 290, 20 );
	self.ContentPane.E1:SetPos( 340, y );
	self.ContentPane.E1:SetFont( "Stalker2-16" );
	self.ContentPane.E1:SetSkin( "StalkerUI" );
	self.ContentPane.E1:SetValue( self.szName or "Name" );
	self.ContentPane.E1:SetTextColor( Color( 255, 255, 255, 255 ) );

	y = y + 20 + 4; -- why add the two figures together when now you can easily edit the distance?
	
	self.ContentPane.E2 = vgui.Create( "DTextEntry", self.ContentPane );
	self.ContentPane.E2:SetSize( 290, 20 );
	self.ContentPane.E2:SetPos( 340, y );
	self.ContentPane.E2:SetFont( "Stalker2-16" );
	self.ContentPane.E2:SetSkin( "StalkerUI" );
	self.ContentPane.E2:SetValue( self.szTitleOne or "Overhead Line 1" );
	self.ContentPane.E2:SetTextColor( Color( 255, 255, 255, 255 ) );
	function self.ContentPane.E2:OnTextChanged()
	
		local txt = self:GetValue()
		if( #txt > 64 ) then

			self:SetValue( self.OldText );
			self:SetText( self.OldText );
			
		else
		
			self.OldText = txt;
			
		end
		
	end
	
	y = y + 20 + 4;
	
	self.ContentPane.E3 = vgui.Create( "DTextEntry", self.ContentPane );
	self.ContentPane.E3:SetSize( 290, 20 );
	self.ContentPane.E3:SetPos( 340, y );
	self.ContentPane.E3:SetFont( "Stalker2-16" );
	self.ContentPane.E3:SetValue( self.szTitleTwo or "Overhead Line 2" );
	self.ContentPane.E3:SetSkin( "StalkerUI" );
	self.ContentPane.E3:SetTextColor( Color( 255, 255, 255, 255 ) );
	function self.ContentPane.E3:OnTextChanged()
	
		local txt = self:GetValue()
		if( #txt > 64 ) then

			self:SetValue( self.OldText );
			self:SetText( self.OldText );
			
		else
		
			self.OldText = txt;
			
		end
		
	end
	
	y = y + 20 + 4;
	
	self.ContentPane.E4 = vgui.Create( "DTextEntry", self.ContentPane );
	self.ContentPane.E4:SetSize( 290, 290 );
	self.ContentPane.E4:SetPos( 340, y );
	self.ContentPane.E4:SetMultiline( true );
	self.ContentPane.E4:SetFont( "Stalker2-16" );
	self.ContentPane.E4:SetSkin( "StalkerUI" );
	self.ContentPane.E4:SetTextColor( Color( 255, 255, 255, 255 ) );
	self.ContentPane.E4:SetValue( self.szDescription or "Description" );
	
	local faction = GAMEMODE.Factions["loner"];
	self.ContentPane.CP = vgui.Create( "CCharPanel", self.ContentPane );
	self.ContentPane.CP:SetPos( 0, 0 );
	self.ContentPane.CP:SetSize( 324, self.ContentPane:GetTall() );
	self.ContentPane.CP:SetModel( "models/error.mdl" );
	self.ContentPane.CP:SetModel( self.szModel or faction.Models[1] );
	self.ContentPane.CP:SetFOV( 30 );
	self.ContentPane.CP.HeadFollowMouse = true;
	self.ContentPane.CP:LayoutEntity( self.ContentPane.CP.Entity );
	self.Head = self.ContentPane.CP:InitializeModel( self.szHead or GAMEMODE.LonerHeads[1], self.ContentPane.CP.Entity );
	self.ContentPane.CP:StartAnimation( "idle" );
	
end


function PANEL:Page3()

	self.LastPage = 3;
	self.LastPageCallback = function()
	end
	
	function self.ContentPane:Paint( w, h )
	end
	
	local faction = GAMEMODE.Factions["loner"];
	self.ContentPane.CP = vgui.Create( "CCharPanel", self.ContentPane );
	self.ContentPane.CP:SetPos( 0, 0 );
	self.ContentPane.CP:SetSize( 324, self.ContentPane:GetTall() );
	self.ContentPane.CP:SetModel( "models/error.mdl" );
	self.ContentPane.CP:SetModel( self.szModel or faction.Models[1] );
	self.ContentPane.CP:SetFOV( 30 );
	self.ContentPane.CP.HeadFollowMouse = true;
	self.ContentPane.CP:LayoutEntity( self.ContentPane.CP.Entity );
	self.Head = self.ContentPane.CP:InitializeModel( self.szHead or GAMEMODE.LonerHeads[1], self.ContentPane.CP.Entity );
	self.ContentPane.CP:StartAnimation( "idle" );
	
	self.ContentPane.Scroll = vgui.Create( "DScrollPanel", self.ContentPane );
	self.ContentPane.Scroll:SetPos( 338, 0 );
	self.ContentPane.Scroll:SetSize( 418, 365 );
	function self.ContentPane.Scroll:Paint( w, h )
	end
	
	local tblGearSelection = {};
	if( faction.GearSelection ) then
	
		for k,v in next, faction.GearSelection do
		
			tblGearSelection[#tblGearSelection + 1] = table.Copy( v );
		
		end
		
	end
	
	for k,v in next, GAMEMODE.GlobalGearSelection do
	
		tblGearSelection[#tblGearSelection + 1] = table.Copy( v );
	
	end
	
	for k,v in next, tblGearSelection do
	
		-- create
	
	end

end

function PANEL:Page4()

	function self.ContentPane:Paint( w, h )
	end
	
	self.LastPage = 4;
	self.LastPageCallback = function() end
	
	local faction = GAMEMODE.Factions["loner"];
	self.ContentPane.CP = vgui.Create( "CCharPanel", self.ContentPane );
	self.ContentPane.CP:SetPos( 0, 0 );
	self.ContentPane.CP:SetSize( 324, self.ContentPane:GetTall() );
	self.ContentPane.CP:SetModel( "models/error.mdl" );
	self.ContentPane.CP:SetModel( self.szModel or faction.Models[1] );
	self.ContentPane.CP:SetFOV( 30 );
	self.ContentPane.CP.HeadFollowMouse = true;
	self.ContentPane.CP:LayoutEntity( self.ContentPane.CP.Entity );
	self.Head = self.ContentPane.CP:InitializeModel( self.szHead or GAMEMODE.LonerHeads[1], self.ContentPane.CP.Entity );
	self.ContentPane.CP:StartAnimation( "idle" );
	
	self.ContentPane.Done = vgui.Create( "DButton", self.ContentPane );
	self.ContentPane.Done:SetSize( 256, 24 );
	self.ContentPane.Done:SetPos( self.ContentPane:GetWide() - 260, 20 );
	self.ContentPane.Done:SetFont( "Stalker2-24" );
	self.ContentPane.Done:SetText( "Done" );
	self.ContentPane.Done.DoClick = function( pnl )
	
		local s_szName = GAMEMODE.CharacterCreate.szName;
		local s_szOverhead1 = GAMEMODE.CharacterCreate.szTitleOne;
		local s_szOverhead2 = GAMEMODE.CharacterCreate.szTitleTwo;
		local s_szDesc = GAMEMODE.CharacterCreate.szDescription;
		local s_szPlyModel = GAMEMODE.CharacterCreate.szModel;
		local s_szPlyHead = GAMEMODE.CharacterCreate.szHead;
		
		-- implement field checking so we cant continue until player has desc and name
		netstream.Start( "CreateCharacter", s_szName, s_szOverhead1, s_szOverhead2, s_szDesc, s_szPlyModel, s_szPlyHead );
		
		GAMEMODE.CharacterCreate:Close();
	
	end

end

function PANEL:Init()

	GAMEMODE.CharacterCreate = self;
	self.LastPageCallback = function() end
	
	self:SetSize( 800, 500 );
	self:Center();
	self:SetTitle( "" );
	self:MakePopup( );
	self:SetSkin( "StalkerSkin" );
	self:ShowCloseButton( true );
	self:SetAlpha( 0 );
	self:AlphaTo( 255, 0.2, 0 );
	self:SetDraggable( false );
	
	self.TopBar = vgui.Create( "DPanel", self );
	self.TopBar:SetPos( 12, 28 );
	self.TopBar:SetSize( 754, 40 );
	function self.TopBar:Paint( w, h )
	end
	self.TopBar.Buttons = {};
	
	self.TopBar.Buttons[1] = vgui.Create( "DButton", self.TopBar );
	self.TopBar.Buttons[1]:SetFont( "STALKER.LabelMenu" );
	self.TopBar.Buttons[1]:SetText( "Looks" );
	self.TopBar.Buttons[1]:SetSkin( "StalkerSkin" );
	self.TopBar.Buttons[1]:SetPos( 0, 0 );
	self.TopBar.Buttons[1]:SetSize( 760 / 4, 40 );
	self.TopBar.Buttons[1].m_bTitleButton = true;
	self.TopBar.Buttons[1].m_bIsDown = true;
	self.TopBar.Buttons[1].DoClick = function( pnl )
	
		if( pnl.m_bIsDown ) then return end
	
		for k,v in next, self.TopBar.Buttons do
		
			v.m_bIsDown = false;
			
		end
		pnl.m_bIsDown = true;
		self:LastPageCallback();
		self.ContentPane:Clear();
		self:Page1();
		
	end
	self.TopBar.Buttons[1]:PerformLayout();
	
	self.TopBar.Buttons[2] = vgui.Create( "DButton", self.TopBar );
	self.TopBar.Buttons[2]:SetFont( "STALKER.LabelMenu" );
	self.TopBar.Buttons[2]:SetText( "Details" );
	self.TopBar.Buttons[2]:SetSkin( "StalkerSkin" );
	self.TopBar.Buttons[2]:SetPos( 188, 0 );
	self.TopBar.Buttons[2]:SetSize( 760 / 4, 40 );
	self.TopBar.Buttons[2].m_bTitleButton = true;
	self.TopBar.Buttons[2].DoClick = function( pnl )
	
		if( pnl.m_bIsDown ) then return end
	
		for k,v in next, self.TopBar.Buttons do
		
			v.m_bIsDown = false;
			
		end
		pnl.m_bIsDown = true;
		self:LastPageCallback();
		self.ContentPane:Clear();
		self:Page2();
		
	end
	self.TopBar.Buttons[2]:PerformLayout();
	
	self.TopBar.Buttons[3] = vgui.Create( "DButton", self.TopBar );
	self.TopBar.Buttons[3]:SetFont( "STALKER.LabelMenu" );
	self.TopBar.Buttons[3]:SetText( "Gear" );
	self.TopBar.Buttons[3]:SetSkin( "StalkerSkin" );
	self.TopBar.Buttons[3]:SetPos( 377, 0 );
	self.TopBar.Buttons[3]:SetSize( 760 / 4, 40 );
	self.TopBar.Buttons[3].m_bTitleButton = true;
	self.TopBar.Buttons[3].DoClick = function( pnl )
	
		if( pnl.m_bIsDown ) then return end
	
		for k,v in next, self.TopBar.Buttons do
		
			v.m_bIsDown = false;
			
		end
		pnl.m_bIsDown = true;
		self:LastPageCallback();
		self.ContentPane:Clear();
		self:Page3();
		
	end
	self.TopBar.Buttons[3]:PerformLayout();

	self.TopBar.Buttons[4] = vgui.Create( "DButton", self.TopBar );
	self.TopBar.Buttons[4]:SetFont( "STALKER.LabelMenu" );
	self.TopBar.Buttons[4]:SetText( "Finish" );
	self.TopBar.Buttons[4]:SetSkin( "StalkerSkin" );
	self.TopBar.Buttons[4]:SetPos( 566, 0 );
	self.TopBar.Buttons[4]:SetSize( 760 / 4, 40 );
	self.TopBar.Buttons[4].m_bTitleButton = true;
	self.TopBar.Buttons[4].DoClick = function( pnl )
	
		if( pnl.m_bIsDown ) then return end
	
		for k,v in next, self.TopBar.Buttons do
		
			v.m_bIsDown = false;
			
		end
		pnl.m_bIsDown = true;
		self:LastPageCallback();
		self.ContentPane:Clear();
		self:Page4();
		
	end
	self.TopBar.Buttons[4]:PerformLayout();
	
	self.ContentPane = vgui.Create( "DPanel", self );
	self.ContentPane:SetSize( 754, 366 );
	self.ContentPane:SetPos( 12, 80 );

	self:Page1();

end

function PANEL:Paint( w, h )

	kingston.gui.FindFunc( self, "Paint", "CharCreateFrame" );

end

vgui.Register( "CharacterCreateVGUI", PANEL, "DFrame" );