AddCSLuaFile();

--- EscapeMenuVGUI ---

local PANEL = { }

function PANEL:Init( )

	GAMEMODE.EscapeMenu = self;
	
	self.player = LocalPlayer();
	self.w, self.h = ScrW( ), ScrH( );
	self.x, self.y = ScrW( ) / 2 - self.w / 2, ScrH( ) / 2 - self.h / 2;
	
	self:SetSize( self.w, self.h );
	self:SetPos( self.x, self.y );
	self:SetTitle( "" );
	self:MakePopup( );
	self:ShowCloseButton( false );
	self:SetAlpha( 0 );
	self:AlphaTo( 255, 0.2, 0 );
	self:SetDraggable( false );
	
	self.leftPanel = vgui.Create( "DPanel", self );
	self.leftPanel:SetSize( self.w * 0.3, self.h );
	self.leftPanel:SetPos( 0 - self.leftPanel:GetWide( ), 0 );
	self.leftPanel:MoveTo( 0, 0, 0.2, 0 );
	self.leftPanel.Paint = function( pnl, w, h )
	
		draw.SimpleText( GetHostName( ), "Trebuchet24", w / 2, 20, Color( 255, 255, 255, 255 ), 1, 1 );
		
	end
	self.leftPanel.PerformLayout = function( )

	end
	
	if( !GAMEMODE.MainMenu ) then
	
		self.leftPanel.MainMenu = vgui.Create( "DButton", self.leftPanel );
		self.leftPanel.MainMenu:SetSize( self.w * 0.3, self.h / 20 );
		self.leftPanel.MainMenu:SetPos( 0, self.leftPanel:GetTall() - ( self.leftPanel.MainMenu:GetTall() * 2 ) );
		self.leftPanel.MainMenu:SetFont( "Stalker2-24" );
		self.leftPanel.MainMenu:SetText( "Main Menu" );
		self.leftPanel.MainMenu:SetSkin( "StalkerUI" );
		self.leftPanel.MainMenu.DoClick = function( pnl )
		
			vgui.Create( "MainMenuVGUI" );
		
		end
		
	end
	
	self.leftPanel.Disconnect = vgui.Create( "DButton", self.leftPanel );
	self.leftPanel.Disconnect:SetSize( self.w * 0.3, self.h / 20 ); -- on 1600 should be 128
	self.leftPanel.Disconnect:SetPos( 0, self.leftPanel:GetTall() - self.leftPanel.Disconnect:GetTall() ); 
	self.leftPanel.Disconnect:SetFont( "Stalker2-24" );
	self.leftPanel.Disconnect:SetText( "Disconnect" );
	self.leftPanel.Disconnect:SetSkin( "StalkerUI" );
	self.leftPanel.Disconnect.DoClick = function( pnl )
	
		local panel = Derma_Query( "Are you sure?", "Disconnect", "Yes", function()
			
			RunConsoleCommand( "disconnect" );
			
		end, "No" );
		panel:SetSkin( "StalkerUI" );
	
	end
	
end

function PANEL:Paint( w, h )

end

function PANEL:Close( )
	if ( self.closing ) then return end
	
	self.closing = true;
	
	self.leftPanel:MoveTo( 0 - self.leftPanel:GetWide( ), 0, 0.2, 0 );
	self:AlphaTo( 0, 0.2, 0, function( )
	
		self:Remove( );
		self = nil;
		
	end )
end

vgui.Register( "EscapeMenuVGUI", PANEL, "DFrame" );