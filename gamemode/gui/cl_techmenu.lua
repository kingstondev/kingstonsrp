local PANEL = {};

function PANEL:Init()

	self:SetSize( ScrW() / 3.7, ScrH() );
	self:SetPos( ScrW() / 10.5, 0 );
	self:SetSkin( "StalkerSkin" );
	self:SetAlpha( 0 );
	self:AlphaTo( 255, 0.2, 0 );
	
	self.ItemDisplay = vgui.Create( "CWeaponDisplay", self );
	self.ItemDisplay:SetPos( ScrW() / 100, ScrH() / 5 );
	self.ItemDisplay:SetSize( ( ScrW() / 3.9 ) - ( ( ScrW() / 100 ) * 2 ), ScrH() / 3.5 );
	
	self.Container = vgui.Create( "DScrollPanel", self );
	self.Container:SetPos( ScrW() / 100, ScrH() / 2 );
	self.Container:SetSize( ( ScrW() / 3.9 ) - ( ( ScrW() / 100 ) * 2 ), ScrH() / 2.1 );

end

function PANEL:SendToMenu( ItemObj )

	local metaitem = GAMEMODE:GetMeta( ItemObj:GetClass() );
	if( !metaitem.CanUpgrade or !ItemObj:CanUpgrade() ) then
	
		return false
		
	end
	
	self.ItemDisplay:SetModel( metaitem.Model );

	local y = 5;
	for k,v in next, GAMEMODE.Upgrades do
	
		if( v.Item != ItemObj:GetClass() ) then continue end
	
		local upgrade = vgui.Create( "CUpgradeButton", self.Container );
		upgrade:SetPos( 5, y );
		upgrade:SetSize( 110, 50 );
		upgrade.UpgradeID = k;
		upgrade:SetUpgradeImage( v.IconX, v.IconY );
		upgrade.CurrentItem = ItemObj;
		
		for _,class in next, ItemObj:GetVar( "Upgrades", {} ) do
		
			if( class == k ) then
		
				upgrade.ItemHasUpgrade = true;
				break;
				
			end
			
		end
		
		if( !v.CanUpgrade( v, ItemObj ) ) then

			upgrade:SetDisabled( true );
		
		end
		
		y = y + 60;
	
	end

end

function PANEL:Paint( w, h )
	
	kingston.gui.FindFunc( self, "Paint", "TechMenuFrame" );

end

vgui.Register( "CTechMenu", PANEL, "DPanel" );