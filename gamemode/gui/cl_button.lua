AddCSLuaFile();

local PANEL = {};

function PANEL:Init()

	local main = self;

	function self.m_Image:Paint()
	
		surface.SetDrawColor( Color( 255, 255, 255, 255 ) );
		surface.SetMaterial( self:GetMaterial() );
		surface.DrawTexturedRect( 0, 0, main:GetWide(), main:GetTall() );
		surface.SetTextColor( main:GetTextColor() );
		surface.SetFont( main:GetFont() );
		local w,h = surface.GetTextSize( main:GetText() );
		surface.SetTextPos( ( main:GetWide() / 2 ) - ( w / 2 ), ( main:GetTall() / 2 ) - ( h / 2 ) );
		surface.DrawText( main:GetText() );
		
	end
	
end

function PANEL:SetDownImage( szPath )

	self.m_DownImage = Material( szPath );

end

function PANEL:GetDownImageMaterial()

	return self.m_DownImage;

end

function PANEL:OnMousePressed()
	
	self.m_Original = self.m_Image:GetMaterial();
	self.m_Image:SetMaterial( self:GetDownImageMaterial() );

end

function PANEL:OnMouseReleased()

	self.m_Image:SetMaterial( self.m_Original );
	self:DoClick();

end

vgui.Register( "CImageButton", PANEL, "DImageButton" );