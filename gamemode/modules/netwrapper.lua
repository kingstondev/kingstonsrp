AddCSLuaFile();

MsgC( Color( 0, 255, 0 ), "Net Loaded\n" );

/*
	This system has been deprecated in favor of netstream 2, expressly due to the fact
	there seems to be no efficient way to have both the client and server assure transmission
	of net messages. Whilst this does have encoding support for optimal packet size, and the server
	does assure transmission of messages to the client, it seems to be more effective to simply
	send netmessages at the right times. Instead of the server trying to guess when the client
	has finished connecting, the client requests all information about the server during InitPostEntity.
*/

wznet = wznet or {};
wznet.Messages = wznet.Messages or {};
wznet.ActiveMsg = wznet.ActiveMsg or nil;
wznet.ReceiverFunctions = wznet.ReceiverFunctions or {};
wznet.TransmitBuffer = wznet.TransmitBuffer or {};

if( SERVER ) then

	util.AddNetworkString( "KingstonNet" );
	util.AddNetworkString( "ServerReceivedConfirmation" );
	
end

function wznet.Start(netstr, unreliable, confirm)

	local msg = 
	{

		m_NetMsg = netstr,
		m_Unreliable = unreliable,
		m_Confirm = confirm,
		m_Values = {},

	};

	wznet.Messages[#wznet.Messages + 1] = msg;
	wznet.ActiveMsg = msg;

end

function wznet.WriteTable(tbl)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "Table",
		m_Value = { tbl },

	};

end

function wznet.WriteFloat(float)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "Float",
		m_Value = { float },

	};

end

function wznet.WriteDouble(double)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "Double",
		m_Value = { double },

	};

end

function wznet.WriteInt(int, bits)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "Int",
		m_Value = { int, bits or 32 },

	};

end

function wznet.WriteUInt(uint, bits)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "UInt",
		m_Value = { uint, bits or 32 },

	};

end

function wznet.WriteBit(bbit)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "Bit",
		m_Value = { bbit },

	};

end

function wznet.WriteString(szstring)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "String",
		m_Value = { szstring },

	};

end

function wznet.WriteVector(vector)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "Vector",
		m_Value = { vector },

	};

end

function wznet.WriteAngle(angle)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "Angle",
		m_Value = { angle },

	};

end

function wznet.WriteEntity(entity)

	wznet.ActiveMsg.m_Values[#wznet.ActiveMsg.m_Values + 1] = 
	{

		m_Type = "Entity",
		m_Value = { entity },

	};

end

function wznet.Process(msg)

	if( SERVER ) then

		if(not istable(msg.m_SendTo))
		then
		
			if(not msg.m_SendTo:IsValid())	
			then

				return;

			end
			
		end
		
		if(not istable(msg.m_SendTo))
		then
		
			printf( COLOR_WHITE, "Sending NetMsg '%s' to '%s'", msg.m_NetMsg, msg.m_SendTo:Name() );
			
		else
		
			for k,v in next, msg.m_SendTo 
			do
			
				printf( COLOR_WHITE, "Sending NetMsg '%s' to '%s'", msg.m_NetMsg, v:Name() );
				
			end
			
		end
		
	else
	
		printf( COLOR_WHITE, "Sending NetMsg '%s' to server.", msg.m_NetMsg );
	
	end
	
	if( !msg.m_Confirm ) then

		net.Start(msg.m_NetMsg, msg.m_Unreliable);
		
			for k, v in next, msg.m_Values
			do

				net["Write" .. v.m_Type](unpack(v.m_Value));

			end

		net.Send(msg.m_SendTo);
		
	else
	
		net.Start( "KingstonNet", msg.m_Unreliable );
		
			local data = {};
			for k, v in next, msg.m_Values
			do

				data[#data + 1] = v.m_Value[1];

			end
			local encodedData = pon.encode( data );
			
			net.WriteString( msg.m_NetMsg );
			net.WriteUInt( #encodedData, 32 );
			net.WriteData( encodedData, #encodedData );
			
		if( SERVER ) then

			net.Send( msg.m_SendTo );
			
		else
		
			net.SendToServer();
			
		end
		
	end

end

function wznet.Send(ply)

	if(not wznet.ActiveMsg)
	then

		return;

	end

	if(not istable(ply))
	then
	
		if(not ply or not ply:IsValid())
		then

			wznet.ActiveMsg = nil;
			table.remove(wznet.Messages, #wznet.Messages);
			return;

		end
	
	end

	local msg = wznet.ActiveMsg;
	msg.m_SendTo = ply;
	
	wznet.ActiveMsg = nil;

	if(not istable(ply))
	then
	
		if(not ply:GetIsReadyToReceiveNetMsgs() and not ply:IsBot())
		then

			return;

		end
		
	else
	
		for k,v in next, ply
		do
		
			if(not v:GetIsReadyToReceiveNetMsgs() and not ply:IsBot())
			then
			
				table.remove(msg.m_SendTo, k);
				
			end
			
		end
		
	end

	table.remove( wznet.Messages, #wznet.Messages );
	if( msg.m_Confirm ) then
		table.insert( wznet.TransmitBuffer, msg );
	end
	wznet.Process(msg);

end

function wznet.Broadcast()

	if(not wznet.ActiveMsg)
	then

		return;

	end

	local msg = wznet.ActiveMsg;
	wznet.ActiveMsg = nil;

	table.remove(wznet.Messages, #wznet.Messages);

	for k, v in next, player.GetAll()
	do

		wznet.Start(msg.m_NetMsg);

			wznet.ActiveMsg.m_Values = msg.m_Values;

		wznet.Send(v);

	end

end

function wznet.SendOmit(ply)

	if(not wznet.ActiveMsg)
	then

		return;

	end

	local tbl = ply;
	if(type(tbl) ~= "table")
	then

		tbl = { ply };

	end

	local msg = wznet.ActiveMsg;
	wznet.ActiveMsg = nil;

	table.remove(wznet.Messages, #wznet.Messages);

	for k, v in next, player.GetAll()
	do

		if(table.HasValue(tbl, v))
		then

			continue;

		end

		wznet.Start(msg.m_NetMsg);

			wznet.ActiveMsg.m_Values = msg.m_Values;

		wznet.Send(v);

	end

end

function wznet.SendPVS(pos)

	if(not wznet.ActiveMsg)
	then

		return;

	end

	local msg = wznet.ActiveMsg;
	wznet.ActiveMsg = nil;

	table.remove(wznet.Messages, #wznet.Messages);

	for k, v in next, player.GetAll()
	do

		if(not v:TestPVS(pos))
		then

			continue;

		end

		wznet.Start(msg.m_NetMsg);

			wznet.ActiveMsg.m_Values = msg.m_Values;

		wznet.Send(v);

	end

end

if( CLIENT ) then

	function wznet.SendToServer()

		if(not wznet.ActiveMsg)
		then

			return;

		end

		local msg = wznet.ActiveMsg;
		wznet.ActiveMsg = nil;
		
		msg.m_StartTime = RealTime();
		msg.m_CheckTime = .5;

		table.remove( wznet.Messages, #wznet.Messages );
		if( msg.m_Confirm ) then
			table.insert( wznet.TransmitBuffer, msg );
		end
		wznet.Process(msg);

	end

end

function wznet.Receive( szNetMsg, nCallback )

	wznet.ReceiverFunctions[szNetMsg] = nCallback;

end

net.Receive( "KingstonNet", function( len, ply )

	local szNetMsg = net.ReadString();
	local nDataLength = net.ReadUInt( 32 );
	local nData = net.ReadData( nDataLength );

	if( SERVER ) then
	
		if( szNetMsg and nDataLength and nData ) then
		
			local decodedData = pon.decode( nData );
		
			if( wznet.ReceiverFunctions[szNetMsg] ) then
			
				wznet.ReceiverFunctions[szNetMsg]( ply, unpack( decodedData ) );
				
				net.Start( "ServerReceivedConfirmation" );
				net.Send( ply );
				
			end
			
		end
		
	else
	
		if( szNetMsg and nDataLength and nData ) then
		
			local decodedData = pon.decode( nData );
			
			if( wznet.ReceiverFunctions[szNetMsg] ) then
			
				wznet.ReceiverFunctions[szNetMsg]( unpack( decodedData ) );
				
			end
			
		end
		
	end

end );

if( CLIENT ) then

	hook.Add( "Think", "CheckActiveTransmissions", function()
	
		for _,msg in next, wznet.TransmitBuffer do
		
			if( msg.m_StartTime + msg.m_CheckTime > RealTime() ) then
			
				wznet.Process( msg );
			
			end
		
		end
	
	end );

	net.Receive( "ServerReceivedConfirmation", function( len )
	
		printf( COLOR_WHITE, "TRANSMISSION CONFIRMED FOR %d", #wznet.TransmitBuffer );
		table.remove( wznet.TransmitBuffer, #wznet.TransmitBuffer );
	
	end );

end

if( SERVER ) then

	wznet.Receive( "TestTest", function( ply, hello, dude )


	end );
	
end

local Player = FindMetaTable("Player");

function Player:GetIsReadyToReceiveNetMsgs()

	return self.m_bReadyToReceiveNetMsgs;

end

function Player:SetIsReadyToReceiveNetMsgs(rdy)

	self.m_bReadyToReceiveNetMsgs = rdy;

end

hook.Add("SetupMove", "WZNET_ReadyToSend", function(ply, mv, cmd)

	if(not ply:GetIsReadyToReceiveNetMsgs() and cmd:TickCount() ~= 0 and not cmd:IsForced())
	then

		local i = 1;
		while (i <= #wznet.Messages)
		do

			local msg = wznet.Messages[i];
			if(msg.m_SendTo == ply)
			then

				wznet.Process(msg);
				table.remove(wznet.Messages, i);

			else

				i = i + 1;

			end

		end

		ply:SetIsReadyToReceiveNetMsgs(true);

	end

end);