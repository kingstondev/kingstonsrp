AddCSLuaFile();

GM.ChatCommands = { };

function GM:AddChatCommand( cmd, col, func ) -- instead of including how people can hear, we want a func for it. ex: CanHear
	
	table.insert( self.ChatCommands, { cmd, col, func } );
	
end

function GM:GetChatCommand( text )
	
	local tab = self.ChatCommands;
	
	table.sort( tab, function( a, b )
		
		return string.len( a[1] ) > string.len( b[1] );
		
	end );
	
	for _, v in next, tab do
		
		if( text ) then
			
			if( string.find( string.lower( text ), string.lower( v[1] ), nil, true ) == 1 ) then
				
				return v;
				
			end
			
		end
		
	end
	
	return false;
	
end

function GM:FormatLine( str, font, size )
	
	if( string.len( str ) == 1 ) then return str, 0 end
	
	local start = 1;
	local c = 1;
	
	surface.SetFont( font );
	
	local endstr = "";
	local n = 0;
	local lastspace = 0;
	local lastspacemade = 0;
	
	while( string.len( str or "" ) > c ) do
	
		local sub = string.sub( str, start, c );
	
		if( string.sub( str, c, c ) == " " ) then
			lastspace = c;
		end
		
		if( surface.GetTextSize( sub ) >= size and lastspace ~= lastspacemade ) then
			
			local sub2;
			
			if( lastspace == 0 ) then
				lastspace = c;
				lastspacemade = c;
			end
			
			if( lastspace > 1 ) then
				sub2 = string.sub( str, start, lastspace - 1 );
				c = lastspace;
			else
				sub2 = string.sub( str, start, c );
			end
			
			endstr = endstr .. sub2 .. "\n";
			
			lastspace = c + 1;
			lastspacemade = lastspace;
			
			start = c + 1;
			n = n + 1;
		
		end
	
		c = c + 1;
	
	end
	
	if( start < string.len( str or "" ) ) then
	
		endstr = endstr .. string.sub( str or "", start );
	
	end
	
	return endstr, n;

end

function GM:OnChat( ply, text )

	if( self:GetChatCommand( text ) ) then

		local cc = self:GetChatCommand( text );
		local f = string.sub( text, string.len( cc[1] ) + 1 );
		
		if( SERVER ) then
			
			local ret = cc[3]( ply, f, nil, cc );
			
			netstream.Start( ret, "Say", ply, text );
			
		else
			
			if( ply == LocalPlayer() ) then
				
				cc[3]( ply, f, true, cc );
				
			else
				
				cc[3]( ply, f, nil, cc );
				
			end
			
		end
		
	else
		
		if( text == "" ) then return end
		if( SERVER ) then
			
			local rf = { };
			for _, v in next, player.GetAllBut( ply ) do
				
				if( v:GetPos():Distance( ply:GetPos() ) < 500 ) then
					
					table.insert( rf, v );
					
				end
				
			end
			
			netstream.Start( rf, "CCLocal", ply, text );
			
			self:Log( "chat", " ", ply:RPName() .. ": " .. text, ply );
			
		else
			
			GAMEMODE:AddChat( CB_IC, "ChatNormal", Color( 114, 160, 193, 255 ), ply, ": ", text );
			
		end
		
	end
	
end

if( CLIENT ) then

	local function CCLocal( ply, text )

		GAMEMODE:AddChat( CB_IC, "ChatNormal", Color( 114, 160, 193, 255 ), ply, ": ", text );
		
	end
	netstream.Hook( "CCLocal", CCLocal );
	
	local function kngRadio( ply, item, text )
	
		-- since client is only exposed to own items
		-- no need to check if is owner like on server.
		local ItemObj = GAMEMODE.g_ItemTable[item];
		if( ItemObj ) then
		
			GAMEMODE:AddChat( CB_RADIO, "ChatNormal", Color( 0, 255, 0 ), "["..ItemObj:GetVar( "Frequency", 100.1 ).." kHz] ", ply:RPName(), ":", text );
		
		end
	
	end
	netstream.Hook( "kngRadio", kngRadio );
	
end

local function ccWhisper( ply, arg, l, cc )
	
	if( arg == "" ) then return end
	
	if( SERVER ) then
		
		GAMEMODE:Log( "chat", "W", ply:RPName() .. ": " .. arg, ply );
		
		local rf = { };
		for _, v in next, player.GetAllBut( ply ) do
			
			if( v:GetPos():Distance( ply:GetPos() ) < 200 ) then
				
				table.insert( rf, v );
				
			end
			
		end
		
		return rf;
		
	else
		
		GAMEMODE:AddChat( CB_IC, "ChatSmall", cc[2], "[Whisper] ", ply, ":", arg );
		
	end
	
end
GM:AddChatCommand( "/w", Color( 200, 200, 200, 255 ), ccWhisper );

local function ccYell( ply, arg, l, cc )
	
	if( arg == "" ) then return end
	
	if( SERVER ) then
		
		GAMEMODE:Log( "chat", "Y", ply:RPName() .. ": " .. arg, ply );
		
		local rf = { };
		
		for _, v in next, player.GetAllBut( ply ) do
			
			if( v:GetPos():Distance( ply:GetPos() ) < 1000 ) then
				
				table.insert( rf, v );
				
			end
			
		end
		
		return rf;
		
	else
		
		GAMEMODE:AddChat( CB_IC, "ChatBig", cc[2], "[Yell] "..ply:RPName()..":"..arg, nil, ply );
		
	end
	
end
GM:AddChatCommand( "/y", Color( 255, 0, 0, 255 ), ccYell );

local function ccMe( ply, arg, l, cc )
	
	if( arg == "" ) then return end
	
	if( SERVER ) then
		
		GAMEMODE:Log( "chat", "M", ply:RPName() .. " " .. arg, ply );
		
		local rf = { };
		
		for _, v in next, player.GetAllBut( ply ) do
			
			if( v:GetPos():Distance( ply:GetPos() ) < 500 ) then
				
				table.insert( rf, v );
				
			end
			
		end
		
		return rf;
		
	else
		
		GAMEMODE:AddChat( CB_IC, "ChatNormal", cc[2], ply:RPName() .. arg, nil, ply );
		
	end
	
end
GM:AddChatCommand( "/me", Color( 255, 150, 50, 255 ), ccMe );
GM:AddChatCommand( "/e", Color( 255, 150, 50, 255 ), ccMe );

local function ccIt( ply, arg, l, cc )
	
	if( arg == "" ) then return end
	
	if( SERVER ) then
		
		GAMEMODE:Log( "chat", "I", "[" .. ply:RPName() .. "] " .. arg, ply );
		
		local rf = { };
		
		for _, v in next, player.GetAllBut( ply ) do
			
			if( v:GetPos():Distance( ply:GetPos() ) < 500 ) then
				
				table.insert( rf, v );
				
			end
			
		end
		
		return rf;
		
	else
		
		GAMEMODE:AddChat( CB_IC, "ChatNormal", cc[2], "**", arg );
		
	end
	
end
GM:AddChatCommand( "/it", Color( 255, 150, 50, 255 ), ccIt );

local function ccLocalOOC( ply, arg, l, cc )
	
	if( arg == "" ) then return end
	
	if( SERVER ) then
		
		GAMEMODE:Log( "chat", "o", ply:RPName() .. ": " .. arg, ply );
		
		local rf = { };
		
		for _, v in next, player.GetAllBut( ply ) do
			
			if( v:GetPos():Distance( ply:GetPos() ) < 500 ) then
				
				table.insert( rf, v );
				
			end
			
		end
		
		return rf;
		
	else
		
		GAMEMODE:AddChat( CB_OOC, "ChatNormalItalic", cc[2], "[LOOC] " .. ply:RPName() .. ":" .. arg, nil, ply );
		
	end
	
end
GM:AddChatCommand( "[[", Color( 50, 200, 255, 255 ), ccLocalOOC );
GM:AddChatCommand( ".//", Color( 50, 200, 255, 255 ), ccLocalOOC );
GM:AddChatCommand( "/looc", Color( 50, 200, 255, 255 ), ccLocalOOC );

local function ccOOC( ply, arg, l, cc )
	
	if( arg == "" ) then return end
	
	if( SERVER ) then
		
		GAMEMODE:Log( "chat", "O", ply:RPName() .. ": " .. arg, ply );
		
		return player.GetAllBut( ply );
		
	else
		
		GAMEMODE:AddChat( CB_OOC, "ChatNormal", Color( 255, 50, 50 ), "[OOC] ", team.GetColor( ply:Team() ), ply, color_white, ":", arg );
		
	end
	
end
GM:AddChatCommand( "//", Color( 50, 150, 255, 255 ), ccOOC );
GM:AddChatCommand( "/ooc", Color( 50, 150, 255, 255 ), ccOOC );

local function ccAdmin( ply, arg, l, cc )
	
	if( arg == "" ) then return end
	
	if( SERVER ) then
		
		GAMEMODE:Log( "chat", "A", ply:RPName() .. ": " .. arg, ply );
		
		local rf = { };
		
		for _, v in next, player.GetAllBut( ply ) do
			
			if( v:IsAdmin() ) then
				
				table.insert( rf, v );
				
			end
			
		end
		
		return rf;
		
	else
		
		GAMEMODE:AddChat( CB_OOC, "ChatNormal", cc[2], "[Admin] " .. ply:RPName() .. ":" .. arg, nil, ply );
		
	end
	
end
GM:AddChatCommand( "/a", Color( 255, 70, 70, 255 ), ccAdmin );

local function kngRadio( ply, arg, l, cc )

	if( arg == "" ) then return end
	if( l ) then
	
		-- #ply.ActiveRadios does not continue counting when it encounters nil.
		-- because our indices keys are the ID of the item, 1 is usually going to be nil.
		if( !ply.ActiveRadios or table.Count( ply.ActiveRadios ) == 0 ) then return end
		
	end
	if( SERVER ) then
	
		GAMEMODE:Log( "chat", "R", ply:RPName()..": ".. arg, ply );
	
		local curFreq = 0;
		local curItem;

		for k,v in next, ply.ActiveRadios do
		
			if( !v:GetVar( "PrimaryRadio", false ) ) then continue end
		
			curFreq = v:GetVar( "Frequency", 0 );
			curItem = v;
			break;
			
		end

		if( curFreq == 0 ) then return end
	
		for k,v in next, player.GetAll() do
			
			if( !v.ActiveRadios ) then continue end
			for m,n in next, v.ActiveRadios do
			
				if( n:GetVar( "Frequency", 0 ) != curFreq ) then continue end
				netstream.Start( v, "kngRadio", ply, n:GetID(), arg ); 
			
			end
			
		end
	
	end

end
GM:AddChatCommand( "/r", Color( 0, 255, 0, 255 ), kngRadio );