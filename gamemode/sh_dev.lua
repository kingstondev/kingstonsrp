AddCSLuaFile();

DEV_MODE = true;

function printf( color, str, ... )

	if( DEV_MODE ) then
	
		if( CLIENT ) then
	
			MsgC( color, string.format( "[CLIENT] "..str.."\n", ... ) );
		
		elseif( SERVER ) then
		
			MsgC( color, string.format( "[SERVER] "..str.."\n", ... ) );
		
		end
		
	end

end

if( DEV_MODE ) then

	hook.Add( "PostRenderVGUI", "dev", function()
	
		local w, h = ScrW( ), ScrH( )
		
		draw.RoundedBox( 0, w - 260, 30, 260, 215, Color( 0, 0, 0, 100 ) )
		draw.SimpleText( "DEVELOPMENT MODE", "DebugFixed", w - 10, 45, Color( 255, 0, 0, 255 ), TEXT_ALIGN_RIGHT, 1 )
		draw.SimpleText( "FRAME TIME - " .. math.Round( 1 / FrameTime( ) ), "DebugFixed", w - 10, 65, Color( 0, 255, 0, 255 ), TEXT_ALIGN_RIGHT, 1 )
		draw.SimpleText( "CUR TIME - " .. math.Round( CurTime( ) ), "DebugFixed", w - 10, 90, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, 1 )
		draw.SimpleText( "SYS TIME - " .. math.Round( SysTime( ) ), "DebugFixed", w - 10, 110, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, 1 )
		draw.SimpleText( "REAL TIME - " .. math.Round( RealTime( ) ), "DebugFixed", w - 10, 130, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, 1 )
		draw.SimpleText( "FRAME NUMBER - " .. FrameNumber( ), "DebugFixed", w - 10, 150, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, 1 )
		draw.SimpleText( "SIZE MODE - (wide:" .. w .. ", tall:" .. h .. ")", "DebugFixed", w - 10, 170, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, 1 )
		draw.SimpleText( "OS TIME - " .. os.time( ), "DebugFixed", w - 10, 190, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, 1 )
		
		local pos, ang = LocalPlayer():GetPos( ), LocalPlayer():GetAngles( )
		draw.SimpleText( "POS - <" .. math.Round( pos.x ) .. ", " .. math.Round( pos.y ) .. ", " .. math.Round( pos.z ) .. ">", "DebugFixed", w - 10, 210, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, 1 )
		draw.SimpleText( "ANG - <" .. math.Round( ang.p ) .. ", " .. math.Round( ang.y ) .. ", " .. math.Round( ang.r ) .. ">", "DebugFixed", w - 10, 230, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, 1 )

	end )

end