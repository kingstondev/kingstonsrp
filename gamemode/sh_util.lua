AddCSLuaFile();

local s_Meta = FindMetaTable( "Player" );

local whitelist = {
    ["italic"] = true
};

local CHAR_LT = 60
local CHAR_GT = 62
local CHAR_FSLASH = 47

local stbl = {
    ["<"] = "",
    [">"] = ""
}

function escape_markup(s) -- by firehawk
    local function wreplace(str)
        local tag = str:sub(1,1)
        local tag2 = ""
        local markup = str:sub(2)
        
        local lessthan = false
        
        if tag:byte() ~= CHAR_LT then
            tag = str:sub(-1)
            markup = str:sub(1, -2)
        else
            tag2 = str:sub(2, 2)
            
            lessthan = true
            
            if tag2:byte() ~= CHAR_FSLASH then
                tag2 = ""

            else
                markup = str:sub(3)
            end
        end
        
        if whitelist[markup] then
            return str
        end
        
        return lessthan and (stbl[tag] .. tag2 .. markup) or (markup .. stbl[tag])
    end
    
    return s:gsub("<%/%a+", wreplace):gsub("<%a+", wreplace):gsub("%a+>", wreplace)
end

function player.GetAllBut( ply )
	
	local ret = { };
	
	for _, v in pairs( player.GetAll() ) do
		
		if( v != ply ) then
			
			table.insert( ret, v );
			
		end
		
	end
	
	return ret;
	
end

function s_Meta:Notify( color, text, ... )

	if( CLIENT ) then
	
		GAMEMODE:AddChat( CB_OOC, "Rundschrift-16", color, Format( text, ... ), nil, ply );
		
	elseif( SERVER ) then
		
		netstream.Start( self, "NotifyPlayer", color, text, {...} );
	
	end

end

function GM:CanSeePos( pos1, pos2, filter )
	
	local trace = { };
	trace.start = pos1;
	trace.endpos = pos2;
	trace.filter = filter;
	trace.mask = MASK_SOLID + CONTENTS_WINDOW + CONTENTS_GRATE;
	local tr = util.TraceLine( trace );
	
	if( tr.Fraction == 1.0 ) then
		
		return true;
		
	end
	
	return false;
	
end

function s_Meta:CanSeePlayer( ply )
	
	return GAMEMODE:CanSeePos( self:EyePos(), ply:EyePos(), { self, ply } );
	
end

function util.wrapText(text, width, font) // thanks chessnut
	font = font or "Rundschrift-16"
	surface.SetFont(font)

	local exploded = string.Explode("%s", text, true)
	local line = ""
	local lines = {}
	local w = surface.GetTextSize(text)
	local maxW = 0

	if (w <= width) then
		return {(text:gsub("%s", " "))}, w
	end

	for i = 1, #exploded do
		local word = exploded[i]
		line = line.." "..word
		w = surface.GetTextSize(line)

		if (w > width) then
			lines[#lines + 1] = line
			line = ""

			if (w > maxW) then
				maxW = w
			end
		end
	end

	if (line != "") then
		lines[#lines + 1] = line
	end

	return lines, maxW
end

function BoolToNumber( bool )

	if( bool ) then

		return 1;
		
	else

		return 0;
		
	end
	
end

if( CLIENT ) then

	netstream.Hook( "NotifyPlayer", function( color, text, varargs )
		
		LocalPlayer():Notify( color, text, unpack( varargs ) );
	
	end );
	
end

-- again thanks to chessnut for this code (from nutscript)

function util.include(fileName, state)
	if (!fileName) then
		error("No file name specified for including.")
	end

	-- Only include server-side if we're on the server.
	if ((state == "server" or fileName:find("sv_")) and SERVER) then
		include(fileName)
	-- Shared is included by both server and client.
	elseif (state == "shared" or fileName:find("sh_")) then
		if (SERVER) then
			-- Send the file to the client if shared so they can run it.
			AddCSLuaFile(fileName)
		end

		include(fileName)
	-- File is sent to client, included on client.
	elseif (state == "client" or fileName:find("cl_")) then
		if (SERVER) then
			AddCSLuaFile(fileName)
		else
			include(fileName)
		end
	end
end

-- Include files based off the prefix within a directory.
function util.IncludeDir(directory, fromLua, recursive)
	local baseDir = "kingstonsrp"
	baseDir = baseDir.."/gamemode/"
	
	if recursive then
		local function AddRecursive(folder)
			local files, folders = file.Find(folder.."/*", "LUA")
			if (!files) then MsgN("Warning! This folder is empty!") return end

			for k, v in pairs(files) do
				util.include(folder .. "/" .. v)
			end

			for k, v in pairs(folders) do
				AddRecursive(folder .. "/" .. v)
			end
		end
		AddRecursive((fromLua and "" or baseDir)..directory)
	else
		-- Find all of the files within the directory.
		for k, v in ipairs(file.Find((fromLua and "" or baseDir)..directory.."/*.lua", "LUA")) do
			-- Include the file from the prefix.
			util.include(directory.."/"..v)
		end
	end
end