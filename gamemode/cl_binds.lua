function GM:PlayerBindPress( ply, szBind, bPressed )

	if( bPressed and string.find( szBind, "messagemode" ) ) then
		
		self:CreateChatbox();
		return true;
		
	end

	if( bPressed and szBind == "gm_showspare1" ) then
		-- f3
		self:ShowInventory();
		return true;
		
	end
	
	if( bPressed and szBind == "gm_showteam" ) then
		-- f2
		self:ShowCharacterCard( LocalPlayer() );
		return true;
	
	end
	
	if( bPressed and szBind == "gm_showhelp" ) then
		-- f1
		self:ShowHelpMenu();
		return true;
		
	end
	
	if( bPressed and szBind == "gm_showspare2" ) then
		-- f4
		self:ShowAdminMenu();
		return true;
		
	end
	
	if( bPressed and string.find( szBind, "slot" ) ) then

		if( !LocalPlayer():GetActiveWeapon() or !LocalPlayer():GetActiveWeapon():IsValid() ) then return true end
		
		local n = string.gsub( szBind, "slot", "" );
		
		if( tonumber( n ) and tonumber( n ) >= 1 and tonumber( n ) <= 5 ) then

			local wep = self:GetWeaponBySlot( tonumber( n ) );
			
			if( LocalPlayer():HasWeapon( wep ) ) then
				
				LocalPlayer():SelectWeapon( wep );
				
			end
			
		end
		
		return true;
		
	end
	
	if( bPressed and string.find( szBind, "rp_toggleholster" ) and !input.IsKeyDown( KEY_B ) ) then
		
		if( LocalPlayer():GetActiveWeapon() and LocalPlayer():GetActiveWeapon():IsValid() and LocalPlayer():GetActiveWeapon() != NULL ) then
			
			if( ALWAYS_RAISED[LocalPlayer():GetActiveWeapon():GetClass()] ) then
			
				return false;
				
			end
			
		end
		
		netstream.Start( "nToggleHolster" );
		
		if( LocalPlayer():GetActiveWeapon() != NULL ) then
			
			LocalPlayer():SetRaised( !LocalPlayer():Raised() );
			
		end
		
		return true;
		
	end
	
	return self.BaseClass:PlayerBindPress( ply, szBind, bPressed );

end

function GM:GetWeaponSlot()
	
	if( !LocalPlayer():GetActiveWeapon() or !LocalPlayer():GetActiveWeapon():IsValid() ) then return 1 end
	if( LocalPlayer():GetActiveWeapon():GetClass() == "weapon_physgun" ) then return 4 end
	if( LocalPlayer():GetActiveWeapon():GetClass() == "gmod_tool" ) then return 5 end
	
	return 1;
	
end

function GM:GetWeaponBySlot( i )

	if( i == 1 ) then return "kingston_hands" end
	if( i == 2 ) then
	
		if( LocalPlayer().EquippedWeapons and LocalPlayer().EquippedWeapons[WEAPON_SECONDARY] ) then
	
			return LocalPlayer().EquippedWeapons[WEAPON_SECONDARY];
			
		end
	
	end
	if( i == 3 ) then 

		if( LocalPlayer().EquippedWeapons and LocalPlayer().EquippedWeapons[WEAPON_PRIMARY] ) then
	
			return LocalPlayer().EquippedWeapons[WEAPON_PRIMARY];
			
		end
		
	end
	if( i == 4 ) then return "weapon_physgun" end
	if( i == 5 ) then return "gmod_tool" end
	
	return "";
	
end

function GM:KeyPress( ply, key )
	
	if( key == IN_RELOAD and !input.IsKeyDown( KEY_E ) and !magazineselect.open ) then

		local wep = ply:GetActiveWeapon();
		if wep.ShotgunReload then return end;
		
		if( wep and wep:IsValid() and GAMEMODE.WeaponMagazines[wep:GetClass()] ) then
		
			magazineselect.opensignal()
			
		end
		
	end
	
end

function GM:KeyRelease( ply, key )
	
	if( key == IN_RELOAD ) then
		
		if( ply:GetActiveWeapon() and ply:GetActiveWeapon():IsValid() and GAMEMODE.WeaponMagazines[ply:GetActiveWeapon():GetClass()] ) then
			
			if( magazineselect.open ) then
				
				magazineselect.closesignal()
				
			end
			
		end
		
	end
	
end