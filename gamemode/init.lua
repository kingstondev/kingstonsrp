-- move includes into sv_includes.lua

include( "shared.lua" );
include( "sv_csluafiles.lua" );
include( "sv_mysql.lua" );
include( "sv_admin.lua" );
include( "sv_itemload.lua" );
include( "sv_player.lua" );
include( "sv_binds.lua" );
include( "sv_chat.lua" );
include( "sv_character.lua" );
include( "sv_inventory.lua" );
include( "sv_items.lua" );
include( "sv_factions.lua" );
include( "sv_artifacts.lua" );

GM.g_CharacterTable = {};
GM.g_ItemTable = {};
GM.NextCheck = 0;

function GM:Initialize()
	
	local function onConnection()
	
		self:LoadPermissions();
		self:LoadFactionSpawns();
		self:LoadBans();
	
	end
	wzsql:Connect( onConnection );
	self:SetupDataDirectories();
	
	if( DEV_MODE ) then
	
		game.ConsoleCommand( "sv_allowcslua 1\n" );
		
	end
	
	game.ConsoleCommand( "sv_voiceenable 0\n" );
	game.ConsoleCommand( "mp_falldamage 1\n" );
	
	-- sv_admin gets loaded to early, thus we cannot add cmds
	-- to the table when AddAdmin is called because
	-- it is inside of the gamemode table.
	local callbacks,autocomplete = concommand.GetTable();
	for k,v in next, callbacks do
	
		if( string.StartWith( k, "rpa" ) ) then
		
			self.Commands[k] = true;
			
		end
		
	end

end

function GM:Think()

	if( RealTime() > self.NextCheck ) then

		for k,v in next, self.g_InventoryTable do

			if( !IsValid( v.owner ) or !v.owner ) then

				table.remove( self.g_InventoryTable, k );
				
			end
		
		end
		
		for k,v in next, self.g_ItemTable do

			if( !IsValid( v.owner ) or !v.owner ) then
			
				self.g_ItemTable[k] = nil;
				
			end
			
		end
		
		local function onSuccess( ret )	
		end
		wzsql.Query( "SHOW STATUS LIKE 'Uptime'", onSuccess );
		
		self.NextCheck = RealTime() + 10;
		
	end
	
end

function GM:ShutDown()

	self:SavePermissions();
	self:SaveFactionSpawns();
	self:DeleteDroppedItems();

end

function GM:GetGameDescription()

	return "Kingston";
	
end

--move to sv_log.lua
function GM:SetupDataDirectories()
	
	file.CreateDir( "stalker" );
	file.CreateDir( "stalker/logs" );
	file.CreateDir( "stalker/logs/" .. os.date( "%y-%m-%d" ) );
	
end

function GM:Log( filename, prefix, str, ply )
	
	local f = "stalker/logs/" .. os.date( "%y-%m-%d" ) .. "/" .. filename .. ".txt";
	local write = os.date( "%m-%H-%S" ) .. "\t[" .. prefix .. "]\t" .. str;
	
	if( ply and ply:IsValid() ) then
		
		write = os.date( "%m-%H-%S" ) .. "\t" .. ply:SteamID() .. "\t[" .. prefix .. "]\t" .. str;
		
	end
	
	if( file.Exists( f, "DATA" ) ) then
		
		file.Append( f, "\n" .. write );
		
	else
		
		file.Write( f, write );
		
	end
	
end