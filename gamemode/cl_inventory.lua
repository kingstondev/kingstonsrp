function GM:ShowInventory()

	if( !GAMEMODE.Inventory ) then
	
		vgui.Create( "DInventoryPanel" );
		
	else
	
		GAMEMODE.Inventory:Close();
		
	end

end

netstream.Hook( "LoadInventories", function( s_InventoryTable )

	if( #GAMEMODE.g_InventoryTable > 0 ) then
	
		GAMEMODE.g_InventoryTable = {};
		
	end
	
	for k,v in next, s_InventoryTable do
	
		local object = inventory( LocalPlayer(), v.w, v.h );
		object:SetID( v.id );
		object.CharID = v.CharID;
		object.ItemID = v.ItemID;
		object.name = v.Name;
		object.Disabled = tobool( v.Disabled );
	
	end

end );

netstream.Hook( "NewInventory", function( s_Width, s_Height, s_InvID, s_CharID, s_ItemID, s_szName, s_bDisabled )

	for k,v in next, GAMEMODE.g_InventoryTable do
	
		if( v.id == s_InvID ) then
		
			table.remove( GAMEMODE.g_InventoryTable, k );
			
		end
	
	end
	
	local s_Object = inventory( LocalPlayer(), s_Width, s_Height );
	s_Object:SetID( s_InvID );
	s_Object.CharID = s_CharID;
	s_Object.ItemID = s_ItemID;
	s_Object.name = s_szName;
	s_Object.Disabled = s_bDisabled;

end );

netstream.Hook( "RemoveInventory", function( nInvID )

	for k,v in next, GAMEMODE.g_InventoryTable do
	
		if( v.id == nInvID ) then
		
			for m,n in next, v:GetContents() do
			
				n:RemoveItem();
			
			end
			
			-- removing the reference should gc object
			table.remove( GAMEMODE.g_InventoryTable, k );
			
		end
		
	end

end );