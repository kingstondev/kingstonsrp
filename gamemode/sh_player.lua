AddCSLuaFile();

team.SetUp( LONER, "Loners", Color( 25, 25, 25, 255 ) );

GM.DefaultMaxWeight = 30; -- create config system

local s_Meta = FindMetaTable( "Player" );

function s_Meta:GetMaxWeight() -- move to sh_characters

	local nMax = GAMEMODE.DefaultMaxWeight;
	for k,v in next, GAMEMODE.g_ItemTable do
	
		if( v:Owner() == self ) then
		
			if( v.CalcMaxWeight ) then
			
				nMax = nMax + v:CalcMaxWeight();
				
			end
		
		end
		
	end
	
	return nMax;

end

function s_Meta:GetCharDataByID( nID ) -- move to sh_characters

	if( !nID ) then nID = self:CharID() end;
	if( !GAMEMODE.g_CharacterTable[self:SteamID()] ) then return end;

	for k, v in next, GAMEMODE.g_CharacterTable[self:SteamID()] do

		if( v.id == nID ) then

			return v;
			
		end
		
	end

end

function s_Meta:HasItem( szClass )

	-- nested hell
	local items = {};
	for k,v in next, self:GetInventories() do
	
		for m,n in next, v:GetContents() do
		
			if( n:GetClass() == szClass ) then
			
				items[#items + 1] = n;
				
			end
		
		end
	
	end
	
	if( table.Count( items ) > 0 ) then return items end
	
	return false

end

function s_Meta:CalculateBody()

	local mdl = self:GetModel();
	return string.StripExtension( mdl ).."_body.mdl";

end

-- create netvar system for players, so we can broadcast info about players easily and doesnt require new charvars

-- sh_weapon.lua

if( CLIENT ) then

	function s_Meta:SelectWeapon( szClass )

		if( !self:HasWeapon( szClass ) ) then return end;
		self.DoWeaponSwitch = self:GetWeapon( szClass );

	end

	hook.Add( "CreateMove", "PredictedWeaponSwitch", function( cmd )

		if( !IsValid( LocalPlayer().DoWeaponSwitch ) ) then return end;
		
		cmd:SelectWeapon( LocalPlayer().DoWeaponSwitch );
		
		if( LocalPlayer():GetActiveWeapon() == LocalPlayer().DoWeaponSwitch ) then
			
			LocalPlayer().DoWeaponSwitch = nil;
			
		end

	end );
	
end

local s_Meta = FindMetaTable( "Weapon" );
function s_Meta:AttachHelper( szAttachment )

	if( !self.CW20Weapon ) then return end

	local category,value;
	for k,v in next, self.Attachments do
	
		if( v.atts ) then
		
			for g,h in next, v.atts do
			
				if( h == szAttachment ) then

					category,value = k,g;
				
				end
			
			end
			
		end
		
	end

	if( category and value ) then
	
		timer.Simple( 0.1, function()

			self:attach( category, value - 1 );
			
		end )
		
	end
	
end