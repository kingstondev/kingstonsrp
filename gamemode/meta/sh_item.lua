AddCSLuaFile();

item = {};
item.__index = item;
item.IsItem = true;
function item:__tostring()

	return "item["..( self.id or 0 ).."]";

end

function item:New( owner, nItemID, metaitem, x, y, id, vars )

	if( !owner ) then return end
	if( !IsValid( owner ) ) then return end
	if( !nItemID ) then return end
	if( !metaitem ) then return end
	if( isstring( metaitem ) ) then
	
		metaitem = GAMEMODE:GetMeta( metaitem );
		
	end
	if( !metaitem ) then return end -- yeah not exactly amazing
	
	local itemdata = {};
	
	for k,v in next, metaitem do
	
		itemdata[k] = v;
		
		-- maybe instead of having defined funcs in our item, we construct funcs on the item load for all table entries.
		-- like so:
		--[[
			itemdata["Get"..k] = function()
			
				return v;
				
			end
		]]--
	
	end
	
	itemdata["owner"] = owner;
	itemdata["ItemID"] = nItemID;
	
	if( !x and !y ) then
	
		for k,v in next, GAMEMODE.g_InventoryTable do

			if( v.id == itemdata["ItemID"] ) then

				x, y = v:GetNextAvailableSlot( metaitem.W, metaitem.H );
				
			end
			
		end
		
	end
	
	itemdata["X"] = x;
	itemdata["Y"] = y;
	
	if( id ) then
	
		itemdata["id"] = id;
		
	end
	
	if( vars and istable( vars ) ) then

		itemdata["Vars"] = vars;
		
	else

		itemdata["Vars"] = metaitem.Vars or {};
		
	end

	setmetatable( itemdata, item );

	itemdata:Initialize();

	if( id ) then
	
		GAMEMODE.g_ItemTable[id] = itemdata;
		
	end
	
	return itemdata;

end

function item:GetName()

	return self.Name; -- cool thing about these funcs is you should be able to override in ur item code.

end

function item:GetDesc()

	return self.Desc;
	
end

function item:GetWeight()

	return self.Weight;

end

function item:GetWidth()

	return self.W;
	
end

function item:GetHeight()

	return self.H;
	
end

function item:GetX()

	return self.X;
	
end

function item:GetY()

	return self.Y;
	
end

function item:Owner()

	return self.owner;
	
end

function item:GetInventory()

	for k,v in next, GAMEMODE.g_InventoryTable do

		if( v.id == self:GetInvID() ) then

			return v;
			
		end
		
	end

end

function item:GetChildInventory()

	for k,v in next, GAMEMODE.g_InventoryTable do

		if( v.ItemID == self.id ) then
		
			return v;
			
		end
	
	end

end

function item:GetVars()

	if( self.Vars ) then
	
		return self.Vars;
		
	end
	
end

function item:SetVar( key, value, noSave ) -- add bool for network to item owner.

	if( !self.Vars ) then
	
		self.Vars = {}
		
	end
	
	if( self.Vars[key] == value ) then return end -- prevent useless pushes to database.
	
	self.Vars[key] = value;
	
	if( noSave ) then return end
	if( SERVER ) then
	
		self:UpdateSave();
		
	end
	
end

function item:GetVar( key, fallback )

	if( !self.Vars ) then return fallback end

	return self.Vars[key] or fallback;

end

function item:Initialize() -- maybe we can use hook.Run

	-- override when u need obj init cb

end

function item:GetInvID()

	return self.ItemID;

end

function item:GetClass()

	return self.Class;
	
end

function item:GetID()

	return self.id or 0;
	
end

function item:SetID( nID )

	self.id = nID;

end

function item:SetInvID( nID )

	table.RemoveByValue( self:GetInventory().contents, self );
	self.InvID = nID;
	self:GetInventory().contents[#self:GetInventory().contents + 1] = self;
	
end

function item:CallFunction( szKey )

	if( self.functions ) then

		if( self.functions[szKey] ) then

			if( self.functions[szKey].CanRun( self ) ) then

				if( self.FunctionHooks and self.FunctionHooks["Pre"..szKey] ) then
				
					self.FunctionHooks["Pre"..szKey]( self );
					
				end
			
				local ret = self.functions[szKey].OnUse( self );
				if( self.functions[szKey].RemoveOnUse ) then

					self:RemoveItem();
					
				end
				
				if( self.FunctionHooks and self.FunctionHooks["Post"..szKey] ) then
				
					self.FunctionHooks["Post"..szKey]( self );
					
				end
				
				return ret;
				
			end
			
		end
		
	end

end

function item:MoveItem( nX, nY, nInvID, bNoSave )
	-- this is default movement functionality, can be overriden in item base or item meta.

	local s_Inventory;
	if( nInvID and nInvID != self:GetInventory():GetID() ) then
	
		s_Inventory = self:Owner():FindInventoryByID( nInvID );
		
	else
	
		s_Inventory = self:GetInventory();
		
	end
	
	if( !s_Inventory:IsInventorySlotOccupiedItem( nX, nY, self.W, self.H ) ) then
	
		self.X = nX;
		self.Y = nY;
		
		if( self:GetVar( "Equipped", false ) ) then
		
			self:SetVar( "Equipped", false );
			
		end
		
		if( nInvID and nInvID != self:GetInventory():GetID() ) then
		
			for k,v in next, self:GetInventory():GetContents() do
			
				if( v.id == self:GetID() ) then
				
					table.remove( self:GetInventory():GetContents(), k );
					
				end
			
			end
			self:SetInvID( nInvID ); 
		
		end
		
		if( SERVER and !bNoSave ) then
		
			self:UpdateSave();
			
		end
		
		hook.Run( "ItemMoved", self, nX, nY, nInvID, bNoSave );
		
	end

end

function item:CanDrop()

	return true;

end

function item:DropItem()

	if( !self:CanDrop() ) then return end
	if( CLIENT ) then
	
		for k, v in next, self:GetInventory():GetContents() do -- sucky shit
		
			if( v == self ) then
			
				self:GetInventory().contents[k] = nil;
				
			end
		
		end
	
		self.InvID = 0;
		self.X = -1;
		self.Y = -1;
		self.owner = nil;
		
		if( self:GetChildInventory() ) then
		
			self:GetChildInventory().owner = nil;
			self:GetChildInventory().CharID = 0;
			
			local inventory = self:GetChildInventory(); -- just a ref
			for k,v in next, self:GetChildInventory():GetContents() do
			
				v.owner = nil;
				if( CLIENT ) then
				
					GAMEMODE.g_ItemTable[v:GetID()] = nil;
					setmetatable( v, nil );
					v = nil;
					
				end
				
			end
			
			if( CLIENT ) then
			
				for k,v in next, GAMEMODE.g_InventoryTable do
				
					if( v:GetID() == self:GetChildInventory():GetID() ) then
					
						GAMEMODE.g_InventoryTable[k] = nil;
						break;
						
					end
					
				end
				setmetatable( inventory, nil );
				inventory = nil; -- we dont really need to do this, as eliminating the object from the item table will gc the object
				
			end
			
		end
	
	end
	
	if( CLIENT ) then
	
		GAMEMODE.g_ItemTable[self:GetID()] = nil;
		setmetatable( self, nil );
		self = nil;
		
	end
	
	if( SERVER ) then
	
		GAMEMODE:DropItem( self );
		
	end

end

function item:RemoveItem()

	if( SERVER ) then
	
		self:DeleteItem();
		
	end

	for k,v in next, self:GetInventory():GetContents() do
	
		if( v == self ) then
		
			self:GetInventory().contents[k] = nil;
			
		end
	
	end
	
	-- remove child inventory
	
	GAMEMODE.g_ItemTable[self:GetID()] = nil;
	setmetatable( self, nil );
	self = nil;

end

function item:DoDragDrop( dropped )

	-- you can override in your item/base

end

function item:OnNewCreation()

end

function item:DynamicFunctions()

	return {};

end

if( SERVER ) then

	function item:SaveNewObject( cb )
		
		local query = SAVE_NEW_ITEM;
		query.onSuccess = function( query, data )
		
			local insertTable = {
				["id"] = query:lastInsert(),
			};
			
			table.Merge( self, insertTable );
			
			GAMEMODE.g_ItemTable[query:lastInsert()] = self;
			
			printf( COLOR_GREEN, "Item saved successfully for %s.", self:Owner():Name() );
			
			if( self.OnNewCreation ) then
			
				self:OnNewCreation();
				
			end
			
			if( cb ) then
			
				cb()
				
			end

		end

		query:setNumber( 1, self:GetInvID() );
		query:setString( 2, self:GetClass() );
		query:setNumber( 3, self:GetX() );
		query:setNumber( 4, self:GetY() );
		query:setString( 5, pon.encode( self:GetVars() or {} ) );
		query:start();
	
	end
	
	function item:UpdateSave()

		local query = UPDATE_ITEM;
		query.onSuccess = function()
		
			printf( COLOR_GREEN, "Item %d updated successfully.", self:GetID() );
		
		end

		query:setNumber( 1, self:GetInvID() );
		query:setNumber( 2, self:GetX() );
		query:setNumber( 3, self:GetY() );
		query:setString( 4, pon.encode( self:GetVars() or {} ) );
		query:setNumber( 5, self:GetID() );
		query:start();
		
	end
	
	-- use this when transferring owners!
	function item:TransmitToOwner()
		
		netstream.Start( self:Owner(), "ReceiveItem", self:GetInventory():GetID(), self:GetClass(), self:GetID(), self:GetVars(), self:GetX(), self:GetY() );
	
	end

	function item:Transmit( ply ) -- for sending some item info that is only needed for shit like bonemerge
	
		-- if ply is nil, netstream broadcasts to all clients.
		netstream.Start( ply, "ReceiveDummyItem", self:GetID(), self:GetClass(), self:GetVars(), self:Owner() );
		self.IsTransmitted = true; -- for join in progress, client asks server to supply all dummy items.
	
	end
	
	function item:DeleteItem()

		local query = DELETE_ITEM;
		query.onSuccess = function()
		
			printf( COLOR_GREEN, "Item removed successfully from database." );
		
		end
		query:setNumber( 1, self:GetID() );
		query:start();
	
	end
	
end
	
setmetatable( item, { __call = item.New } )