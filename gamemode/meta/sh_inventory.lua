AddCSLuaFile();
-- we need inventory:HasItem(szStr)
inventory = {};
inventory.__index = inventory;
inventory.IsInventory = true;

function inventory:__tostring()

	return "inventory["..( self.id or 0 ).."]";

end

-- perhaps create inventory classes like items? no name stuff, just a classname so we can do all sorts of special functionality

function inventory:New( owner, w, h, name ) -- suggestion from blacktea: instead of having it all in one function, divide for each owner type.

	if( owner.IsPlayer and owner:IsPlayer() ) then
	
		local inventoryStruct = {
			["name"] = name or "Pockets",
			["owner"] = owner,
			["w"] = w or 10,
			["h"] = h or 10,
			["CharID"] = owner:CharID(),
			["ItemID"] = 0,
			["contents"] = {},
		};
		setmetatable( inventoryStruct, inventory );

		-- instead of having it be unassoc. array, we create a stack of temporary tables, push the inv object then when the obj is created
		-- take bottom of stack and remove it when mysql retrieve has been finished, then move obj to g_InventoryTable.
		-- use sz key to create temp area of g_InventoryTable where invs that will never get loaded/saved go
		
		GAMEMODE.g_InventoryTable[#GAMEMODE.g_InventoryTable + 1] = inventoryStruct;
		
		return inventoryStruct;
		
	elseif( owner.IsItem ) then
	
		local inventoryStruct = {
			["name"] = name or "Pockets",
			["owner"] = owner:Owner(),
			["ItemObj"] = owner,
			["w"] = w or 10,
			["h"] = h or 10,
			["CharID"] = owner:Owner():CharID(),
			["ItemID"] = owner:GetID(),
			["contents"] = {},
		};
		setmetatable( inventoryStruct, inventory );

		GAMEMODE.g_InventoryTable[#GAMEMODE.g_InventoryTable + 1] = inventoryStruct;
		
		return inventoryStruct;
		
	elseif( owner.GetClass and owner:GetClass() == "prop_physics" ) then
	
		local inventoryStruct = {
			["name"] = name or "Pockets",
			["owner"] = owner,
			["w"] = w or 10,
			["h"] = h or 10,
			["contents"] = {},
		};
		setmetatable( inventoryStruct, inventory );

		GAMEMODE.g_InventoryTable[#GAMEMODE.g_InventoryTable + 1] = inventoryStruct;
		
		return inventoryStruct;
		
	end

end

function inventory:GetID()

	return self.id;
	
end

function inventory:SetID( nID ) -- only used when loading inventories from db.

	self.id = nID;

end

function inventory:GetWidth()

	return self.w;
	
end

function inventory:GetHeight()

	return self.h;

end

function inventory:GetName()

	return self.name;

end

function inventory:GetContents()

	return self.contents;
	
end

function inventory:Owner()

	return self.owner;
	
end

-- additem metamethod so we dont have to manually add item objs to contents tbl

function inventory:IsInventorySlotOccupiedItem( i, j, w, h )

	if( i + w - 1 <= self:GetWidth() and j + h - 1 <= self:GetHeight() ) then -- if the item could potentially fit here
		
		local good = true;
		
		for x = 1, w do -- for each cell of the width of the item
			
			for y = 1, h do
				
				if( self:IsInventorySlotOccupied( i + x - 1, j + y - 1 ) ) then
					
					good = false;
					
				end
				
			end
			
		end
		
		return !good;
		
	end
	
	return true;
	
end

function inventory:IsInventorySlotOccupied( x, y ) -- detect item so we can move items smoothly

	for _, v in next, self:GetContents() do
	
		local ItemMeta = GAMEMODE:GetMeta( v.Class );

		if( x >= v.X and x <= v.X + ItemMeta.W - 1 ) then

			if( y >= v.Y and y <= v.Y + ItemMeta.H - 1 ) then

				return true;
				
			end
			
		end
		
	end

	return false;
	
end

function inventory:GetNextAvailableSlot( w, h )

	for j = 1, self:GetHeight() do -- For each inventory slot (i, j)
		
		for i = 1, self:GetWidth() do
			
			if( !self:IsInventorySlotOccupiedItem( i, j, w, h ) ) then
				
				return i, j;
				
			end
			
		end
		
	end
	
	return false, false;
	
end

function inventory:HasItem( szClass )

	for k,v in next, self:GetContents() do
	
		if( v:GetClass() == szClass ) then
		
			return true
		
		end
	
	end
	
	return false

end

function inventory:Remove()

	for k,v in next, self:GetContents() do
	
		v:RemoveItem();
		
	end
	
	for k,v in next, GAMEMODE.g_InventoryTable do
	
		if( v.id == self:GetID() ) then
		
			GAMEMODE.g_InventoryTable[k] = nil;
			
		end
		
	end

end

if( SERVER ) then

	function inventory:GiveItem( szItemClass, x, y, Vars )
	
		if( !GAMEMODE:GetMeta( szItemClass ) ) then

			return;
			
		end
		
		local metaitem = GAMEMODE:GetMeta( szItemClass );
		if( !x or !y ) then
	
			local nX,nY = self:GetNextAvailableSlot( metaitem.W, metaitem.H );
			if( !nX or !nY ) then
			
				print("no space");
				return false;
				
			end
		
		end
		
		local object = item( self:Owner(), self.id, szItemClass, x, y );
		
		if( Vars ) then
			
			object.Vars = Vars;
			
		end
		
		local function cb()

			self.contents[#self.contents + 1] = object;
			netstream.Start( self:Owner(), "ReceiveItem", self.id, szItemClass, object.id, object.Vars or {}, x or 0, y or 0 );
			
		end
		object:SaveNewObject( cb );
		
		return obj;
		
	end

	-- todo, remove child inventory of item on deletion!
	
	function inventory:TransmitToOwner()
		
		netstream.Start( self:Owner(), "NewInventory", self.w, self.h, self.id, self.CharID, self.ItemID, self.name, self.Disabled );
	
	end
	
	function inventory:UpdateSave()
		
		local function onSuccess( data, query )
		
			printf( COLOR_GREEN, "Inventory updated successfully." );
		
		end
		wzsql.Query( Format( "UPDATE Inventories SET CharID = '%d', ItemID = '%d', Disabled = '%d' WHERE id = '%d'", self.CharID, self.ItemID, BoolToNumber( self.Disabled ), self.id ), onSuccess );
	
	end
	
	function inventory:Delete()
	
		local function onSuccess( data, query )
		
			printf( COLOR_GREEN, "Inventory removed successfully." );
			
		end
		wzsql.Query( Format( "DELETE FROM Inventories WHERE id = '%d'", self:GetID() ), onSuccess );
	
	end
	
end

setmetatable( inventory, { __call = inventory.New } )