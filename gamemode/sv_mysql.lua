require( "mysqloo" );
printf( COLOR_GREEN, "MySQL Loaded" );

local s_Meta = FindMetaTable( "Player" );

wzsql = wzsql or {};
wzsql.Queue = wzsql.Queue or {};

wzsql.m_szHost = "kingston.site.nfoservers.com";
wzsql.m_szUser = "kingston";
wzsql.m_szPassword = "xDFkPSvu2A";
wzsql.m_szDatabase = "kingston_custom";
wzsql.m_iPort = 3306;

-- add prepared queries for inventory, retrieve player, character, items
wzsql.PreparedQueries = {
	["UPDATE_ITEM"] = "UPDATE Items SET InvID = ?, x = ?, y = ?, Vars = ? WHERE id = ?",
	["DELETE_ITEM"] = "DELETE FROM Items WHERE id = ?",
	["SAVE_NEW_ITEM"] = "INSERT INTO Items ( InvID, ItemClass, x, y, Vars ) VALUES ( ?, ?, ?, ?, ? )",
	["UPDATE_CHARACTER"] = "UPDATE Characters SET RPName = ?, Description = ?, Overhead1 = ?, Overhead2 = ?, Head = ?, Disabled = ? WHERE id = ?",
	["SAVE_NEW_CHARACTER"] = "INSERT INTO Characters ( SteamID, RPName, Overhead1, Overhead2, Model, Head, Description, CharFlags, Faction ) VALUES ( ?, ?, ?, ?, ?, ?, ?, '', 'loner' )",
	["UPDATE_CHARACTER_BY_FIELD"] = "UPDATE Characters SET ? = ? WHERE id = ?",
	["UPDATE_PLAYER_BY_FIELD"] = "UPDATE Players SET ? = ? WHERE SteamID = ?",
	["CHECK_TABLE"] = "SELECT ? FROM ?",
};

function wzsql:Connect( callback )

	db = mysqloo.connect( self.m_szHost, self.m_szUser, self.m_szPassword, self.m_szDatabase, self.m_iPort );
	
	function db:onConnected()
	
		printf( COLOR_GREEN, "MySQL successfully connected to %s\nMySQL Server Version: %s", self:hostInfo(), self:serverInfo() );

		wzsql:InitiateTables();
		
		for k,v in next, wzsql.Queue do

			wzsql.Query( v[1], v[2], v[3], v[4] );
			
		end
		
		for k,v in next, wzsql.PreparedQueries do
		
			if( !_G[k] ) then
			
				_G[k] = self:prepare( v );
				hook.Run( "OnQueryPrepared", _G[k] );
				
			end
		
		end
		
		wzsql.Queue = { };
		wzsql.Query( "SET interactive_timeout = 28800" );
		wzsql.Query( "SET wait_timeout = 28000" );
		
		hook.Run( "OnDatabaseConnected", self );
		
		callback();
	
	end
	
	function db:onConnectionFailed()
	
		printf( COLOR_RED, "MySQL connection attempt failed." );
		
		hook.Run( "OnDatabaseConnectFailed" );
		
		if( string.find( err, "Unknown MySQL server host" ) ) then
			
			return;
			
		elseif( string.find( err, "Access denied for user" ) ) then
			
			printf( COLOR_RED, "ERROR: MySQL connection failed: Access denied for user \"%s\"", wzsql.m_szUser );
			return;
			
		else
			
			printf( COLOR_RED, "ERROR: MySQL connection failed: Unspecified error: \"%s\".", err );
			wzsql:Connect();
			
		end
	
	end

	db:connect();
	
end

function wzsql:RunQueue()

	for k,v in next, wzsql.Queue do

		wzsql.Query( v[1], v[2], v[3], v[4] );
		
	end
	
	wzsql.Queue = { };

end

function wzsql.Query( q, cb, cbe, noerr )

	local hwndQuery = db:query( q );
	if( !hwndQuery ) then
		
		table.insert( wzsql.Queue, { q, cb, cbe, noerr } );
		wzsql:RunQueue();
		return;
		
	end
	
	function hwndQuery:onSuccess( ret )
		
		hook.Run( "OnQuerySuccess", ret, hwndQuery );
		
		if( cb ) then
			
			cb( ret, hwndQuery );
			
		end
		
	end
	
	function hwndQuery:onError( err )
		
		if( db:status() == mysqloo.DATABASE_NOT_CONNECTED ) then
			
			printf( COLOR_RED, "mysqloo.DATABASE_NOT_CONNECTED" );
			table.insert( wzsql.Queue, { q, cb, cbe, noerr } );
			wzsql:RunQueue();
			return;
			
		end
		
		if( err == "MySQL server has gone away" ) then
			
			printf( COLOR_RED, "MySQL server has gone away" );
			table.insert( wzsql.Queue, { q, cb, cbe, noerr } );
			wzsql:RunQueue();
			return;
			
		end
		
		if( string.find( err, "Lost connection to MySQL server" ) ) then
			
			printf( COLOR_RED, "Lost connection to MySQL server" );
			table.insert( wzsql.Queue, { q, cb, cbe, noerr } );
			wzsql:RunQueue();
			return;
			
		end
		
		hook.Run( "OnQueryFailed", err, hwndQuery );
		
		if( cbe ) then
			
			cbe( err, hwndQuery );
			
		end
		
		if( !noerr ) then
			
			printf( COLOR_RED, "ERROR: MySQL query \"%s\" failed (\"%s\").", q, err );
			
		end
		
	end
	hwndQuery:start();
	
end

wzsql.m_rgSQLStruct = { };
wzsql.m_rgSQLStruct["Players"] = {
	{ "SteamID", "VARCHAR(30)" },
	{ "Rank", "VARCHAR(256)", "user" },
	{ "IPAddress", "VARCHAR(30)" },
	{ "PlayerFlags", "TEXT" },
	{ "PhysTrust", "INT", 1 },
	{ "PropTrust", "INT", 0 },
	{ "ToolTrust", "INT", 0 },
	{ "Whitelists", "TEXT" },
};

wzsql.m_rgSQLStruct["Characters"] = {
	{ "SteamID", "VARCHAR(30)" },
	{ "RPName", "TEXT" },
	{ "Overhead1", "TEXT" },
	{ "Overhead2", "TEXT" },
	{ "Model", "TEXT" },
	{ "Head", "TEXT" },
	{ "Description", "TEXT" },
	{ "CharFlags", "TEXT" },
	{ "Faction", "VARCHAR(256)", "loner" },
	{ "Disabled", "INT", "0" },
};

wzsql.m_rgSQLStruct["Bans"] = {
	{ "SteamID", "VARCHAR(30)" },
	{ "Length", "INT" },
	{ "Reason", "TEXT" },
	{ "Date", "INT" },
	{ "Unban", "INT", "0" },
};

wzsql.m_rgSQLStruct["Items"] = {
	{ "CharID", "INT(11)" },
	{ "ItemID", "INT(11)" },
	{ "ItemClass", "TEXT" },
	{ "x", "INT(11)" },
	{ "y", "INT(11)" },
	{ "Vars", "TEXT" },
};

function wzsql:InitTable( rgData, szTableName )

	for _, v in next, rgData do

		local function onSuccess( data )
			
			printf( COLOR_WHITE, "Column %s exists in table %s.", v[1], szTableName );
			
		end
		
		local function onError( err )
			
			printf( COLOR_WHITE, "Column %s does not exist in table %s. Creating...", v[1], szTableName );
			
			if( v[3] ) then

				local function onError( err )
				
					printf( COLOR_RED, "Failed to create column! %s", err );
					
				end
				wzsql.Query( Format( "ALTER TABLE %s ADD COLUMN %s %s NOT NULL DEFAULT '%s'", szTableName, v[1], v[2], tostring( v[3] ) ), nil, onError );
				
			else
			
				local function onError( err )
				
					printf( COLOR_RED, "Failed to create column! %s", err );
					
				end
				wzsql.Query( Format( "ALTER TABLE %s ADD COLUMN %s %s NOT NULL", szTableName, v[1], v[2] ), nil, onError );
				
			end
			
		end
		wzsql.Query( Format( "SELECT %s FROM %s", v[1], szTableName ), onSuccess, onError, true );
		
	end

end

function wzsql:InitiateTables()

	for k,v in next, wzsql.m_rgSQLStruct do
		
		local function onSuccess()
		
			wzsql:InitTable( wzsql.m_rgSQLStruct[k], k )
			
		end
		wzsql.Query( Format( "CREATE TABLE IF NOT EXISTS %s ( id INT NOT NULL auto_increment, PRIMARY KEY ( id ) );", k ), onSuccess );
		
	end

end

function s_Meta:RetrievePlayer( szSteamID )

	if( self:IsBot() ) then return end
	local function onSuccess( data )
	
		if( #data == 0 ) then -- succ
		
			printf( COLOR_WHITE, "Player record does not exist, creating..." );
		
			local function onSuccess()
			
				printf( COLOR_WHITE, "Player record successfully created." );
				self:RetrievePlayer( szSteamID );
			
			end
			wzsql.Query( Format( "INSERT INTO Players ( SteamID, Rank, IPAddress, PlayerFlags ) VALUES ( '%s', 'user', '%s', '' )", szSteamID, self:IPAddress() ), onSuccess );
			
			return;
			
		end
	
		GAMEMODE.g_PlayerTable[szSteamID] = data;
		
		netstream.Start( self, "LoadPlayer", data );
		
		self:LoadPlayerData( data[1] );
		
		printf( COLOR_WHITE, "Query finished, data added to g_PlayerTable" );
	
	end
	wzsql.Query( Format( "SELECT * FROM Players WHERE SteamID = '%s'", szSteamID ), onSuccess );

end

function s_Meta:RetrieveCharacters( szSteamID )

	if( self:IsBot() ) then return end
	local function onSuccess( data )
	
		GAMEMODE.g_CharacterTable[szSteamID] = data;
		netstream.Start( self, "LoadCharacters", data );
		
		printf( COLOR_WHITE, "Query finished, data added to g_CharacterTable." );
	
	end
	wzsql.Query( Format( "SELECT * FROM Characters WHERE SteamID = '%s'", szSteamID ), onSuccess );

end

function s_Meta:RetrieveInventory( nID ) -- this is final query before loading, probably should use a lambda function

	local function onSuccess( data )

		for k,v in next, data do
		
			local object = item( 
				self, 
				v.ItemID, 
				v.ItemClass, 
				v.x, 
				v.y, 
				v.id, 
				pon.decode( v.Vars )
			); -- need all before Initialize is called.
			
			object:GetInventory().contents[#object:GetInventory():GetContents() + 1] = object;
		
		end
		netstream.Start( self, "LoadItems", data );
		
		printf( COLOR_WHITE, "Query finished, data added to g_ItemTable." );
		
		hook.Run( "ItemPickedUp", self, nil );
	
	end
	wzsql.Query( Format( "SELECT * FROM Items WHERE CharID = '%d'", nID ), onSuccess );
	
end