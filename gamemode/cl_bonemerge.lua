local s_Meta = FindMetaTable( "Player" );

function s_Meta:CreateNewBonemerge( szModel )

	if( !IsValidModel( szModel ) ) then return end

	b = ClientsideModel( szModel, RENDERGROUP_OPAQUE );
	b:SetModel( szModel );
	b:InvalidateBoneCache();
	b:SetParent( self );
	b:AddEffects( EF_BONEMERGE );
	b:SetupBones();
	function b:Think()

		local ply = self:GetParent();
	
		if( IsValid( ply ) ) then
		
			if( !self.LastParent ) then
		
				if( !self.bLastAliveState ) then
				
					self.bLastAliveState = ply:Alive();
					
				end
				
				if( !self.nLastCharID ) then
					
					self.nLastCharID = ply:CharID();
					
				end
				
			else
			
				if( !self.bLastAliveState ) then
				
					self.bLastAliveState = self.LastParent:Alive();
					
				end
				
				if( !self.nLastCharID ) then
					
					self.nLastCharID = self.LastParent:CharID();
					
				end
			
			
			end

			if( ply:GetNoDraw() and !self.bLastDrawState ) then
			
				self:SetNoDraw( true );
				
			elseif( !ply:GetNoDraw() and self.bLastDrawState ) then
			
				self:SetNoDraw( false );
				
			end

			if( !self.LastParent ) then
			
				if( !ply:Alive() and self.bLastAliveState ) then
					
					self.LastParent = ply;
					self:SetParent( ply:GetRagdollEntity() );
					self:AddEffects( EF_BONEMERGE );
					
				end
				
				if( ply:CharID() != self.nLastCharID ) then

					self:Remove();
					
				end
				
			else
			
				if( self.LastParent:Alive() and !self.bLastAliveState ) then
				
					self:SetParent( self.LastParent );
					self:AddEffects( EF_BONEMERGE );
					self.LastParent = nil;
					
				end
				
			end
			
		else
		
			if( !IsValid( self.LastParent ) ) then

				self:Remove();
				
			else
			
				self:SetParent( self.LastParent );
				self:AddEffects( EF_BONEMERGE );
				self.LastParent = nil;
			
			end
		
		end
		
		self.bLastDrawState = self:GetNoDraw();
		if( !self.LastParent ) then
		
			if( IsValid( ply ) ) then
		
				self.bLastAliveState = ply:Alive();
				self.nLastCharID = ply:CharID();
				
			end
			
		else
			
			if( !IsValid( self.LastParent ) ) then return end
		
			self.bLastAliveState = self.LastParent:Alive();
			self.nLastCharID = self.LastParent:CharID();
		
		end

	end
	hook.Add( "Think", b, b.Think );
	
	return b;

end

GM.BonemergeItems = GM.BonemergeItems or {};

function GM:OnReceiveDummyItem( s_iID, s_DummyItem )

	local metaitem = GAMEMODE:GetMeta( s_DummyItem.szClass );
	if( s_DummyItem.Vars["Equipped"] ) then
	
		GAMEMODE.BonemergeItems[s_iID] = {

			Owner = s_DummyItem.Owner,
			szClass = s_DummyItem.szClass,
			Vars = s_DummyItem.Vars,
			
		};
	
	elseif( !s_DummyItem.Vars["Equipped"] and GAMEMODE.BonemergeItems[s_iID] ) then
	
		local metaitem = GAMEMODE:GetMeta( GAMEMODE.BonemergeItems[s_iID]["szClass"] );
		local owner = GAMEMODE.BonemergeItems[s_iID]["Owner"];
		if( owner[s_DummyItem.szClass] ) then
		
			owner[s_DummyItem.szClass]:Remove();
			owner[s_DummyItem.szClass] = nil;
			
			if( metaitem.CalculateBody ) then
			
				owner.BonemergeBody:Remove();
				owner.BonemergeBody = nil;
				
			end
			
		end
			
		GAMEMODE.BonemergeItems[s_iID] = nil;
	
	end

end

hook.Add( "CreateEquipSlots", "STALKER.HeadgearSlot", function( panel )

	panel.Headgear = vgui.Create( "CInventoryEquip", panel );
	panel.Headgear:SetSize( ScreenScale( 56 ), ScreenScale( 54 ) );
	panel.Headgear:SetPos( ( panel:GetWide() / 2.95 ), panel:GetTall() / 80 );
	panel.Headgear.AcceptingItemType = CATEGORY_BONEMERGE;
	panel.Headgear:Receiver( "Items", function( Receiver, Dropped, bIsDropped )

		if( bIsDropped ) then
		
			local ToSlot = Receiver; -- spaghetti here
			local DraggedItem = Dropped[1];
			local ItemObj = DraggedItem.Item;
			local ItemMeta = GAMEMODE:GetMeta( ItemObj.Class );
			if( ToSlot.AcceptingItemType == ItemMeta.Type ) then

				netstream.Start( "ItemMove", ItemObj:GetID(), -1, -1, ItemObj:GetInventory():GetID(), true );
				ItemObj:CallFunction( "Equip" );
				netstream.Start( "ItemCallFunction", ItemObj:GetID(), "Equip" );
				panel:GetParent():RefreshInventory();
				
			end
		
		end
	
	end );

end );

local function BonemergeThink()

	for k,v in next, player.GetAll() do
	
		if( !IsValid( v ) ) then continue end
		if( !v.CharID ) then continue end
		if( v:CharID() <= 0 ) then continue end
		
		if( v:Head() != "" ) then

			if( !IsValid( v.BonemergeHead ) ) then
			
				v.BonemergeHead = v:CreateNewBonemerge( v:Head() );
			
			end
			
		end
		
		if( v:Body() != "" ) then
		
			if( !IsValid( v.BonemergeBody ) ) then
				
				v.BonemergeBody = v:CreateNewBonemerge( v:Body() );
			
			end
		
		else
		
			if( IsValid( v.BonemergeBody ) ) then
			
				v.BonemergeBody:Remove();
				v.BonemergeBody = nil;
				
			end
			
		end
		
		for m,n in next, GAMEMODE.BonemergeItems do

			if( n.Owner == v and ( !v[n.szClass] or !IsValid( v[n.szClass] ) ) and n.Vars["Equipped"] ) then
				
				local metaitem = GAMEMODE:GetMeta( n.szClass );
				if( metaitem.CalculateBody ) then
				
					v.BonemergeBody = v:CreateNewBonemerge( v:CalculateBody() );
					
				end
				
				if( metaitem.Bonemerge ) then
			
					v[n.szClass] = v:CreateNewBonemerge( metaitem.Bonemerge );
				
					if( metaitem.Bodygroups ) then
					
						for _,bodygroup in next, metaitem.Bodygroups do
						
							-- first key in bodygroup is bodygroup index
							-- second key in bodygroup is bodygroup value
							v[n.szClass]:SetBodygroup( bodygroup[1], bodygroup[2] );
							
						end
						
					end
					
					if( metaitem.Submaterials ) then
					
						for _,submaterial in next, metaitem.Submaterials do
						
							v[n.szClass]:SetSubMaterial( submaterial[1], submaterial[2] );
						
						end
						
					end
					
				end

			end
		
		end
	
	end

end
hook.Add( "Think", "STALKER.BonemergeThink", BonemergeThink );