netstream.Hook( "ItemCallFunction", function( ply, s_nID, s_szKey )

	local s_Item = ply:FindItemByID( s_nID );

	if( s_Item ) then

		s_Item:CallFunction( s_szKey );
		
	end


end );

netstream.Hook( "ItemCallDynamicFunction", function( ply, s_nID, s_nFuncKey )

	local s_Item = ply:FindItemByID( s_nID );
	
	if( s_Item and s_Item.DynamicFunctions ) then
	
		local struct = s_Item:DynamicFunctions()[s_nFuncKey];
	
		if( struct.CanRun( s_Item ) ) then
	
			struct.OnUse( s_Item );
			
		end
		
	end
	
end );

netstream.Hook( "ItemMove", function( ply, s_nID, s_nX, s_nY, s_nInvID, s_bNoSave )

	local s_Item = ply:FindItemByID( s_nID );
	if( s_Item ) then

		if( ply:FindInventoryByID( s_nInvID ) ) then

			s_Item:MoveItem( s_nX, s_nY, s_nInvID, s_bNoSave );
			
		end
	
	end
	
end );

netstream.Hook( "ItemDragDrop", function( ply, s_nReceiverItemID, s_nDroppedItemID )

	local s_ReceiverItem = ply:FindItemByID( s_nReceiverItemID );
	local s_DroppedItem = ply:FindItemByID( s_nDroppedItemID );
	
	if( s_ReceiverItem and s_DroppedItem ) then
	
		s_ReceiverItem:DoDragDrop( s_DroppedItem );
		
	end

end );

netstream.Hook( "ItemDrop", function( ply, s_nID )

	local s_Item = ply:FindItemByID( s_nID );
	
	if( s_Item ) then

		s_Item:DropItem();
		
	end

end );

function GM:DropItem( s_Item )

	if( s_Item ) then
	
		for k, v in next, s_Item:GetInventory().contents do -- need to use associative table in future
		
			if( v == s_Item ) then
			
				s_Item:GetInventory().contents[k] = nil;
				
			end
		
		end
		hook.Run( "ItemDropped", s_Item );
		local itement = self:CreateItemEntity( s_Item, s_Item:Owner():EyePos() + s_Item:Owner():GetAimVector() * 50, Angle() );
		
	end

end

-- when a previous item object does not exist.
function GM:CreateNewItemEntity( szClass, pos, ang )

	local ent = ents.Create( "stalker_item" );
	ent:SetItemClass( szClass );
	ent:SetPos( pos );
	ent:SetAngles( ang );
	ent:Spawn();
	ent:Activate();
	
	hook.Run( "OnNewItemEntityCreated", ent );
	
	return ent;
	
end

function GM:OnAdminCreatedItem( ply, item )

	ply:Notify( Color( 0, 255, 0 ), "Item has been created." );

end

GM.OnAdminCreatedItemEntity = GM.OnAdminCreatedItem;

function GM:OnNewItemEntityCreated( entity )

end

-- when a previous item object exists, e.g. dropping an item
function GM:CreateItemEntity( ItemObj, pos, ang )

	local ent = ents.Create( "stalker_item" );
	ent:SetItemClass( ItemObj:GetClass() );
	ent:SetVarString( util.TableToJSON( ItemObj:GetVars() ) );
	ItemObj.InvID = 0;
	ItemObj.X = -1;
	ItemObj.Y = -1;
	ItemObj.owner = ent;
	if( ItemObj:GetChildInventory() ) then
	
		ItemObj:GetChildInventory().owner = ent;
		ItemObj:GetChildInventory().CharID = 0;
		ItemObj:GetChildInventory():UpdateSave();
		
		for k,v in next, ItemObj:GetChildInventory():GetContents() do
		
			v.owner = ent;
		
		end
		
	end
	ent.ItemObj = ItemObj;
	ItemObj:UpdateSave();
	ent:SetPos( pos );
	ent:SetAngles( ang );
	ent:Spawn();
	ent:Activate();
	
	return ent;

end

function GM:ItemPickedUp( ply, item )

	if( ply:GetWeight() > ply:GetMaxWeight() + 10 ) then
	
		ply:SetWalkSpeed( 1 );
		ply:SetRunSpeed( 1 );
		
	elseif( ply:GetWeight() >= ply:GetMaxWeight() ) then
	
		ply:SetRunSpeed( ply:GetWalkSpeed() );
	
	end

	if( item and item.PickupSound ) then
	
		ply:EmitSound( item.PickupSound, 75, 100, 1, CHAN_ITEM );
		
	end
	
end

function GM:ItemDropped( item )

	-- when this is called, the entity hasnt actually
	-- been created yet. 

	local ply = item:Owner();

	if( ply.LastWeight and ply.LastWeight >= ply:GetMaxWeight() ) then

		if( ply:GetWeight() <= ply:GetMaxWeight() ) then
		
			ply:SetRunSpeed( PLAYER_DEFAULTRUNSPEED );
			
		end
		
	elseif( ply.LastWeight and ply.LastWeight >= ply:GetMaxWeight() + 10 ) then
	
		if( ply:GetWeight() <= ply:GetMaxWeight() + 10 ) then
		
			ply:SetWalkSpeed( PLAYER_DEFAULTWALKSPEED );
			ply:SetRunSpeed( PLAYER_DEFAULTRUNSPEED );
			
		end
		
	end
	
	ply.LastWeight = ply:GetWeight();

end

function GM:DeleteDroppedItems()

	for k,v in next, ents.FindByClass( "stalker_item" ) do
	
		wzsql.Query( Format( "DELETE FROM Items WHERE id = %d", v:GetItemObject():GetID() ) );
	
	end

end

local function Reload( ply, s_nKey )

	local s_nItem = ply:FindItemByID( s_nKey );
	local weapon = ply:GetActiveWeapon();

	if( !s_nItem ) then return end
	if( !weapon or !weapon:IsValid() ) then return end
	
	local metaitem = GAMEMODE:GetMeta( s_nItem:GetClass() );
	for m,n in next, GAMEMODE.g_ItemTable do
	
		if( n.Base == "magazine" and table.HasValue( GAMEMODE.WeaponMagazines[weapon:GetClass()], n:GetClass() ) ) then

			if( n.IsLoaded ) then
			
				if( n:GetID() == s_nItem:GetID() ) then continue end
			
				n.IsLoaded = false;
				break;
				
			end
			
		end
		
	end
	
	s_nItem.IsLoaded = true;

	hook.Run( "WeaponReloaded", weapon );
	
	if( metaitem.OnLoaded ) then
	
		s_nItem:OnLoaded( weapon );

	end

	ply:SetAmmo( s_nItem:GetVar( "Rounds", 0 ), ply:GetActiveWeapon():GetPrimaryAmmoType() );
	ply:GetActiveWeapon():Reload();

end
netstream.Hook( "Reload", Reload );

netstream.Hook( "RetrieveDummyItems", function( ply )

	for k,v in next, GAMEMODE.g_ItemTable do
	
		if( v.IsTransmitted ) then
		
			netstream.Start( ply, "ReceiveDummyItem", v:GetID(), v:GetClass(), v:GetVars(), v:Owner() );
		
		end
	
	end

end );

netstream.Hook( "RequestItemUpgrade", function( ply, nItemID, szUpgrade )

	local ItemObj = GAMEMODE.g_ItemTable[nItemID];
	local Upgrade = GAMEMODE.Upgrades[szUpgrade];
	if( ItemObj and Upgrade ) then
	
		if( Upgrade.CanUpgrade( Upgrade, ItemObj ) ) then
		
			Upgrade.OnUpgrade( Upgrade, ItemObj );
		
		end
	
	end

end );