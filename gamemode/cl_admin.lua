netstream.Hook( "LoadPermissions", function( ranks )

	GAMEMODE.Ranks = ranks;
	
	if( GAMEMODE.AdminMenu and IsValid( GAMEMODE.AdminMenu ) ) then
	
		--hook.Run( "UpdateAdminMenu", GAMEMODE.AdminMenu );
		
	end
	
end );

netstream.Hook( "LoadCommands", function( commands )

	GAMEMODE.Commands = commands;

end );

function GM:ShowAdminMenu()

	if( !LocalPlayer():IsAdmin() ) then return end
	
	GAMEMODE.AdminMenu = vgui.Create( "CAdminMenu" );
	
end

function GM:UpdateAdminMenu( panel )

	panel:Update();

end