include( "shared.lua" );
include( "cl_includes.lua" );

UseDefaultMainMenu = true;

GM.g_CharacterTable = {};
GM.g_ItemTable = {};

function GM:Initialize()

	self.ChatboxFilter[CB_IC] = true;
	self.ChatboxFilter[CB_OOC] = true;
	self.ChatboxFilter[CB_RADIO] = true;

end

function GM:InitPostEntity()

	netstream.Start( "nRequestPlayerData" );
	netstream.Start( "nRetrieveClientData" );
	netstream.Start( "RetrievePermissions" );
	netstream.Start( "RetrieveDummyItems" );
	netstream.Start( "RetrieveCommands" );
	
end