ITEM.Name = "Money";
ITEM.Desc = "Money";
ITEM.Model = "models/props_lab/box01a.mdl";
ITEM.W = 1;
ITEM.H = 1;
ITEM.Weight = 0.5;
ITEM.Vars = {
	Money = 1,
};
ITEM.functions = {};
ITEM.functions.Split = {
	SelectionName = "split",
	OnUse = function( item )
	
	end,
	CanRun = function( item )
	
		return true
		
	end,
};
function ITEM:Paint( panel, w, h )

	surface.SetFont( "InventoryDisplay" );
	
	local tW,tH = surface.GetTextSize( self:GetVar( "Money", 0 ) );
	
	surface.SetTextColor( Color( 255, 255, 255 ) );
	surface.SetTextPos( w - tW, 0 );
	surface.DrawText( self:GetVar( "Money", 0 ) );

end
function ITEM:DoDragDrop( dropped )

	if( dropped:GetClass() == "money" ) then
		
		self:SetVar( "Money", self:GetVar( "Money", 0 ) + dropped:GetVar( "Money", 0 ) );
		dropped:RemoveItem();
	
	end

end