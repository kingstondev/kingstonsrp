UPGRADE.Name = "Long RIS";
UPGRADE.Desc = [[ yello 2 ]];
UPGRADE.Item = "test_weapon";
UPGRADE.Attachment = "bg_longris";
UPGRADE.IconX = 4;
UPGRADE.IconY = 4;
-- Third value is ConsumeOnUse.
UPGRADE.RequiredItems = {
	{ "long_ris", 1, true },
};
UPGRADE.Incompatible = {
	"test_upgrade",
};

-- base CanUpgrade
function UPGRADE:CanUpgrade( item )

	if( self.RequiredItems ) then
	
		for _,requirement in next, self.RequiredItems do
		
			local Items = item:Owner():HasItem( requirement[1] );
			if( !Items or #Items < requirement[2] ) then return false end
		
		end
	
	end
	
	if( self.Incompatible ) then
	
		local CurrentUpgrades = item:GetVar( "Upgrades", {} );
		for _,upgrade in next, self.Incompatible do
		
			local bHasUpgrade = table.HasValue( CurrentUpgrades, upgrade );
			if( bHasUpgrade ) then return false end
		
		end
	
	end
	
	if( self.RequiredUpgrades ) then
	
		local CurrentUpgrades = item:GetVar( "Upgrades", {} );
		for _,upgrade in next, self.RequiredUpgrades do

			local bHasUpgrade = table.HasValue( CurrentUpgrades, upgrade );
			if( !bHasUpgrade ) then return false end
		
		end
	
	end
	
	return true;

end

-- This is a shared function.
function UPGRADE:OnUpgrade( item )

	local upgrades = item:GetVar( "Upgrades", {} );
	if( !table.HasValue( upgrades, self.ClassName ) ) then

		upgrades[#upgrades + 1] = self.ClassName;
		item:SetVar( "Upgrades", upgrades );
		
		if( self.RequiredItems ) then
		
			for _,requirement in next, self.RequiredItems do
				
				if( !requirement[3] ) then continue end
				
				local Items = item:Owner():HasItem( requirement[1] );
				for _,item in next, Items do
				
					item:RemoveItem();
				
				end
				
				if( CLIENT ) then
				
					if( GAMEMODE.Inventory ) then
					
						GAMEMODE.Inventory:RefreshInventory();
						
					end
					
				end
			
			end
			
		end
		
	end
	
	if( SERVER ) then
	
		if( item:GetVar( "Equipped", false ) ) then
		
			local weapon = item:Owner():GetWeapon( item.WeaponClass );
			local szAttachment = self.Attachment;
			weapon:AttachHelper( szAttachment );
		
		end
		
	end

end