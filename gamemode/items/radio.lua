ITEM.Name = "Radio"
ITEM.Desc = "Radio dude!"
ITEM.Model = "models/props_c17/SuitCase_Passenger_Physics.mdl"
ITEM.W = 1
ITEM.H = 1
ITEM.Weight = 1
ITEM.Vars = {
	Power = false,
	Frequency = 100.1,
}
ITEM.functions = {};
ITEM.functions.PowerOn = {
	SelectionName = "power on",
	OnUse = function( item )
	
		item:SetVar( "Power", true );
		
		if( !item:Owner().ActiveRadios ) then item:Owner().ActiveRadios = {} end
		
		-- item id as key, reference to item obj as value.
		item:Owner().ActiveRadios[item:GetID()] = item;
	
	end,
	CanRun = function( item )
	
		return !item:GetVar( "Power", false );
	
	end,
};
ITEM.functions.PowerOff = {
	SelectionName = "power off",
	OnUse = function( item )
	
		item:SetVar( "Power", false );
		item:Owner().ActiveRadios[item:GetID()] = nil;
	
	end,
	CanRun = function( item )
	
		return item:GetVar( "Power", false );
	
	end,
};
ITEM.functions.ChangeFrequency = {
	SelectionName = "change frequency",
	OnUse = function( item )
	
		if( CLIENT ) then
		
			Derma_StringRequest(
				"Change Frequency",
				"Input the kHz and decimal range, e.g 100.1",
				tostring( item:GetVar( "Frequency", 100.1 ) ),
				function( text ) 
					-- disallow non-number characters, and guarantee number is within a range of 100 ~ 500.
					-- outright replace with better freq selector.
					netstream.Start( "nChangeRadioFrequency", item:GetID(), text );
					item:SetVar( "Frequency", tonumber( text ) );
					
				end,
				function() end
			);
		
		end
	
	end,
	CanRun = function( item )
	
		return true
	
	end,
};

function ITEM:Initialize()

	if( self:GetVar( "Power", false ) ) then
	
		self.functions.PowerOn.OnUse( self );
		
	end

end

netstream.Hook( "nChangeRadioFrequency", function( ply, item, frequency )

	local ItemObj = GAMEMODE.g_ItemTable[item];
	if( ItemObj and ItemObj:Owner() == ply ) then
	
		ItemObj:SetVar( "Frequency", tonumber( frequency ) );
	
	end

end );

-- in case we ever want to store configuration.
netstream.Hook( "nSetPrimaryRadio", function( ply, item, bPrimary )

	local ItemObj = GAMEMODE.g_ItemTable[item];
	if( ItemObj and ItemObj:Owner() == ply ) then
	
		ItemObj:SetVar( "PrimaryRadio", bPrimary );
	
	end

end );