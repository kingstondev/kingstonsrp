BASE.Vars = {
	Equipped = false,
}
BASE.Type = CATEGORY_DETECTOR;
BASE.DetectorType = DETECTOR_ECHO;
BASE.functions = {};
BASE.functions.Equip = {
	SelectionName = "equip",
	OnUse = function( item )
	
		local metaitem = GAMEMODE:GetMeta( item:GetClass() );

		item.X = -1;
		item.Y = -1;
		item:SetVar( "Equipped", true );
		
		if( CLIENT ) then
		
			local detector = vgui.Create("CDetector")
			detector:SetSize(400, 600)
			detector:SetPos(ScrW() - 350, ScrH() - 600)
			detector:SetType(metaitem.DetectorType)
			GAMEMODE.Detector = detector
			
		end
		
		return true
		
	end,
	CanRun = function( item )
	
		return !item:GetVar( "Equipped", false );
	
	end,
}
BASE.functions.Unequip = {
	SelectionName = "unequip",
	OnUse = function( item )

		local metaitem = GAMEMODE:GetMeta( item:GetClass() );
	
		if( CLIENT ) then
	
			if( GAMEMODE.Detector and IsValid( GAMEMODE.Detector ) ) then
			
				GAMEMODE.Detector:Remove();
				GAMEMODE.Detector = nil;
				
			end
			
		end

		if( item.X == -1 and item.Y == -1 ) then
		
			local x,y = item:GetInventory():GetNextAvailableSlot( metaitem.W, metaitem.H );
			item.X = x;
			item.Y = y;
		
		end
	
		item:SetVar( "Equipped", false );
		
		return true
		
	end,
	CanRun = function( item )
	
		return item:GetVar( "Equipped", false );
		
	end,
}
function BASE:MoveItem( nX, nY, nInvID, bNoSave )

	local s_Inventory;
	if( nInvID and nInvID != self:GetInventory():GetID() ) then
	
		s_Inventory = self:Owner():FindInventoryByID( nInvID );
		
	else
	
		s_Inventory = self:GetInventory();
		
	end
	
	if( !s_Inventory:IsInventorySlotOccupiedItem( nX, nY, self.W, self.H ) ) then
		
		self.X = nX;
		self.Y = nY;
		
		if( nInvID and nInvID != self:GetInventory():GetID() ) then
		
			for k,v in next, self:GetInventory():GetContents() do
			
				if( v.id == self:GetID() ) then
				
					table.remove( self:GetInventory():GetContents(), k );
					
				end
			
			end
			self:SetInvID( nInvID ); 
		
		end
		
		if( self:GetVar( "Equipped", false ) ) then

			self:CallFunction( "Unequip" );
			bNoSave = true;
			
		end
		
		if( SERVER and !bNoSave ) then
		
			self:UpdateSave();
			
		end
		
		hook.Run( "ItemMoved", self, nX, nY, nInvID, bNoSave );
		
	end

end
function BASE:Initialize()

	if( self:GetVar( "Equipped", false ) ) then
	
		self.functions.Equip.OnUse( self );
		
	end

end
function BASE:CanDrop()

	return !self:GetVar( "Equipped", false );

end