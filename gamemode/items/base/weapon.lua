BASE.Vars = {
	Equipped = false,
	CurrentAttachments = {},
	Upgrades = {},
	Durability = 100,
};
BASE.Type = CATEGORY_PRIMARYWEAPON;
BASE.DurabilityRate = 1;
BASE.StartDurability = 100;
BASE.UseDurability = false;
BASE.functions = {};
BASE.functions.Equip = {
	SelectionName = "equip",
	OnUse = function( item )

		local MetaItem = GAMEMODE:GetMeta( item:GetClass() );
		local weapon;
		
		if( item:GetVar( "Durability", 0 ) < 1 ) then
		
			if( SERVER ) then
			
				item:Owner():Notify( COLOR_WHITE, "This weapon is broken." );
				
			end
			
			return false;
			
		end
	
		item.X = -1;
		item.Y = -1;
		
		item:SetVar( "Equipped", true );
		
		if( SERVER ) then

			weapon = item:Owner():Give( item.WeaponClass );
			if( IsValid( weapon ) ) then
			
				item:Owner():SelectWeapon( weapon:GetClass() );
				
			end
			
			local attachments = item:GetVar( "CurrentAttachments", {} );
			for k,v in next, attachments do
			
				weapon:AttachHelper( k );
			
			end
			
			local upgrades = item:GetVar( "Upgrades", {} );
			for k,v in next, upgrades do
			
				GAMEMODE.Upgrades[v].OnUpgrade( GAMEMODE.Upgrades[v], item );
			
			end
		
		end
		
		if( CLIENT ) then
			
			-- sometimes the weapon is given too soon, and when client runs this, it hasnt received
			-- the packet telling the client that he has this weapon. big problem.
			weapon = item:Owner():GetWeapon( item.WeaponClass ); -- server shouldve already given by this point
			
		end

		if( !item:Owner().EquippedWeapons ) then
		
			item:Owner().EquippedWeapons = {};
			
		end
		item:Owner().EquippedWeapons[MetaItem.WeaponType] = item.WeaponClass;
		
		return true
		
	end,
	CanRun = function( item )

		if( item:Owner().EquippedWeapons and item:Owner().EquippedWeapons[item.WeaponType] ) then

			return false
			
		end

		return !item:GetVar( "Equipped", false );
		
	end,
}
BASE.functions.Unequip = {
	SelectionName = "unequip",
	OnUse = function( item )
	
		local MetaItem = GAMEMODE:GetMeta( item:GetClass() );
		local weapon = item:Owner():GetWeapon( item.WeaponClass );
		
		if( item:GetX() == -1 and item:GetY() == -1 ) then
		
			local x,y;
			for k,v in next, item:Owner():GetInventories() do
			
				x,y = v:GetNextAvailableSlot( MetaItem.W, MetaItem.H );
				if( x != -1 or y != -1 ) then
					break;
				end
			
			end
			if( x == -1 or y == -1 ) then return false end
			
			item:MoveItem( x, y );
			return;
		
		end
		
		if( IsValid( weapon ) ) then
		
			if( SERVER ) then
		
				item:Owner():StripWeapon( item.WeaponClass );
				
			end
		
		end
		
		item:SetVar( "Equipped", false );
		item:Owner().EquippedWeapons[MetaItem.WeaponType] = nil; -- we dont wanna shift the indexes because our key is a const
		
		return true
		
	end,
	CanRun = function( item )
	
		return item:GetVar( "Equipped", false );
		
	end,
}

function BASE:MoveItem( nX, nY, nInvID, bNoSave )

	if( self:GetChildInventory() ) then
	
		if( nInvID == self:GetChildInventory():GetID() ) then
		
			return false;
		
		end
		
	end

	local s_Inventory;
	if( nInvID and nInvID != self:GetInventory():GetID() ) then
	
		s_Inventory = self:Owner():FindInventoryByID( nInvID );
		
	else
	
		s_Inventory = self:GetInventory();
		
	end
	
	if( !s_Inventory:IsInventorySlotOccupiedItem( nX, nY, self.W, self.H ) ) then
		
		self.X = nX;
		self.Y = nY;
		
		if( nInvID and nInvID != self:GetInventory():GetID() ) then
		
			for k,v in next, self:GetInventory():GetContents() do
			
				if( v.id == self:GetID() ) then
				
					table.remove( self:GetInventory():GetContents(), k );
					
				end
			
			end
			self:SetInvID( nInvID ); 
		
		end

		if( self:GetVar( "Equipped", false ) ) then

			self:CallFunction( "Unequip" );
			bNoSave = true;
			
		end
		
		if( SERVER and !bNoSave ) then
		
			self:UpdateSave();
			
		end
		
		hook.Run( "ItemMoved", self, nX, nY, nInvID, bNoSave );
		
	else
	
		return false;
		
	end

	return true;
	
end

function BASE:Initialize()

	if( self:GetVar( "Equipped", false ) ) then
	
		self.functions.Equip.OnUse( self );
		
		for k,v in next, self:GetVar( "Upgrades", {} ) do
		
			GAMEMODE.Upgrades[v].OnUpgrade( GAMEMODE.Upgrades[v], self );
		
		end

	end

end

function BASE:OnNewCreation()

	local meta = GAMEMODE:GetMeta( self:GetClass() );

	self:SetVar( "Durability", meta.StartDurability );

end
-- this function returns a table with some various options
function BASE:DynamicFunctions()

	local struct = {};
	for k,v in next, self:GetVar( "CurrentAttachments", {} ) do
	
		local att, index = CustomizableWeaponry:findAttachment( k );
		local setup = {
			SelectionName = Format( "detach %s", att.displayName ),
			OnUse = function( item )
			
				local MetaItem = GAMEMODE:GetMeta( item:GetClass() );
				local weapon = item:Owner():GetWeapon( MetaItem.WeaponClass );
				
				if( SERVER ) then
				
					if( weapon and IsValid( weapon ) ) then
					
						local category,value;
						for g,h in next, weapon.Attachments do
						
							if( h.atts ) then
							
								for m,n in next, h.atts do
								
									if( n == k ) then

										category,value = g,m;
									
									end
								
								end
								
							end
							
						end
						
						if( category and value ) then

							weapon:detach( category );
							
						end
						
					end
					
				end
				
				local CurrentAttachments = item:GetVar( "CurrentAttachments", {} );
				CurrentAttachments[k] = nil;
				item:SetVar( "CurrentAttachments", CurrentAttachments );
				
				if( SERVER ) then
				
					for m,n in next, GAMEMODE.MetaItems do
					
						if( n.Base == "attachment" ) then
						
							if( n.AttachmentName == k ) then
								
								item:Owner():GetInventory():GiveItem( m );
								break;
								
							end
							
						end
						
					end
					
				end
				
				return true;
			
			end,
			CanRun = function( item )
			
				return true;
				
			end,
		};
		
		struct[#struct + 1] = setup;
	
	end
	
	return struct;

end
function BASE:WeaponHasFired() -- move to OnLoaded

	if( !self:Owner() or !IsValid( self:Owner() ) ) then return end

	local weapon = self:Owner():GetActiveWeapon();
	local meta = GAMEMODE:GetMeta( self:GetClass() );
	if( !self:GetVar( "Equipped", false ) ) then return end
	
	weapon:SetNW2Int( "TimesFired", weapon:GetNW2Int( "TimesFired", 0 ) + 1 );
	
	if( !meta.UseDurability ) then return end
	
	if( meta.WeaponClass == weapon:GetClass() ) then
	
		if( self:GetVar( "Durability", 0 ) > 0 ) then
		
			self:SetVar( "Durability", self:GetVar( "Durability", 0 ) - ( 1 * meta.DurabilityRate ) );
			
		else
		
			for k,v in next, self:Owner():GetInventories() do
			
				for x = 1, v:GetWidth() do
				
					for y = 1, v:GetHeight() do
		
						if( self:MoveItem( x, y, v:GetID() ) ) then -- find empty slot
						
							break;
						
						end
						
					end
					
				end
				
			end
		
		end
		
	end

end
function BASE:CanDrop()

	return !self:GetVar( "Equipped", false );

end
function BASE:CanUpgrade()

	return !self:GetVar( "Equipped", false );
	
end