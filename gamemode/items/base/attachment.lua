BASE.functions = {};
-- add ability to check if weapon has upgrades
BASE.functions.Attach = {
	SelectionName = "attach",
	RemoveOnUse = true,
	OnUse = function( item )

		local MetaItem = GAMEMODE:GetMeta( item:GetClass() );
		local att, index = CustomizableWeaponry:findAttachment( MetaItem.AttachmentName );
		local weapon = item:Owner():GetActiveWeapon();
		local canAttach = weapon:canAttachSpecificAttachment( MetaItem.AttachmentName, item:Owner(), nil, nil, nil, nil );
		local category,value;
		
		for k,v in next, weapon.Attachments do
		
			if( v.atts ) then
			
				for m,n in next, v.atts do
				
					if( n == MetaItem.AttachmentName ) then

						category,value = k,m;
					
					end
				
				end
				
			end
			
		end

		if( canAttach and category and value ) then
		
			if( SERVER ) then

				weapon:attach( category, value - 1 ); -- for some reason, the index starts at 0 when using attach.
				
			end
			
			for k,v in next, GAMEMODE.g_ItemTable do
			
				local meta = GAMEMODE:GetMeta( v:GetClass() );
				if( meta.WeaponClass == weapon:GetClass() and v:GetVar( "Equipped", false ) ) then -- make sure we confirm owner is the player using the item
				
					local tbl = v:GetVar( "CurrentAttachments", {} );
					tbl[MetaItem.AttachmentName] = true;
					v:SetVar( "CurrentAttachments", tbl );
				
				end
				
			end
			
		end
		
	end,
	CanRun = function( item )

		local weapon = item:Owner():GetActiveWeapon();
		for k,v in next, GAMEMODE.g_ItemTable do
		
			local meta = GAMEMODE:GetMeta( v:GetClass() );
			if( v:Owner() == item:Owner() and meta.WeaponClass == weapon:GetClass() and v:GetVar( "Equipped", false ) ) then

				return true;
			
			end
			
		end
	
	end,
}