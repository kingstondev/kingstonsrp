-- define keys you want the item to have that use this base.
-- already defined keys from the item itself will be used instead of the base keys.
BASE.Vars = {
	Equipped = false,
}
-- create system for networking vars to other players with as little info about the item as possible
-- id, item class, var determined by function.
BASE.Type = CATEGORY_CLOTHING;
BASE.functions = {};
BASE.functions.Equip = {
	SelectionName = "equip",
	OnUse = function( item )
	
		local metaitem = GAMEMODE:GetMeta( item:GetClass() );

		item.X = -1;
		item.Y = -1;
		item:SetVar( "Equipped", true );
		
		if( metaitem.Bonemerge ) then
		
			if( SERVER ) then
		
				item:Transmit();
				
			end
		
		else
		
			item:Owner():SetModel( item.ClothingModel );
			
			if( metaitem.CalculateBody ) then
			
				if( SERVER ) then
				
					item:Transmit();
					
				end
				
			end
			
		end
		
		return true
		
	end,
	CanRun = function( item )
	
		return !item:GetVar( "Equipped", false );
	
	end,
}
BASE.functions.Unequip = {
	SelectionName = "unequip",
	OnUse = function( item )

		local metaitem = GAMEMODE:GetMeta( item:GetClass() );
	
		item:SetVar( "Equipped", false );
		if( item.X == -1 and item.Y == -1 ) then
		
			local x,y = item:GetInventory():GetNextAvailableSlot( metaitem.W, metaitem.H );
			item.X = x;
			item.Y = y;
		
		end
		
		if( metaitem.Bonemerge ) then
		
			if( SERVER ) then
		
				item:Transmit();
				
			end
			
		else
		
			item:Owner():SetModel( item:Owner():GetCharDataByID( item:Owner():CharID() ).Model );
			
			if( metaitem.CalculateBody ) then
			
				if( SERVER ) then
				
					item:Transmit();
					
				end
				
			end
			
		end
		
		return true
		
	end,
	CanRun = function( item )
	
		return item:GetVar( "Equipped", false );
		
	end,
}
function BASE:MoveItem( nX, nY, nInvID, bNoSave )

	local s_Inventory;
	if( nInvID and nInvID != self:GetInventory():GetID() ) then
	
		s_Inventory = self:Owner():FindInventoryByID( nInvID );
		
	else
	
		s_Inventory = self:GetInventory();
		
	end
	
	if( !s_Inventory:IsInventorySlotOccupiedItem( nX, nY, self.W, self.H ) ) then
		
		self.X = nX;
		self.Y = nY;
		
		if( nInvID and nInvID != self:GetInventory():GetID() ) then
		
			for k,v in next, self:GetInventory():GetContents() do
			
				if( v.id == self:GetID() ) then
				
					table.remove( self:GetInventory():GetContents(), k );
					
				end
			
			end
			self:SetInvID( nInvID ); 
		
		end
		
		if( self:GetVar( "Equipped", false ) ) then

			self:CallFunction( "Unequip" );
			bNoSave = true;
			
		end
		
		if( SERVER and !bNoSave ) then
		
			self:UpdateSave();
			
		end
		
		hook.Run( "ItemMoved", self, nX, nY, nInvID, bNoSave );
		
	end

end
function BASE:Initialize()

	if( self:GetVar( "Equipped", false ) ) then
	
		self.functions.Equip.OnUse( self );
		
	end

end
function BASE:CanDrop()

	return !self:GetVar( "Equipped", false );

end