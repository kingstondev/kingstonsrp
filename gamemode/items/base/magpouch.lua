BASE.Vars = {
	Equipped = false,
};
BASE.Type = CATEGORY_MAGPOUCH;
BASE.functions = {};
BASE.functions.Equip = {
	SelectionName = "equip",
	OnUse = function( item )

		item.X = -1;
		item.Y = -1;
		item:GetChildInventory().Disabled = false;
		item:SetVar( "Equipped", true );
		
		return true
		
	end,
	CanRun = function( item )
	
		return !item:GetVar( "Equipped", false );
		
	end,
}
BASE.functions.Unequip = {
	SelectionName = "unequip",
	OnUse = function( item )
	
		item:GetChildInventory().Disabled = true;
		item:SetVar( "Equipped", false );
		
		for k,v in next, item:GetChildInventory():GetContents() do
		
			if( v.Base == "magazine" and v.IsLoaded ) then
			
				v:CallFunction( "Unload" );
				
			end
			
		end
		
		local metaitem = GAMEMODE:GetMeta( item:GetClass() );
		if( item.X == -1 and item.Y == -1 ) then
		
			local x,y = item:GetInventory():GetNextAvailableSlot( metaitem.W, metaitem.H );
			item.X = x;
			item.Y = y;
		
		end
		
		return true
		
	end,
	CanRun = function( item )
	
		return item:GetVar( "Equipped", false );
		
	end,
}
function BASE:MoveItem( nX, nY, nInvID, bNoSave )

	if( self:GetChildInventory() ) then
	
		if( nInvID == self:GetChildInventory():GetID() ) then
		
			return false;
		
		end
		
	end

	local s_Inventory;
	if( nInvID and nInvID != self:GetInventory():GetID() ) then
	
		s_Inventory = self:Owner():FindInventoryByID( nInvID );
		
	else
	
		s_Inventory = self:GetInventory();
		
	end
	
	if( !s_Inventory:IsInventorySlotOccupiedItem( nX, nY, self.W, self.H ) ) then
		
		self.X = nX;
		self.Y = nY;

		if( nInvID and nInvID != self:GetInventory():GetID() ) then
			
			for k,v in next, self:GetInventory():GetContents() do
			
				if( v.id == self:GetID() ) then
				
					table.remove( self:GetInventory():GetContents(), k );
					
				end
			
			end
			self:SetInvID( nInvID ); 
		
		end

		if( self:GetVar( "Equipped", false ) ) then

			self:CallFunction( "Unequip" );
			bNoSave = true;
			
		end
		
		if( SERVER and !bNoSave ) then

			self:UpdateSave();
			
		end
		
		hook.Run( "ItemMoved", self, nX, nY, nInvID, bNoSave );
		
	end

end
function BASE:Initialize()

	if( self:GetVar( "Equipped", false ) ) then
		
		self.functions.Equip.OnUse( self );
		
	else
	
		if( self:GetChildInventory() ) then
	
			self:GetChildInventory().Disabled = true;
			
		end
	
	end

end
function BASE:OnNewCreation() -- not very awesome. need to adjust more.

	local object = inventory( self:Owner(), self.SizeX, self.SizeY, self.InventoryName );
	object.Disabled = true;
	object.ItemID = self:GetID();
	object:SaveNewObject( function()
	
		object:TransmitToOwner();
		
	end );

end
function BASE:CanDrop()

	return !self:GetVar( "Equipped", false );
	
end