BASE.Vars = {
	Ammo = 30,
};
BASE.functions = {};
BASE.functions.Load = {
	SelectionName = "load",
	OnUse = function( item )
	
		for k,v in next, item:Owner():GetInventories() do

			for m,n in next, v:GetContents() do

				if( n.Base == "magazine" ) then
				
					if( !n:GetVar( "IsLoaded", false ) ) then

						if( n.AmmoType == item.AmmoType ) then
						
							local delta = n.Capacity - n:GetVar( "Rounds", 0 );
							delta = math.min( n.Capacity - n:GetVar( "Rounds", 0 ), item:GetVar( "Ammo", 0 ) );
							
							if( delta > 0 ) then

								n:SetVar( "Rounds", n:GetVar( "Rounds", 0 ) + delta );

								if( item:GetVar( "Ammo", 0 ) - delta <= 0 ) then
									
									item:RemoveItem();
									
								else
								
									item:SetVar( "Ammo", item:GetVar( "Ammo", 0 ) - delta );
									
								end
							
								break;
							
							end
							
						end
						
					end
				
				end
			
			end
		
		end
		
	end,
	CanRun = function( item )
	
		return true
		
	end,
}