BASE.Vars = {
	Rounds = 0,
}
BASE.functions = {};
BASE.functions.Unload = {
	SelectionName = "unload from weapon",
	OnUse = function( item )

		for k,v in next, item:Owner():GetWeapons() do
		
			if( !GAMEMODE.WeaponMagazines[v:GetClass()] ) then continue end
			if( table.HasValue( GAMEMODE.WeaponMagazines[v:GetClass()], item:GetClass() ) ) then
			
				v:SetClip1( 0 );
				
			end
			
		end
		
		item.IsLoaded = false;
	
	end,
	CanRun = function( item )
	
		return item.IsLoaded;
		
	end,
}
function BASE:MoveItem( nX, nY, nInvID, bNoSave )

	if( self.IsLoaded and nInvID != self:GetInventory():GetID() ) then
	
		return;
		
	end
	
	local s_Inventory;
	if( nInvID and nInvID != self:GetInventory():GetID() ) then

		s_Inventory = self:Owner():FindInventoryByID( nInvID );
		
	else
	
		s_Inventory = self:GetInventory();
		
	end
	
	if( !s_Inventory:IsInventorySlotOccupiedItem( nX, nY, self.W, self.H ) ) then
	
		self.X = nX;
		self.Y = nY;
		
		if( nInvID and nInvID != self:GetInventory():GetID() ) then
			
			for k,v in next, self:GetInventory():GetContents() do
			
				if( v.id == self:GetID() ) then
				
					table.remove( self:GetInventory():GetContents(), k );
					
				end
			
			end
			self:SetInvID( nInvID ); 
		
		end
		
		if( SERVER and !bNoSave ) then
		
			self:UpdateSave();
			
		end
		
		hook.Run( "ItemMoved", self, nX, nY, nInvID, bNoSave );
		
	end

end
function BASE:DoDragDrop( dropped )

	if( !self.IsLoaded ) then

		if( dropped.AmmoType == self.AmmoType ) then
		
			local delta = self.Capacity - self:GetVar( "Rounds", 0 );
			delta = math.min( self.Capacity - self:GetVar( "Rounds", 0 ), dropped:GetVar( "Ammo", 0 ) );
			
			if( delta > 0 ) then

				self:SetVar( "Rounds", self:GetVar( "Rounds", 0 ) + delta );

				if( dropped:GetVar( "Ammo", 0 ) - delta <= 0 ) then
					
					dropped:RemoveItem();
					
				else
				
					dropped:SetVar( "Ammo", dropped:GetVar( "Ammo", 0 ) - delta );
					
				end
			
			end
			
		end
		
	end

end
function BASE:OnLoaded( weapon )

	if( self.MagazineAttachment ) then
	
		local MetaItem = GAMEMODE:GetMeta( self:GetClass() );
		local att, index = CustomizableWeaponry:findAttachment( MetaItem.MagazineAttachment );
		local canAttach = weapon:canAttachSpecificAttachment( MetaItem.MagazineAttachment, self:Owner(), nil, nil, nil, nil );
		local category,value;
		
		for k,v in next, weapon.Attachments do
		
			if( v.atts ) then
			
				for m,n in next, v.atts do
				
					if( n == MetaItem.MagazineAttachment ) then

						category,value = k,m;
					
					end
				
				end
				
			end
			
		end

		if( canAttach and category and value ) then

			if( value == weapon.Attachments[category]["last"] ) then return end;
			if( SERVER ) then

				weapon:attach( category, value - 1 ); -- for some reason, the index starts at 0 when using attach.
				
			end
			
		end
		
	end
	
	if( !self.MagazineAttachment and self.DefaultToBaseAttachment ) then
	
		if( weapon and IsValid( weapon ) ) then

			if( weapon.detach ) then
		
				weapon:detach( 5 );
				
			end
			
		end
			
	end

	weapon:SetNW2Int( "LastMagazineID", self:GetID() );
	
	return 1;

end
function BASE:WeaponHasFired()

	if( !self:Owner() or !IsValid( self:Owner() ) ) then return end

	local wep = self:Owner():GetActiveWeapon();
	
	if( !wep or !IsValid( wep ) ) then return end
	if( !self.IsLoaded ) then return end -- we have to assume that only one magazine is going to be loaded.
	if( table.HasValue( GAMEMODE.WeaponMagazines[wep:GetClass()], self:GetClass() ) ) then
	
		if( self:GetVar( "Rounds", 0 ) > 0 ) then

			--self:SetVar( "Rounds", self:GetVar( "Rounds", 0 ) - 1 );
			
		end
	
	end

end
function BASE:Paint( panel, w, h )

	if( self.IsLoaded ) then
	
		surface.SetDrawColor( Color( 0, 255, 0, 80 ) );
		surface.DrawRect( 0, 0, w, h );
		
	end
	
	if( panel.ShowAmmo ) then
		
		surface.SetTextColor( Color( 255, 255, 255, 255 ) );
		surface.SetFont( "Arial-16" );
		surface.SetTextPos( 4, 4 );
		surface.DrawText( self:GetVar( "Rounds", 0 ).."/"..self.Capacity );
		
	end

end