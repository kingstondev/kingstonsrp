ITEM.Base = "magazine";
ITEM.Name = "30rnd STANAG";
ITEM.Desc = "STANAG Magazine with a capacity of 30 rounds.";
ITEM.Model = "models/Items/BoxMRounds.mdl";
ITEM.AmmoType = "5.56x45MM";
ITEM.W = 1;
ITEM.H = 2;
ITEM.Weight = 1.5;
ITEM.Capacity = 30;
ITEM.DefaultToBaseAttachment = true;