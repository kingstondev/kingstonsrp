GM.ChatboxOpen = GM.ChatboxOpen or false;
GM.ChatboxFilter = GM.ChatboxFilter or {};

GM.ChatWidth = 600;
GM.ChatHeight = 300;

GM.ChatLines = GM.ChatLines or { };

function GM:AddChat( filter, font, ... )

	local text = "<font=ChatNormal>"
	local unedited = "";

	if (font) then
		text = "<font="..(font or "ChatNormal")..">"
	end
	
	for k, v in ipairs({...}) do
		if (type(v) == "IMaterial") then
			local ttx = tostring(v):match("%[[a-z0-9/]+%]")
			ttx = ttx:sub(2, ttx:len() - 1)
			text = text.."<img="..ttx..","..v:Width().."x"..v:Height()..">"
		elseif (type(v) == "table" and v.r and v.g and v.b) then
			text = text.."<color="..v.r..","..v.g..","..v.b..">"
		elseif (type(v) == "Player") then
			local color = team.GetColor(v:Team())

			text = text..v:RPName():gsub("<", "&lt;"):gsub(">", "&gt;")
			unedited = unedited..v:RPName();
		else
			if( !v ) then continue end
			text = text..tostring(v):gsub("<", "&lt;"):gsub(">", "&gt;")
			unedited = unedited..tostring(v):gsub("<", "&lt;"):gsub(">", "&gt;");
			text = text:gsub("%b//", function(value)
				local inner = value:sub(2, -2)

				if (inner:find("%S")) then
					return "<font="..font.."Italic>"..value:sub(2, -2).."</font>"
				end
			end)
			text = text:gsub("%b**", function(value)
				local inner = value:sub(2, -2)
				
				if(inner:find("%S")) then
					return "<font="..font.."Bold>"..value:sub(2, -2).."</font>"
				end
			end)
		end
	end

	text = text.."</font>"
	table.insert( self.ChatLines, { CurTime(), filter, font, text } );
	MsgC( unedited:gsub("&lt;", "<"):gsub("&gt;", ">") .. "\n" );
	
	if( #self.ChatLines > 100 ) then
		
		for i = 1, #self.ChatLines - 100 do
			
			table.remove( self.ChatLines, 1 );
			
		end
		
	end
	
	if( !system.HasFocus() and self.ChatboxFilter[filter] ) then
		
		system.FlashWindow();
		
	end
	
	return #self.ChatLines;
	
end

function GM:HUDPaintChat()
	
	if( !self.ChatboxOpen ) then
		
		local y = ScrH() - 200 - 40 - 34;
		
		local tab = { };
		for _, v in next, self.ChatLines do

			if( self.ChatboxFilter[v[2]] and CurTime() - v[1] < 10 ) then
				
				table.insert( tab, v );
				
			end
			
		end
		
		local ty = 0;
		
		for i = #tab, math.max( #tab - 8, 1 ), -1 do

			local start = tab[i][1];
			local filter = tab[i][2];
			local font = tab[i][3];
			local text = tab[i][4];
			
			local amul = 0;
			
			if( CurTime() - start < 9 ) then
				
				amul = 1;
				
			else
				
				amul = 1 - ( CurTime() - start - 9 );
				
			end
			
			if( !tab[i].MarkupObjCreated ) then
			
				tab[i].MarkupObjCreated = true;
				tab[i].obj = king.markup.parse( text, GAMEMODE.ChatWidth - 20 - 16 );
				tab[i].obj.onDrawText = function( text, font, x, y, colour, halign, valign, alphaoverride, blk )
				
					if (valign == TEXT_ALIGN_CENTER) then	
					
						y = y - (tab[i].obj.totalHeight / 2)
						
					elseif (valign == TEXT_ALIGN_BOTTOM) then	
					
						y = y - (tab[i].obj.totalHeight)
						
					end
					
					local alpha = blk.colour.a
					if (alphaoverride) then alpha = alphaoverride end

					surface.SetFont( font )
					surface.SetTextColor( 0, 0, 0, alpha );
					surface.SetTextPos( x + 1, y + 1 );
					surface.DrawText( text );
					surface.SetTextColor( colour.r, colour.g, colour.b, alpha )
					surface.SetTextPos( x, y )
					surface.DrawText( text )
				
				end;
				
			end
			if( !tab[i].obj ) then return end
			tab[i].obj:draw( 30, y - tab[i].obj:getHeight(), nil, nil, 255 * amul );

			y = y - tab[i].obj:getHeight();
			ty = ty + tab[i].obj:getHeight();
			
		end
		
	end
	
end

function GM:CreateChatbox()
	
	self.ChatboxOpen = true;
	
	self.Chat = vgui.Create( "CChatPanel" );
	self.Chat:SetSize( self.ChatWidth, self.ChatHeight );
	self.Chat:SetPos( 20, ScrH() - 200 - 34 - self.ChatHeight );
	function self.Chat:Think()
	
		if( input.IsKeyDown( KEY_ESCAPE ) ) then
		
			GAMEMODE.ChatboxOpen = false;
			
			netstream.Start( "SetTyping", 0 );
			
			self:Remove();

			if( self.RadioSelection ) then
			
				self.RadioSelection:Remove();
				
			end
			
			GAMEMODE.Chat = nil;
		
		end
		
	end
	self.Chat:MakePopup();
	
	self.Chat.Entry = vgui.Create( "DTextEntry", self.Chat );
	self.Chat.Entry:SetFont( "ChatNormal" );
	self.Chat.Entry:SetPos( 10, self.ChatHeight - 30 );
	self.Chat.Entry:SetSize( self.ChatWidth - 20, 20 );
	self.Chat.Entry:PerformLayout();
	function self.Chat.Entry:OnEnter()
		
		if( string.len( self:GetValue() ) > 0 ) then
			
			if( string.len( self:GetValue() ) > 500 ) then
				
				GAMEMODE:AddChat( CB_OOC, "ChatNormal", Color( 200, 0, 0, 255 ), "The maximum chat length is 500 characters. You typed " .. string.len( self:GetValue() ) .. "." );
				GAMEMODE.NextChatText = self:GetValue();
				
			else
				
				netstream.Start( "Say", self:GetValue() );
				GAMEMODE:OnChat( LocalPlayer(), self:GetValue() );
				
			end
			
		else
			
			GAMEMODE.NextChatText = nil;
			
		end
		
		GAMEMODE.ChatboxOpen = false;
		
		netstream.Start( "SetTyping", 0 );
		
		self:GetParent():Remove();
		if( self:GetParent().RadioSelection ) then
			
			self:GetParent().RadioSelection:Remove();
			
		end
		
		GAMEMODE.Chat = nil;
		
	end
	function self.Chat.Entry:OnTextChanged()
		
		local cc = GAMEMODE:GetChatCommand( self:GetValue() );
		
		if( string.len( self:GetValue() ) > 0 ) then
			
			if( cc ) then
				
				netstream.Start( "SetTyping", 2 );
				
			else
				
				netstream.Start( "SetTyping", 1 );
				
			end
			
		else
			
			netstream.Start( "SetTyping", 0 );
			
		end
		
		if( cc ) then
			
			self.OverrideColor = cc[2];
			
		else
			
			self.OverrideColor = nil;
			
		end
		
	end
	if( self.NextChatText ) then
		
		self.Chat.Entry:SetValue( self.NextChatText );
		self.NextChatText = nil;
		
	end
	self.Chat.Entry:RequestFocus();
	self.Chat.Entry:SetCaretPos( string.len( self.Chat.Entry:GetValue() ) );
	
	self.Chat.IC = vgui.Create( "DCheckBoxLabel", self.Chat );
	self.Chat.IC:SetPos( 10, 10 );
	self.Chat.IC:SetFont( "ChatNormal" );
	self.Chat.IC:SetText( "IC" );
	self.Chat.IC:SetValue( self.ChatboxFilter[CB_IC] );
	self.Chat.IC:SetTextColor( Color( 0, 0, 0, 255 ) );
	self.Chat.IC:SizeToContents();
	function self.Chat.IC:OnChange( bValue )
	
		GAMEMODE.ChatboxFilter[CB_IC] = bValue;
	
	end
	
	self.Chat.OOC = vgui.Create( "DCheckBoxLabel", self.Chat );
	self.Chat.OOC:SetPos( 70, 10 );
	self.Chat.OOC:SetFont( "ChatNormal" );
	self.Chat.OOC:SetText( "OOC" );
	self.Chat.OOC:SetValue( self.ChatboxFilter[CB_OOC] );
	self.Chat.OOC:SetTextColor( Color( 0, 0, 0, 255 ) );
	self.Chat.OOC:SizeToContents();
	function self.Chat.OOC:OnChange( bValue )
	
		GAMEMODE.ChatboxFilter[CB_OOC] = bValue;
	
	end
	
	self.Chat.Radio = vgui.Create( "DCheckBoxLabel", self.Chat );
	self.Chat.Radio:SetPos( 130, 10 );
	self.Chat.Radio:SetFont( "ChatNormal" );
	self.Chat.Radio:SetText( "Radio" );
	self.Chat.Radio:SetValue( self.ChatboxFilter[CB_RADIO] );
	self.Chat.Radio:SetTextColor( Color( 0, 0, 0, 255 ) );
	self.Chat.Radio:SizeToContents();
	function self.Chat.Radio:OnChange( bValue )
	
		GAMEMODE.ChatboxFilter[CB_RADIO] = bValue;
	
	end
	
	/*self.Chat.CloseButton = vgui.Create( "DButton", self.Chat );
	self.Chat.CloseButton:SetFont( "marlett" );
	self.Chat.CloseButton:SetText( "r" );
	self.Chat.CloseButton:SetPos( self.ChatWidth - 20 - 10, 10 );
	self.Chat.CloseButton:SetSize( 20, 20 );
	function self.Chat.CloseButton:DoClick()
		
		GAMEMODE.ChatboxOpen = false;
		
		netstream.Start( "SetTyping", 0 );
		
		self:GetParent():Remove();
		if( self:GetParent().RadioSelection ) then
			
			self:GetParent().RadioSelection:Remove();
			
		end
		
		GAMEMODE.Chat = nil;
		
	end*/
	
end

function GM:StartChat()
	
	return true;
	
end

function GM:ChatText( i, name, text, type )
	
	if( type == "joinleave" and string.find( text, "has joined the game" ) ) then
		
		--self:AddChat( { CB_OOC }, "ChatNormal", COLOR_NOTIFY, text );
		
	end
	
	return true;
	
end

local function Say( ply, arg )
	
	GAMEMODE:OnChat( ply, arg );
	
end
netstream.Hook( "Say", Say );