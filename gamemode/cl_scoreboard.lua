function GM:ScoreboardHide()
	
	if( self.Scoreboard ) then
		
		self.Scoreboard:Remove();
		
	end
	
	self.Scoreboard = nil;
	
end

function GM:ScoreboardShow()
	
	self.Scoreboard = vgui.Create( "DFrame" );
	self.Scoreboard:SetSize( 500, 600 );
	self.Scoreboard:SetTitle( "" );
	self.Scoreboard:ShowCloseButton( false );
	self.Scoreboard:Center();
	self.Scoreboard:MakePopup();
	function self.Scoreboard:Paint( w, h )
	
		surface.SetDrawColor( 25, 25, 25 );
		surface.DrawRect( 0, 0, w, h );
		
		surface.SetFont( "ScoreboardFont" );
		
		local tW, tH = surface.GetTextSize( GetHostName() );
		surface.SetTextPos( ( w/2 ) - ( tW/2 ), 4 );
		surface.SetTextColor( 255, 255, 255 );
		surface.DrawText( GetHostName() );
		
		surface.SetDrawColor( 255, 255, 255 );
		surface.DrawOutlinedRect( 0, 0, w, h );
	
	end
	
	self.Scoreboard.Scroll = vgui.Create( "DScrollPanel", self.Scoreboard );
	self.Scoreboard.Scroll:SetPos( 0, 24 );
	self.Scoreboard.Scroll:SetSize( 500, 600 - 24 );
	self.Scoreboard.Scroll.Paint = function() end
	
	self.Scoreboard.Board = vgui.Create( "IScoreboard", self.Scoreboard.Scroll );
	self.Scoreboard.Board:SetSize( 500, 8000 );
	self.Scoreboard.Board:SetPos( 0, 0 );
	
end