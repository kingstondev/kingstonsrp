AddCSLuaFile();
GM.CharacterVars = GM.CharacterVars or {};

local s_Meta = FindMetaTable( "Player" );
-- created to make sh_accessors obsolete 
function GM:RegisterCharacterVar( szKey, szField, Default, fnOnSet, fnOnGet, bPrivate, bServer ) -- default can be any kind of value

	local szProperName = szKey:sub( 1, 1 ):upper()..szKey:sub( 2 );
	self.CharacterVars[szKey] = {
		["field"] = szField,
		["default"] = Default,
		["private"] = bPrivate,
		["server"] = bServer,
		["OnSet"] = fnOnSet,
		["OnGet"] = fnOnGet
	};
	
	if( !fnOnGet ) then
	
		s_Meta["Get"..szProperName] = function( self )
				
			if( self["_accessor_"..szProperName] != nil ) then
			
				return self["_accessor_"..szProperName];
				
			else
			
				return Default;
				
			end
			
		end
		
	else
	
		s_Meta["Get"..szProperName] = fnOnGet;
		
	end
	
	if( !fnOnSet ) then

		s_Meta["Set" .. szProperName] = function( self, Value )
			
			local OldValue = self["_accessor_"..szProperName];
			self["_accessor_"..szProperName] = Value;
			hook.Run( "OnAccessorChanged", self, szProperName, OldValue, Value );
			
			if( SERVER and !bServer ) then
				
				if( bPrivate ) then
					
					if( self:IsBot() ) then return end
					
					netstream.Start( self, "nSetAccessorP" .. szProperName, Value );
					
				else
					
					netstream.Start( nil, "nSetAccessor" .. szProperName, self, Value );
					
				end
				
			end
			
		end
		
	else
	
		s_Meta["Set"..szProperName] = fnOnSet;
		
	end
		
	if( CLIENT ) then
		
		local function nSetAccessorP( s_Value )
			
			LocalPlayer()["Set" .. szProperName]( LocalPlayer(), s_Value );
			
		end
		netstream.Hook( "nSetAccessorP" .. szProperName, nSetAccessorP );
		
		local function nSetAccessor( ply, s_Value )

			if( ply and ply:IsValid() ) then
				
				ply["Set" .. szProperName]( ply, s_Value );
				
			end
			
		end
		netstream.Hook( "nSetAccessor" .. szProperName, nSetAccessor );
		
	end

end

GM.GlobalGearSelection = {
	{ "test_weapon", 1000 },
}