AddCSLuaFile();
DeriveGamemode( "sandbox" );
include( "sh_includes.lua" );

GM.Name = "Kingston SRP";

function GM:PlayerGiveSWEP( ply, weapon, info )
	
	return false;
	
end

--[[ ScreenScale = function( size )
	-- imagine ultrawide monitors, default screenscale uses ScrW
	-- you can still use SScale for ScrW
	return size * ( ScrH() / 640.0 );
end ]]

GM.LonerHeads = {
	"models/minic23/citizens/male_01_head.mdl",
	"models/minic23/citizens/male_02_head.mdl",
	"models/minic23/citizens/male_03_head.mdl",
	"models/minic23/citizens/male_04_head.mdl",
	"models/minic23/citizens/male_05_head.mdl",
	"models/minic23/citizens/male_06_head.mdl",
	"models/minic23/citizens/male_07_head.mdl",
	"models/minic23/citizens/male_08_head.mdl",
	"models/minic23/citizens/male_09_head.mdl",
	"models/minic23/citizens/male_10_head.mdl",
	"models/minic23/citizens/male_11_head.mdl",
};

GM.Heads = {
	[GENDER_MALE] = {
		"models/kingston/fame/male_01.mdl",
		"models/kingston/fame/male_02.mdl",
		"models/kingston/fame/male_03.mdl",
		"models/kingston/fame/male_04.mdl",
		"models/kingston/fame/male_05.mdl",
		"models/kingston/fame/male_06.mdl",
		"models/kingston/fame/male_07.mdl",
		"models/kingston/fame/male_08.mdl",
		"models/kingston/fame/male_09.mdl",
	},
	[GENDER_FEMALE] = {
		"models/kingston/fame/female_01.mdl",
		"models/kingston/fame/female_02.mdl",
		"models/kingston/fame/female_03.mdl",
		"models/kingston/fame/female_04.mdl",
		"models/kingston/fame/female_05.mdl",
		"models/kingston/fame/female_06.mdl",
	},
	[ALIEN] = {
	
	},
};

GM.WeaponMagazines = {
	["cw_ar15"] = { "quadstack_stanag", "30rnd_stanag" },
	["cw_m1911"] = {},
};

GM.ShowCrosshair = {
	["gmod_tool"] = true,
	["weapon_physgun"] = true,
};

-- move to sh_weapons.lua

local s_Meta = FindMetaTable( "Weapon" );
function s_Meta:PostPrimaryAttack()

	if( IsFirstTimePredicted() ) then

		hook.Run( "WeaponHasFired", self );
		
	end

end

function GM:WeaponHasFired( weapon ) -- here we can do all sorts of cool stuff.

	for k,v in next, GAMEMODE.g_ItemTable do
	
		if( v.WeaponHasFired ) then
		
			v:WeaponHasFired( weapon )
			
		end
		
	end

end

local s_Meta = FindMetaTable( "Player" );

-- lame IsAdmin detours

function s_Meta:IsAdmin()

	local rank = GAMEMODE.Ranks[self:GetUserGroup()];
	if( rank ) then

		if( rank.IsAdmin or rank.IsSuperAdmin ) then
		
			return true;
			
		end
		
	end

end

function s_Meta:IsSuperAdmin()

	local rank = GAMEMODE.Ranks[self:GetUserGroup()];
	if( rank ) then
		
		if( rank.IsSuperAdmin ) then
		
			return true;
			
		end
		
	end

end

function IsValidModel( szModelPath )

	return file.Exists( szModelPath, "GAME" );

end

function s_Meta:IsFemale()

	local model = self:Head():lower();
	return model:find("female");

end

function s_Meta:GetBodyModel()

	if( self:IsFemale() ) then
	
		return "";
		
	else
	
		return "";
	
	end

end