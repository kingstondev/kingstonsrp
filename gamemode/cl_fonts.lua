surface.CreateFont( "AmazStalker128", {
	font = "AmazS.T.A.L.K.E.R.v.3.0",
	size = 128,
	weight = 500,
	antialias = true,
} );

surface.CreateFont( "STALKER.LabelMenu", {
	font = "Arial",
	size = 16,
	weight = 900 
} );

surface.CreateFont( "Stalker2-24", { -- change name to match font size
	font = "Myriad Pro",
	size = ScreenScale( 12 ),
	weight = 500,
	antialias = true,
} );

surface.CreateFont( "ScoreboardFont", {
	font = "Myriad Pro",
	size = 48,
	weight = 500,
	antialias = true,
} );

surface.CreateFont( "Stalker2-16", {
	font = "Myriad Pro",
	size = ScreenScale( 6 ),
	weight = 500,
	antialias = true,
} );

surface.CreateFont( "Stalker2-MainMenuFont", {
	font = "STALKER2",
	size = ScreenScale( 8 ),
	weight = 750,
	antialias = true,
} );

surface.CreateFont( "Stalker2-MainMenuFont-32", {
	font = "STALKER2",
	size = ScreenScale( 16 ),
	weight = 750,
	antialias = true,
} );

surface.CreateFont( "Stalker2-ButtonText", {
	font = "STALKER2",
	size = ScreenScale( 8 ),
	weight = 750,
	antialias = true,
} );

surface.CreateFont( "Arial-16", {
	font = "Arial",
	size = ScreenScale( 6 ),
	weight = 575,
	antialias = true,
} );

surface.CreateFont( "Rundschrift-16-B", {
	font = "DIN Rundschrift",
	size = ScreenScale( 6 ),
	weight = 570,
	antialias = true,
} );

surface.CreateFont( "Rundschrift-16", {
	font = "DIN Rundschrift",
	size = ScreenScale( 6 ),
	weight = 560,
	antialias = true,
} );

surface.CreateFont( "Rundschrift-32", {
	font = "DIN Rundschrift",
	size = ScreenScale( 12 ),
	weight = 560,
	antialias = true,
} );

surface.CreateFont( "Rundschrift-16-I", {
	font = "DIN Rundschrift",
	size = ScreenScale( 7 ),
	weight = 530,
	antialias = true,
	italic = true,
} );

surface.CreateFont( "ChatSmall", {
	font = "Myriad Pro",
	size = 14,
	weight = 100 } );
	
surface.CreateFont( "ChatSmallItalic", {
	font = "Myriad Pro",
	size = 14,
	weight = 500,
	italic = true } );

surface.CreateFont( "ChatNormal", {
	font = "Roboto",
	size = 18,
	weight = 500,
	antialias = true,
} );
	
surface.CreateFont( "ChatNormalBold", {
	font = "Roboto",
	size = 18,
	weight = 600,
	antialias = true,
} );
	
surface.CreateFont( "ChatNormalItalic", {
	font = "Roboto",
	size = 18,
	weight = 500,
	italic = true } );
	
surface.CreateFont( "ChatRadio", {
	font = "Lucida Console",
	size = 12,
	weight = 500 } );
	
surface.CreateFont( "ChatBig", {
	font = "Myriad Pro",
	size = 21,
	weight = 700 } );
	
surface.CreateFont( "ChatBigItalic", {
	font = "Myriad Pro",
	size = 21,
	weight = 700,
	italic = true } );
	
surface.CreateFont( "ChatHuge", {
	font = "Myriad Pro",
	size = 20,
	weight = 700 } );
	
surface.CreateFont( "InventoryWeight", {
	font = "Arial",
	size = ScreenScale( 8 ),
	weight = 560,
	antialias = true,
} );

surface.CreateFont( "InventoryDisplay", {
	font = "Arial",
	size = ScreenScale( 6 ),
	weight = 500,
	antialias = true,
} );

surface.CreateFont( "InventoryNameDisplay", {
	font = "Arial",
	size = ScreenScale( 8 ),
	weight = 560,
	antialias = true,
} );

surface.CreateFont( "InventoryFactionDisplay", {
	font = "Arial",
	size = ScreenScale( 6 ),
	weight = 560,
	antialias = true,
} );