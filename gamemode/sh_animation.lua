AddCSLuaFile();
-- NutScript code

kingston = {};

HOLDTYPE_TRANSLATOR = {}
HOLDTYPE_TRANSLATOR[""] = "normal"
HOLDTYPE_TRANSLATOR["physgun"] = "smg"
HOLDTYPE_TRANSLATOR["ar2"] = "smg"
HOLDTYPE_TRANSLATOR["crossbow"] = "shotgun"
HOLDTYPE_TRANSLATOR["rpg"] = "shotgun"
HOLDTYPE_TRANSLATOR["slam"] = "normal"
HOLDTYPE_TRANSLATOR["grenade"] = "grenade"
HOLDTYPE_TRANSLATOR["fist"] = "normal"
HOLDTYPE_TRANSLATOR["melee2"] = "melee"
HOLDTYPE_TRANSLATOR["passive"] = "normal"
HOLDTYPE_TRANSLATOR["knife"] = "melee"
HOLDTYPE_TRANSLATOR["duel"] = "pistol"
HOLDTYPE_TRANSLATOR["camera"] = "smg"
HOLDTYPE_TRANSLATOR["magic"] = "normal"
HOLDTYPE_TRANSLATOR["revolver"] = "pistol"

PLAYER_HOLDTYPE_TRANSLATOR = {}
PLAYER_HOLDTYPE_TRANSLATOR[""] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["fist"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["pistol"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["grenade"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["melee"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["slam"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["melee2"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["passive"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["knife"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["duel"] = "normal"
PLAYER_HOLDTYPE_TRANSLATOR["bugbait"] = "normal"

local IsValid = IsValid
local string = string
local type = type
local vectorAngle = FindMetaTable("Vector").Angle
local normalizeAngle = math.NormalizeAngle

local PLAYER_HOLDTYPE_TRANSLATOR = PLAYER_HOLDTYPE_TRANSLATOR
local HOLDTYPE_TRANSLATOR = HOLDTYPE_TRANSLATOR

kingston.anim = kingston.anim or {}
kingston.anim.player = {
	normal = {
		[ACT_MP_STAND_IDLE] = ACT_HL2MP_IDLE,
		[ACT_MP_CROUCH_IDLE] = ACT_HL2MP_IDLE_CROUCH,
		[ACT_MP_WALK] = ACT_HL2MP_WALK,
		[ACT_MP_RUN] = ACT_HL2MP_RUN_FAST
	},
	passive = {
		[ACT_MP_STAND_IDLE] = ACT_HL2MP_IDLE_PASSIVE,
		[ACT_MP_WALK] = ACT_HL2MP_WALK_PASSIVE,
		[ACT_MP_CROUCHWALK] = ACT_HL2MP_WALK_CROUCH_PASSIVE,
		[ACT_MP_RUN] = ACT_HL2MP_RUN_PASSIVE
	},
}

local translations = {}

function kingston.anim.setModelClass(model, class)
	if (!kingston.anim[class]) then
		error("'"..tostring(class).."' is not a valid animation class!")
	end
	
	translations[model:lower()] = class
end

-- Micro-optimization since the get class function gets called a lot.
local stringLower = string.lower
local stringFind = string.find

function kingston.anim.getModelClass(model)
	model = stringLower(model)
	local class = translations[model]

	if (!class and stringFind(model, "/cultist")) then
		return "player"
	end

	class = class or "player"

	if (class == "citizen_male" and (stringFind(model, "female") or stringFind(model, "alyx") or stringFind(model, "mossman"))) then
		class = "citizen_female"
	end
	
	local retClass = hook.Run( "ChangeModelClass", model, class );
	
	if( retClass ) then
	
		class = retClass;
		
	end
	
	return class
end

local getModelClass = kingston.anim.getModelClass

do
	local playerMeta = FindMetaTable("Player")

	function playerMeta:forceSequence(sequence, callback, time, noFreeze)
		hook.Run("OnPlayerEnterSequence", self, sequence, callback, time, noFreeze)

		if (!sequence) then
			return self:leaveSequence();
		end

		local sequence = self:LookupSequence(sequence)

		if (sequence and sequence > 0) then
			time = time or self:SequenceDuration(sequence)

			self.kingstonSeqCallback = callback
			self.kingstonForceSeq = sequence

			if (!noFreeze) then
				self:SetMoveType(MOVETYPE_NONE)
			end

			if (time > 0) then
				timer.Create("kingstonSeq"..self:EntIndex(), time, 1, function()
					if (IsValid(self)) then
						self:leaveSequence()
					end
				end)
			end
			
			netstream.Start( nil, "seqSet", self, sequence );

			return time
		end

		return false
	end

	function playerMeta:leaveSequence()
		hook.Run("OnPlayerLeaveSequence", self)
		
		netstream.Start( nil, self, 0 );

		self:SetMoveType(MOVETYPE_WALK)
		self.kingstonForceSeq = nil

		if (self.kingstonSeqCallback) then
			self:kingstonSeqCallback()
		end
	end

	if (CLIENT) then
	
		netstream.Hook( "seqSet", function( ply, entity, sequence )
		
			if (IsValid(entity)) then
				if (!sequence or sequence == 0) then
					entity.kingstonForceSeq = nil

					return
				end

				entity:SetCycle(0)
				entity:SetPlaybackRate(1)
				entity.kingstonForceSeq = sequence
			end
		
		end );
		
	end
end

if( SERVER ) then

	function GM:PlayerSwitchWeapon(client, oldWeapon, newWeapon)
		client:SetRaised(false)
	end
	
end

function GM:TranslateActivity(client, act)
	local model = string.lower(client.GetModel(client))
	local class = getModelClass(model) or "player"
	local weapon = client.GetActiveWeapon(client)
	if (class == "player") then
		if (IsValid(weapon) and !client.isWepRaised(client) and client.OnGround(client)) then
			if (string.find(model, "zombie")) then
				local tree = kingston.anim.zombie

				if (string.find(model, "fast")) then
					tree = kingston.anim.fastZombie
				end

				if (tree[act]) then
					return tree[act]
				end
			end

			local holdType = IsValid(weapon) and (weapon.HoldType or weapon.GetHoldType(weapon)) or "normal"

			if (IsValid(weapon) and !client.isWepRaised(client) and client:OnGround()) then
				holdType = PLAYER_HOLDTYPE_TRANSLATOR[holdType] or "passive"
			end

			local tree = kingston.anim.player[holdType]
			
			if (tree and tree[act]) then
				if (type(tree[act]) == "string") then
					client.CalcSeqOverride = client.LookupSequence(client, tree[act])

					return
				else
					return tree[act]
				end
			end
		end

		return self.BaseClass.TranslateActivity(self.BaseClass, client, act)
	end

	local tree = kingston.anim[class]

	if (tree) then
		local subClass = "normal"

		if (client.InVehicle(client)) then
			local vehicle = client.GetVehicle(client)
			local class = vehicle:isChair() and "chair" or vehicle:GetClass()

			if (tree.vehicle and tree.vehicle[class]) then
				local act = tree.vehicle[class][1]
				local fixvec = tree.vehicle[class][2]
				--local fixang = tree.vehicle[class][3]

				if (fixvec) then
					client:SetLocalPos(Vector(16.5438, -0.1642, -20.5493))
				end

				if (type(act) == "string") then
					client.CalcSeqOverride = client.LookupSequence(client, act)

					return
				else
					return act
				end
			else
				act = tree.normal[ACT_MP_CROUCH_IDLE][1]

				if (type(act) == "string") then
					client.CalcSeqOverride = client:LookupSequence(act)
				end

				return
			end
		elseif (client.OnGround(client)) then
			client.ManipulateBonePosition(client, 0, vector_origin)

			if (IsValid(weapon)) then
				subClass = weapon.HoldType or weapon.GetHoldType(weapon)
				subClass = HOLDTYPE_TRANSLATOR[subClass] or subClass
			end

			if (tree[subClass] and tree[subClass][act]) then
				local act2 = tree[subClass][act][client.isWepRaised(client) and 2 or 1]

				if (type(act2) == "string") then
					client.CalcSeqOverride = client.LookupSequence(client, act2)

					return
				end

				return act2
			end
		elseif (tree.glide) then
			return tree.glide
		end
	end
end

function GM:DoAnimationEvent(client, event, data)
	local model = client:GetModel():lower()
	local class = kingston.anim.getModelClass(model)
	
	if( !class ) then return end
	if (class == "player") then
		return self.BaseClass:DoAnimationEvent(client, event, data)
	else
		local weapon = client:GetActiveWeapon()

		if (IsValid(weapon)) then
			local holdType = weapon.HoldType or weapon:GetHoldType()
			holdType = HOLDTYPE_TRANSLATOR[holdType] or holdType
			
			local animation = kingston.anim[class][holdType]

			if (event == PLAYERANIMEVENT_ATTACK_PRIMARY) then
				client:AnimRestartGesture(GESTURE_SLOT_ATTACK_AND_RELOAD, animation.attack or ACT_GESTURE_RANGE_ATTACK_SMG1, true)

				return ACT_VM_PRIMARYATTACK
			elseif (event == PLAYERANIMEVENT_ATTACK_SECONDARY) then
				client:AnimRestartGesture(GESTURE_SLOT_ATTACK_AND_RELOAD, animation.attack or ACT_GESTURE_RANGE_ATTACK_SMG1, true)

				return ACT_VM_SECONDARYATTACK
			elseif (event == PLAYERANIMEVENT_RELOAD) then
				client:AnimRestartGesture(GESTURE_SLOT_ATTACK_AND_RELOAD, animation.reload or ACT_GESTURE_RELOAD_SMG1, true)

				return ACT_INVALID
			elseif (event == PLAYERANIMEVENT_JUMP) then
				client.m_bJumping = true
				client.m_bFistJumpFrame = true
				client.m_flJumpStartTime = CurTime()

				client:AnimRestartMainSequence()

				return ACT_INVALID
			elseif (event == PLAYERANIMEVENT_CANCEL_RELOAD) then
				client:AnimResetGestureSlot(GESTURE_SLOT_ATTACK_AND_RELOAD)

				return ACT_INVALID
			end
		end
	end

	return ACT_INVALID
end

function GM:CalcMainActivity(client, velocity)
	local eyeAngles = client.EyeAngles(client)
	local yaw = vectorAngle(velocity)[2]
	local normalized = normalizeAngle(yaw - eyeAngles[2])

	client.SetPoseParameter(client, "move_yaw", normalized)
	
	local oldSeqOverride = client.CalcSeqOverride
	local seqIdeal, seqOverride = self.BaseClass.CalcMainActivity(self.BaseClass, client, velocity)
	--client.CalcSeqOverride is being -1 after this line.

	return seqIdeal, client.kingstonForceSeq or oldSeqOverride or client.CalcSeqOverride
end

local KEY_BLACKLIST = IN_ATTACK + IN_ATTACK2

function GM:StartCommand(client, command)
	local weapon = client:GetActiveWeapon()

	if (!client:isWepRaised()) then
		if( IsValid( weapon ) and weapon.SecondaryFireWhenLowered ) then
		
			command:RemoveKey( IN_ATTACK );
			return;
			
		end
		/*
		if (IsValid(weapon) and weapon.FireWhenLowered) then
			return
		end
		*/

		command:RemoveKey(KEY_BLACKLIST)
	end
end

ALWAYS_RAISED = {};
ALWAYS_RAISED["weapon_physgun"] = true;
ALWAYS_RAISED["gmod_tool"] = true;

local playerMeta = FindMetaTable( "Player" );

-- Returns whether or not the player has their weapon raised.
function playerMeta:isWepRaised()
	local weapon = self.GetActiveWeapon(self)
	local override = hook.Run("ShouldWeaponBeRaised", self, weapon)

	-- Allow the hook to check first.
	if (override != nil) then
		return override
	end

	-- Some weapons may have their own properties.
	if (IsValid(weapon)) then
		-- If their weapon is always raised, return true.
		if (weapon.IsAlwaysRaised or ALWAYS_RAISED[weapon.GetClass(weapon)]) then
			return true
		-- Return false if always lowered.
		elseif (weapon.IsAlwaysLowered or weapon.NeverRaised) then
			return false
		end
	end

	return self:Raised() or false;
end

if( CLIENT ) then

	local LOWERED_ANGLES = Angle(30, -30, -25)

	function GM:CalcViewModelView(weapon, viewModel, oldEyePos, oldEyeAngles, eyePos, eyeAngles)
		if (!IsValid(weapon)) then
			return
		end

		local client = LocalPlayer()
		local value = 0

		if (!client:isWepRaised()) then
			value = 100
		end

		local fraction = (client.kingstonRaisedFrac or 0) / 100
		local rotation = weapon.LowerAngles or LOWERED_ANGLES
		
		eyeAngles:RotateAroundAxis(eyeAngles:Up(), rotation.p * fraction)
		eyeAngles:RotateAroundAxis(eyeAngles:Forward(), rotation.y * fraction)
		eyeAngles:RotateAroundAxis(eyeAngles:Right(), rotation.r * fraction)

		client.kingstonRaisedFrac = Lerp(FrameTime() * 2, client.kingstonRaisedFrac or 0, value)

		viewModel:SetAngles(eyeAngles)

		if (weapon.GetViewModelPosition) then
			local position, angles = weapon:GetViewModelPosition(eyePos, eyeAngles)

			oldEyePos = position or oldEyePos
			eyeAngles = angles or eyeAngles
		end
		
		if (weapon.CalcViewModelView) then
			local position, angles = weapon:CalcViewModelView(viewModel, oldEyePos, oldEyeAngles, eyePos, eyeAngles)

			oldEyePos = position or oldEyePos
			eyeAngles = angles or eyeAngles
		end

		return oldEyePos, eyeAngles
	end

end