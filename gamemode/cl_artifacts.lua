function GM:CreateEquipSlots( panel )

	for i = 0, 4 do

		panel["ArtifactSlot"..i] = vgui.Create( "CInventoryEquip", panel );
		panel["ArtifactSlot"..i]:SetSize( ScreenScale( 30 ), ScreenScale( 28 ) );
		panel["ArtifactSlot"..i]:SetPos( ( panel:GetWide() / 8.2 ) - ( ScreenScale( 30 ) / 2 ) + ( ( ScreenScale( 30 ) + 7 ) * i ), panel:GetTall() / 1.63 );
		panel["ArtifactSlot"..i].AcceptingItemType = CATEGORY_ARTIFACT;
		panel["ArtifactSlot"..i]:Receiver( "Items", function( Receiver, Dropped, bIsDropped )

			if( bIsDropped ) then
			
				local ToSlot = Receiver; -- spaghetti here
				local DraggedItem = Dropped[1];
				local ItemObj = DraggedItem.Item;
				local ItemMeta = GAMEMODE:GetMeta( ItemObj.Class );
				if( ToSlot.AcceptingItemType == ItemMeta.Type ) then

					netstream.Start( "ItemMove", ItemObj:GetID(), -1, -1, ItemObj:GetInventory():GetID(), true );
					ItemObj:CallFunction( "Equip" );
					netstream.Start( "ItemCallFunction", ItemObj:GetID(), "Equip" );
					panel:GetParent():RefreshInventory();
					
				end
			
			end
		
		end );
		
	end

end