GM.DummyItems = GM.DummyItems or {};

netstream.Hook( "LoadItems", function( s_ItemTable )

	for k,v in next, s_ItemTable do
	
		GAMEMODE.g_ItemTable[v.id] = nil;
	
		local s_Object = item( 
			LocalPlayer(), 
			v.InvID, 
			v.ItemClass, 
			v.x, 
			v.y, 
			v.id, 
			pon.decode( v.Vars )
		); -- need all before Initialize is called.
		
		s_Object:GetInventory().contents[#s_Object:GetInventory():GetContents() + 1] = s_Object;
	
	end

end );

netstream.Hook( "ReceiveItem", function( s_iInvID, s_szClass, s_iID, s_Vars, s_iX, s_iY )

	if( s_iX == 0 or s_iY == 0 ) then
	
		s_iX, s_iY = nil,nil;
		
	end
	
	if( GAMEMODE.g_ItemTable[s_iID] ) then
	
		GAMEMODE.g_ItemTable[s_iID] = nil;
		
	end
	
	local s_Object = item( LocalPlayer(), s_iInvID, s_szClass, s_iX, s_iY, s_iID, s_Vars );
	
	s_Object:GetInventory().contents[#s_Object:GetInventory():GetContents() + 1] = s_Object;
	
	if( GAMEMODE.Inventory ) then
	
		GAMEMODE.Inventory:RefreshInventory();
		
	end

end );

netstream.Hook( "ReceiveDummyItem", function( s_iID, s_szClass, s_Vars, s_Owner )

	GAMEMODE.DummyItems[s_iID] = {
	
		szClass = s_szClass,
		Vars = s_Vars,
		Owner = s_Owner,
		
	};

	hook.Run( "OnReceiveDummyItem", s_iID, GAMEMODE.DummyItems[s_iID] );
	
end );