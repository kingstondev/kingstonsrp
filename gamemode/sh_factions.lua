AddCSLuaFile();
GM.Factions = {};
GM.FactionIndices = {};

local s_tFactionFiles = file.Find( GM.FolderName.."/gamemode/factions/*.lua", "LUA", "namedesc" );
if( #s_tFactionFiles > 0 ) then
	
	for _, v in next, s_tFactionFiles do
		
		FACTION = {};
		FACTION.index = #GM.Factions + 1;
		FACTION.identifier = string.StripExtension( v );
		FACTION.Name = "Faction";
		FACTION.Desc = "Description";
		FACTION.Color = Color( 255, 255, 255 );
		FACTION.Models = {};
		
		-- todo, onfactiongiven, onspawn, onloadout

		if( SERVER ) then
			
			AddCSLuaFile( "factions/"..v );
			
		end
		
		include( "factions/"..v );
		
		team.SetUp( FACTION.index, FACTION.Name, FACTION.Color, false );
		
		for k, v in next, FACTION.Models do -- we probably dont need this.
		
			if( isstring( v ) ) then
			
				util.PrecacheModel( v )
				
			elseif( istable( v ) ) then
			
				util.PrecacheModel( v[1] )
				
			end
			
		end
		
		GM.FactionIndices[FACTION.index] = FACTION;
		GM.Factions[FACTION.identifier] = FACTION;
		
	end
	
end

local s_Meta = FindMetaTable( "Player" );
function s_Meta:GetFaction()

	local data = self:GetCharDataByID( self:CharID() );
	return data.Faction or "loner";

end