AddCSLuaFile();
local s_Meta = FindMetaTable( "Player" )
GM.MetaItems = GM.MetaItems or {};
GM.MetaBases = GM.MetaBases or {};
GM.Upgrades = GM.Upgrades or {};

local s_tItemFiles = file.Find( GM.FolderName.."/gamemode/items/*.lua", "LUA", "namedesc" );
local s_tBaseFiles = file.Find( GM.FolderName.."/gamemode/items/base/*.lua", "LUA", "namedesc" );
local s_tUpgradeFiles = file.Find( GM.FolderName.."/gamemode/items/upgrades/*.lua", "LUA", "namedesc" );

if( #s_tBaseFiles > 0 ) then

	for _, v in next, s_tBaseFiles do
	
		if( GM.MetaBases[string.StripExtension( v )] ) then return end
	
		BASE = {};
		
		if( SERVER ) then
		
			AddCSLuaFile( "items/base/"..v );
		
		end
		
		include( "items/base/"..v );
		
		GM.MetaBases[string.StripExtension( v )] = table.Copy( BASE );
		
	end
	
end

/* 
	While these aren't technically objects, you can still interact with them like objects.
	They are not objects because we do not need class inherentence, or any separate instances.
	These tables purely hold information to retrieve from when needed.
	
	The specific reason the system is designed this way is to allow any item to be upgraded
	without needing to construct strict tree rules. You can create any kind of upgrade tree
	using the tables RequiredItems, RequiredUpgrades, and Incompatible. Useful for technicians.
	
	Default functions:
		CanUpgrade( item )
		OnUpgrade( item )
		
	Default members:
		Name
		Desc
		IconX
		IconY
		RequiredItems
		Incompatible
		
	Future members:
		RequiredUpgrades
		RequiredFlag

	Keep in mind, these default functions are members of the UPGRADE table itself, and if you do not call
	them as if the UPGRADE table was an object, you will need to pass the upgrade table as the first argument.
	e.g.
	
	UPGRADE.CanUpgrade( UPGRADE, item )
*/

if( #s_tUpgradeFiles > 0 ) then

	for _, v in next, s_tUpgradeFiles do
	
		if( GM.Upgrades[string.StripExtension( v )] ) then return end
	
		UPGRADE = {};
		UPGRADE.ClassName = string.StripExtension( v );
		
		if( SERVER ) then
		
			AddCSLuaFile( "items/upgrades/"..v );
		
		end
		
		include( "items/upgrades/"..v );
		
		GM.Upgrades[string.StripExtension( v )] = table.Copy( UPGRADE );
		
	end
	
end

if( #s_tItemFiles > 0 ) then
	
	for _, v in next, s_tItemFiles do
	
		-- this prevents autorefresh from affecting items.
		if( GM.MetaItems[string.StripExtension( v )] ) then return end
	
		local s_BaseItem = {};
		ITEM = {};
		
		ITEM.Name = "Item";
		ITEM.Desc = "Description";
		ITEM.Model = "models/error.mdl";
		ITEM.W = 1;
		ITEM.H = 1;

		if( SERVER ) then
			
			AddCSLuaFile( "items/"..v );
			
		end
		
		include( "items/"..v );
		
		ITEM.Class = string.StripExtension( v );
		
		if( ITEM.Base ) then
			
			/*
				Explanation for use of Inherit:
				Copies any missing data from base to target, and sets the target's BaseClass member to the base table's pointer.
				
				We don't need a reference to base class table because we can retrieve the item base via accessing the value
				ITEM.Base
				Since ITEM tables are associative arrays, we do not need to shift all indices down
				That is why we do not use table.Remove.
			*/
			
			s_BaseItem = table.Copy( GM.MetaBases[ITEM.Base] );
			table.Inherit( ITEM, s_BaseItem );
			ITEM.BaseClass = nil;
			
		end
		
		GM.MetaItems[string.StripExtension( v )] = ITEM;
		
	end
	
end

function GM:GetMeta( szItem )

	if( isstring( szItem ) ) then
	
		-- without table.Copy, returns a reference. Weird thing happens when you return the reference.
		return table.Copy( GAMEMODE.MetaItems[szItem] );
		
	end

end

function s_Meta:FindItemByID( nID )
	
	return GAMEMODE.g_ItemTable[nID];

end

function s_Meta:GetItemsOfType( t )
	
	local keys = { };
	
	for k, v in next, self:GetInventories() do
	
		for m,n in next, v:GetContents() do
		
			if( n.Class == t ) then
				
				table.insert( keys, n );
				
			end
			
		end
		
	end
	
	return keys;
	
end

function GM:WeaponReloaded( weapon )

	local ply = weapon.Owner;
	
	if( weapon:GetNW2Int( "TimesFired", 0 ) > 0 ) then

		local item = ply:FindItemByID( weapon:GetNW2Int( "LastMagazineID", -1 ) );
		if( item ) then

			item:SetVar( "Rounds", item:GetVar( "Rounds", 0 ) - weapon:GetNW2Int( "TimesFired", 0 ) );
		
		end
	
	end

	weapon:SetNW2Int( "TimesFired", 0 );
	
end

-- return false to prevent pickup
function GM:CanPickup( ply, item, ent )

	return true;

end