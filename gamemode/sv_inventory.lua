local s_Meta = FindMetaTable( "Player" );

function s_Meta:NewInventory( w, h, name ) -- only use when you want to create a completely new inventory in the db.

	local object = inventory( self, w, h, name );
	local function cb()
		
		netstream.Start( self, "NewInventory", w, h, object:GetID(), self:CharID(), 0, name, false );
		
	end
	object:SaveNewObject( cb );

	return object;
	
end