netstream.Hook( "UnhideArtifact", function( ply, artifact )

	if( !IsValid( artifact ) ) then return end
	
	local artifactmeta = GAMEMODE:GetMeta( artifact:GetItemClass() );
	-- bad loop that makes sure that 1) player has detector active and 2) detector can detect artifact
	for k,v in next, GAMEMODE.g_ItemTable do
	
		if( v:Owner() == ply ) then
		
			if( v.Base == "detector" ) then
			
				if( v:GetVar( "Equipped", false ) ) then
				
					local detectormeta = GAMEMODE:GetMeta( v:GetClass() );
					if( detectormeta.DetectorType >= artifactmeta.Tier ) then
					
						break;
						
					end
				
				end
				
			end
		
		end
		
		if( #GAMEMODE.g_ItemTable == k ) then return end;
	
	end
	
	local pPos = ply:GetPos();
	local ePos = artifact:GetPos();
	if( pPos:Distance( ePos ) < 128 ) then
	
		artifact:SetNoDraw( false );
		
	else
	
		artifact:SetNoDraw( true );
		
	end

end );