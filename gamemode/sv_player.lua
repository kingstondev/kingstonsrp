local s_Meta = FindMetaTable( "Player" );

GM.g_PlayerTable = {};

function GM:PlayerInitialSpawn( ply )

	self:PlayerLoadout( ply );
	
end

function GM:PlayerSpawn( ply )

	if( ply:IsBot() ) then
	
		ply:SetRPName( "Mysterious Person" );
		ply:SetOverhead1( "A mysterious person that seems to have appeared from the Zone itself." );
		ply:SetOverhead2( "It's a bot." );
		return;
	
	end

	ply:SetDuckSpeed( 0.3 );
	ply:SetUnDuckSpeed( 0.3 );
	
	ply:SetupHands()
	
	self:SetPlayerSpeed( ply, 100, 200 );

	if( ply:GetWeight() > ply:GetMaxWeight() + 10 ) then -- remove const create func
	
		ply:SetWalkSpeed( 1 );
		ply:SetRunSpeed( 1 );
		
	elseif( ply:GetWeight() > ply:GetMaxWeight() ) then
	
		ply:SetRunSpeed( ply:GetWalkSpeed() );
	
	end
	
	ply:GodDisable();
	ply:SetNoTarget( false );
	ply:SetNoDraw( false );
	ply:SetNotSolid( false );

	if( !ply.InitialSafeSpawn ) then
		
		ply.InitialSafeSpawn = true;
		self:PlayerInitialSpawnSafe( ply );
		
	end
	
	if( ply:CharID() > -1 ) then

		hook.Run( "PlayerLoadout", ply );
		hook.Run( "CharacterLoaded", ply, ply:GetCharDataByID( ply:CharID() ) );
		hook.Run( "HandleFactionSpawn", ply );
		
		ply:SetTeam( GAMEMODE.Factions[ply:GetCharDataByID( ply:CharID() ).Faction].index ); -- refactor!!
		ply:SetModel( ply:GetCharDataByID( ply:CharID() ).Model );
		
	end

end

function GM:PlayerDeath( victim, inflictor, attacker )

end

function GM:PlayerInitialSpawnSafe( ply )
	
	if( ply:IsBot() ) then return end
	
	ply:SetNotSolid( true );
	ply:SetMoveType( MOVETYPE_NOCLIP );
	ply:SetNoDraw( true );
	ply:SetPos( Vector( 0, 0, 10000 ) );
	
	ply:SuppressHint( "OpeningMenu" );
	ply:SuppressHint( "Annoy1" );
	ply:SuppressHint( "Annoy2" );
	ply:SuppressHint( "OpeningContext" );
	ply:SuppressHint( "ContextClick" );
	
end

function s_Meta:PostLoadCharacter()
	
	self:Freeze( false );
	
	self:SetNotSolid( false );
	self:SetMoveType( MOVETYPE_WALK );
	self:SetNoDraw( false );
	
	self:Spawn();
	
end

function GM:CharacterLoaded( ply, data ) -- you can add hooks or override.

end

function GM:PlayerSpray( ply )

	return true

end

function GM:PlayerSwitchFlashlight( ply, on )
	
	if( !ply.LastFlashlight ) then ply.LastFlashlight = 0; end
	
	if( CurTime() - ply.LastFlashlight > 0.2 ) then
		
		ply.LastFlashlight = CurTime();
		return true;
		
	end
	
end

function GM:PlayerLoadout( ply )

	if( !IsValid( ply ) ) then return end

	if( ply.RemoveAllAmmo ) then

		ply:RemoveAllAmmo();
		
	end
	
	if( ply.StripWeapons ) then
	
		ply:StripWeapons();
		
	end
	
	ply:Give( "kingston_hands" );
	
	if( ply:PhysTrust() or ply:IsAdmin() ) then
		
		ply:Give( "weapon_physgun" );
		
	end
	
	if( ply:ToolTrust() or ply:IsAdmin() ) then
		
		ply:Give( "gmod_tool" );
		
	end
	
	if( ply.CharID and ply:CharID() > -1 ) then
	
		hook.Run( "HandleFactionLoadout", ply );
		
	end
	
	for k,v in next, GAMEMODE.g_ItemTable do

		if( v:Owner() == ply ) then

			if( v.Base == "weapon" and v:GetVar( "Equipped", false ) ) then

				local metaitem = self:GetMeta( v:GetClass() );
				metaitem.functions.Equip.OnUse( v );
			
			end
		
		end
		
	end

end

netstream.Hook( "nRetrieveClientData", function( ply )

	ply:RetrievePlayer( ply:SteamID() );
	ply:RetrieveCharacters( ply:SteamID() );

end );

function s_Meta:LoadPlayerData( data )

	self:SetPlayerFlags( data.PlayerFlags or "" );
	self:SetPhysTrust( tobool( data.PhysTrust ) );
	self:SetPropTrust( tobool( data.PropTrust ) );
	self:SetToolTrust( tobool( data.ToolTrust ) );
	self:SetUserGroup( data.Rank );
	
end

function s_Meta:UpdatePlayerField( szField, szValue )

	local PlayerUpdateQuery = UPDATE_PLAYER_BY_FIELD;
	function PlayerUpdateQuery:onSuccess()
	
		printf( COLOR_WHITE, "Successfully updated player field %s with value %s.", szField, szValue );
		
	end
	
	PlayerUpdateQuery:setString( 1, szField );
	PlayerUpdateQuery:setString( 2, szValue );
	PlayerUpdateQuery:setString( 3, self:SteamID() );
	PlayerUpdateQuery:start();

end

local function SetTyping( ply, f )
	
	if( f < 0 or f > 2 ) then return end
	
	ply:SetTyping( f );
	
end
netstream.Hook( "SetTyping", SetTyping );

local function SetRaised( ply )

	if( ply:GetActiveWeapon() != NULL ) then
		
		ply:SetRaised( !ply:Raised() );
		
	end

end
netstream.Hook( "nToggleHolster", SetRaised );

function GM:PlayerSay( ply, szText, bTeamChat )

	return "";

end