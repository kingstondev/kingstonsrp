AddCSLuaFile(); 

-- system is now obsolete, see sh_character

local s_Accessors = { };
s_Accessors["Player"] = {
	{ "CharID", "Int", -1 },
	{ "RPName", "String", "..." },
	{ "Overhead1", "String", "..." },
	{ "Overhead2", "String", "..." },
	{ "Description", "String", "..." },
	{ "Typing", "Int", 0 },
	{ "CharFlags", "String", "" },
	{ "PlayerFlags", "String", "" },
	{ "Head", "String", "" },
	{ "Body", "String", "" },
	{ "SkinColor", "Vector", Vector( 1, 1, 1 ) },
	{ "PhysTrust", "Bool", true },
	{ "PropTrust", "Bool", false },
	{ "ToolTrust", "Bool", false },
	{ "Raised", "Bool", true },
};

for k, v in next, s_Accessors do
	
	local s_Meta = FindMetaTable( k );
	
	for m, n in next, v do
		
		local s_szName = n[1];
		local s_szType = n[2];
		local s_Default = n[3];
		local s_bPrivate = n[4] or false;
		
		s_Meta[s_szName] = function( self )
			
			if( s_szType != "String" ) then
				
				return self["GetNW2"..s_szType]( self, s_szName, s_Default );
				
			else
				
				if( self["_accessor_"..s_szName] != nil ) then
				
					return self["_accessor_"..s_szName];
					
				else
				
					return s_Default;
					
				end
				
			end
			
		end
		
		s_Meta["Set" .. s_szName] = function( self, Value, bShared )

			if( s_szType != "String" ) then
			
				self["SetNW2"..s_szType]( self, s_szName, Value );
				self:SetNWVarProxy( s_szName, function( ent, szName, OldValue, NewValue )
				
					hook.Run( "OnAccessorChanged", ent, szName, NewValue, OldValue );
				
				end );
				
			else
			
				self["_accessor_"..s_szName] = Value;
				hook.Run( "OnAccessorChanged", self, s_szName, Value );
				
				if( SERVER and !bShared ) then
					
					if( s_bPrivate ) then
						
						if( self:IsBot() ) then return end
						
						netstream.Start( self, "nSetAccessorP" .. s_szName, Value );
						
					else
						
						if( self:IsBot() ) then print("attempting transmission") end
						
						netstream.Start( nil, "nSetAccessor" .. s_szName, self, Value );
						
					end
					
				end
				
			end
			
		end
			
		if( CLIENT ) then
			
			local function nSetAccessorP( s_Value )
				
				LocalPlayer()["Set" .. s_szName]( LocalPlayer(), s_Value );
				
			end
			netstream.Hook( "nSetAccessorP" .. s_szName, nSetAccessorP );
			
			local function nSetAccessor( ply, s_Value )
			
				if( ply and ply:IsValid() ) then

					ply["Set" .. s_szName]( ply, s_Value );
				
				end
				
			end
			netstream.Hook( "nSetAccessor" .. s_szName, nSetAccessor );
			
		end
		
	end
	
end

local s_Meta = FindMetaTable( "Player" );

if( SERVER ) then
	
	function s_Meta:SyncPlayer( ply )
		
		for _, n in next, s_Accessors["Player"] do
			
			if( !n[4] and n[2] == "String" ) then
				
				netstream.Start( self, "nSetAccessor"..n[1], ply, ply[n[1]]( ply ) );
				
			end
			
		end
		
		for key,n in next, GAMEMODE.CharacterVars do
		
			local szProperName = key:sub( 1, 1 ):upper()..key:sub( 2 );
			if( !n["private"] ) then
			
				netstream.Start( self, "nSetAccessor"..szProperName, ply, ply["Get"..szProperName]( ply ) );
				
			end
		
		end
		
	end
	
	function s_Meta:SyncOtherPlayers()
		
		for _, v in next, player.GetAll() do
			
			if( v != self ) then
				
				self:SyncPlayer( v );
				
			end
			
		end
		
	end
	
	local function nRequestPlayerData( ply )
		
		ply:SyncOtherPlayers();
		
	end
	netstream.Hook( "nRequestPlayerData", nRequestPlayerData );
	
end

function GM:OnAccessorChanged( entity, szName, szOldValue, szNewValue )

end