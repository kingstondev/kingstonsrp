AddCSLuaFile();

local s_Meta = FindMetaTable( "Player" );

if( SERVER ) then

	function s_Meta:CharacterSetFlags( flags )

		if( self:CharID() == -1 ) then return end;
		
		self:SetCharFlags( flags );
		
		local function onSuccess()
		
			printf( COLOR_GREEN, "Character flags successfully saved." );
		
		end
		wzsql.Query( Format( "UPDATE Characters SET CharFlags = '%s' WHERE id = '%d'", self:CharFlags(), self:CharID() ), onSuccess );

	end
	
	function s_Meta:CharacterGiveFlag( flag )

		if( self:CharID() == -1 ) then return end;
		if( flag == "" or !flag ) then return end;
		
		self:CharacterSetFlags( self:CharFlags()..flag );

	end

	function s_Meta:CharacterTakeFlag( flag )

		if( self:CharID() == -1 ) then return end;
		if( flag == "" or !flag ) then return end;
		
		self:CharacterSetFlags( string.gsub( self:CharFlags(), flag, "" ) );

	end
	
end

function s_Meta:CharacterGetFlags()

	if( self:CharID() == -1 ) then return end;
	
	return self:CharFlags();

end

function s_Meta:CharacterHasFlag( flag )

	if( self:CharID() == -1 ) then return end;
	if( flag == "" ) then return end;
	
	if( flag == "T" ) then
	
		return true // PURE DEV
	
	end
	
	return ( string.find( self:CharFlags(), flag ) ~= nil );

end

if( SERVER ) then

	function s_Meta:PlayerSetFlags( flags )

		self:SetPlayerFlags( flags );
		
		local function onSuccess()
		
			printf( COLOR_GREEN, "Player flags successfully saved." );
		
		end
		wzsql.Query( Format( "UPDATE Players SET PlayerFlags = '%s' WHERE SteamID = '%s'", self:PlayerFlags(), self:SteamID() ), onSuccess );

	end

	function s_Meta:PlayerGiveFlag( flag )
	
		if( flag == "" or !flag ) then return end;
		
		self:PlayerSetFlags( self:PlayerFlags()..flag );

	end

	function s_Meta:PlayerTakeFlag( flag )
	
		if( flag == "" or !flag ) then return end;
		if( !self:PlayerHasFlag( flag ) ) then return end;
		
		self:PlayerSetFlags( string.gsub( self:PlayerFlags(), flag, "" ) );

	end
	
end

function s_Meta:PlayerGetFlags()

	return self:PlayerFlags();

end

function s_Meta:PlayerHasFlag( flag )

	return ( string.find( self:CharFlags(), flag ) ~= nil );

end