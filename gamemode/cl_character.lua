netstream.Hook( "LoadIndividualCharacter", function( s_Table )

	if( !LocalPlayer():IsValid() ) then return end
	
	GAMEMODE.g_CharacterTable[LocalPlayer():SteamID()] = s_Table;

end );

netstream.Hook( "LoadCharacters", function( s_Table )

	GAMEMODE.g_CharacterTable[LocalPlayer():SteamID()] = s_Table;
	
	if( LocalPlayer():CharID() <= -1 ) then
	
		vgui.Create( "MainMenuVGUI" );
		
	end

end );

netstream.Hook( "UnloadCharacter", function()

	GAMEMODE.g_InventoryTable = {};
	GAMEMODE.g_ItemTable = {};

end );