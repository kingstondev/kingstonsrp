AddCSLuaFile();

GM.Ranks = GM.Ranks or {};
GM.Commands = GM.Commands or {};

function concommand.AddAdmin( szCmd, cb, canRun )
	
	local function f( ply, szCmd, args )
	
		if( !canRun ) then
		
			if( ply:HasPermission( szCmd ) ) then
				
				cb( ply, szCmd, args );
				
			else
			
				ply:Notify( COLOR_RED, "You do not have permission to run %s!", szCmd );
				
			end
			
		else
		
			if( canRun( ply, szCmd, args ) ) then
			
				cb( ply, szCmd, args );
			
			else
			
				ply:Notify( COLOR_RED, "You do not have permission to run %s!", szCmd );
			
			end
			
		end
		
	end
	concommand.Add( szCmd, f );
	
end

function GM:PlayerNoClip( ply )

	if( !ply:HasPermission( "rpa_noclip" ) ) then
	
		return false;
		
	end
	
	if( SERVER ) then
		
		if( ply:IsEFlagSet( EFL_NOCLIP_ACTIVE ) ) then
			
			ply:GodDisable();
			ply:SetNoTarget( false );
			ply:SetNoDraw( false );
			ply:SetNotSolid( false );
			
			if( ply:GetActiveWeapon() != NULL ) then
				
				ply:GetActiveWeapon():SetNoDraw( false );
				ply:GetActiveWeapon():SetColor( Color( 255, 255, 255, 255 ) );
				
			end
			
		else
			
			ply:GodEnable();
			ply:SetNoTarget( true );
			ply:SetNoDraw( true );
			ply:SetNotSolid( true );
			
			if( ply:GetActiveWeapon() != NULL ) then
				
				ply:GetActiveWeapon():SetNoDraw( true );
				ply:GetActiveWeapon():SetColor( Color( 255, 255, 255, 0 ) );
				
			end
			
		end
		
	end
	
	return true;
	
end

local s_Meta = FindMetaTable( "Player" )

function s_Meta:FindPlayer( szStr )
	
	if( szStr == "^" ) then return self end
	if( szStr == "*" ) then
		
		local tr = self:GetEyeTrace();
		
		if( tr.Entity and tr.Entity:IsValid() and tr.Entity:IsPlayer() ) then
			
			return tr.Entity;
			
		end
		
	end
	
	for _, v in next, player.GetAll() do
		
		if( v:SteamID() == szStr ) then
			
			return v;
			
		end
		
	end
	
	for _, v in next, player.GetAll() do
		
		if( string.find( string.lower( v:RPName() ), string.lower( szStr ) ) ) then
			
			return v;
			
		elseif( string.find( string.lower( v:Nick() ), string.lower( szStr ) ) ) then
			
			return v;
			
		end
		
	end
	
end

function s_Meta:GetFormattedName()

	if( self:CharID() > -1 ) then
	
		return Format( "%s (%s)", self:Nick(), self:RPName() );
		
	else
	
		return self:Nick();
		
	end

end

function s_Meta:HasPermission( szCmd )
	
	return ( ( self:GetPermissions()[szCmd] or 1 ) != 1 )

end

function s_Meta:GetPermissions()

	if( GAMEMODE.Ranks[self:GetUserGroup()] ) then

		return GAMEMODE.Ranks[self:GetUserGroup()].Permissions;
		
	else
	
		return {};
		
	end

end