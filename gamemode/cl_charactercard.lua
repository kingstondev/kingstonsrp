function GM:ShowCharacterCard( ply )

	if( !GAMEMODE.CharacterCard ) then
	
		vgui.Create( "CCharacterCard" );
		GAMEMODE.CharacterCard:SetPlayer( ply );
		
	else
	
		GAMEMODE.CharacterCard:Close();
		
	end

end

netstream.Hook( "OpenCharacterCard", function( ent )

	GAMEMODE:ShowCharacterCard( ent );

end );