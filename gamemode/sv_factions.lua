GM.FactionSpawns = GM.FactionSpawns or {};
function GM:SaveFactionSpawns()

	local szJSON = util.TableToJSON( self.FactionSpawns );
	
	file.Write( "stalker/factionspawns.txt", szJSON );

end

function GM:LoadFactionSpawns()

	local szJSON = file.Read( "stalker/factionspawns.txt", "DATA" );
	
	if( szJSON and #szJSON > 0 ) then
	
		local tbl = util.JSONToTable( szJSON );
		self.FactionSpawns = tbl;
		
	end

end

function GM:HandleFactionSpawn( ply )

	local data = ply:GetCharDataByID( ply:CharID() );
	local faction = self.Factions[data.Faction];
	
	if( faction.OnSpawn ) then
	
		faction.OnSpawn( ply );
		
	end
	
	if( faction.UsesFactionSpawn ) then
		
		local Spawns = self.FactionSpawns[game.GetMap()];
		if( !Spawns ) then return end
		local nSpawnPos = Spawns[data.Faction];
		
		if( nSpawnPos and isvector( nSpawnPos ) ) then
		
			ply:SetPos( nSpawnPos );
			
		end
		
	end

end

function GM:HandleFactionLoadout( ply )

	local data = ply:GetCharDataByID( ply:CharID() );
	local faction = self.Factions[data.Faction or "loner"];

	if( faction.OnLoadout ) then
	
		faction.OnLoadout( ply );
		
	end
	
end

function GM:SetFactionSpawnPos( szFaction, nVector, bNoSave )

	local faction = self.Factions[szFaction];
	
	if( faction ) then
	
		if( !self.FactionSpawns[game.GetMap()] ) then
		
			self.FactionSpawns[game.GetMap()] = {};
			
		end

		self.FactionSpawns[game.GetMap()][szFaction] = nVector;
		
		if( !bNoSave ) then
	
			self:SaveFactionSpawns();
			
		end
		
	end
	
end