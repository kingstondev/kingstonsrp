function GM:HUDShouldDraw( szName )
	
	if( szName == "CHudWeaponSelection" ) then return false end
	if( szName == "CHudAmmo" ) then return false end
	if( szName == "CHudAmmoSecondary" ) then return false end
	if( szName == "CHudSecondaryAmmo" ) then return false end
	if( szName == "CHudHealth" ) then return false end
	if( szName == "CHudBattery" ) then return false end
	if( szName == "CHudChat" ) then return false end
	if( szName == "CHudDamageIndicator" ) then return false end
	if( szName == "CHudCrosshair" ) then 
	
		if( !self.ShowCrosshair[LocalPlayer():GetActiveWeapon():GetClass()] ) then return false end

	end
	if( szName == "CHudMenu" and !UseDefaultMainMenu ) then return false end
	
	return true;
	
end

function GM:PreRender()

--[[ 	if ( input.IsKeyDown( KEY_ESCAPE ) and gui.IsGameUIVisible() ) then
	
		gui.HideGameUI()
		
		if ( IsValid( self.EscapeMenu ) ) then
		
			self.EscapeMenu:Close()
			
		else
		
			self.EscapeMenu = vgui.Create( "EscapeMenuVGUI" )
			
		end
		
		return true
		
	end ]]

end

local matBlurScreen = Material( "pp/blurscreen" );

function draw.DrawBackgroundBlur( frac ) -- from disseminate
	
	if( frac == 0 ) then return end

	surface.SetMaterial( matBlurScreen );
	surface.SetDrawColor( 255, 255, 255, 255 );
	
	for i = 1, 3 do
		
		matBlurScreen:SetFloat( "$blur", frac * 5 * ( i / 3 ) );
		matBlurScreen:Recompute();
		render.UpdateScreenEffectTexture();
		surface.DrawTexturedRect( 0, 0, ScrW(), ScrH() );
		
	end

end

function GM:HUDPaint()

	if( LocalPlayer():CharID() <= -1 ) then
	
		surface.SetDrawColor( 0, 0, 0, 255 );
		surface.DrawRect( 0, 0, ScrW(), ScrH() );
		
		if( #GAMEMODE.g_CharacterTable == 0 ) then
		
			surface.SetFont( "Stalker2-MainMenuFont-32" );
			local w,h = surface.GetTextSize( "Loading..." );
			
			draw.DrawText( "Loading...", "Stalker2-MainMenuFont-32", ScrW() / 2 - w / 2, ScrH() / 2 - h / 2, Color( 255, 255, 255, 255 ) );
			
		end
		
		return;
		
	end
	
	self:HUDPaintPlayer(); -- convert to hooks maybe
	self:HUDPaintChat();
	self:HUDPaintDisplay();
	magazineselect.draw();

end

local matHints = Material( "kingston/hint_panels" );

function GM:HUDPaintDisplay()

	surface.SetMaterial( matHints );
	surface.SetDrawColor( 255, 255, 255, 255 );
	surface.DrawTexturedRectUV( ScrW() - ( ScrW() / 7 ), ScrH() - ( ScrH() / 7 ), ScrW() / 8, ScrH() / 8, 0.843, 0.04, 0.98, 0.137 );

	if( !self.HPDraw ) then self.HPDraw = 100 end
	if( !self.ARDraw ) then self.ARDraw = 0 end
	if( !self.HGDraw ) then self.HGDraw = 0 end
	
	local hp = LocalPlayer():Health();
	local ar = LocalPlayer():Armor();
	
	if( self.HPDraw > hp ) then
		
		self.HPDraw = self.HPDraw - 0.5;
		
	elseif( self.HPDraw < hp ) then
		
		self.HPDraw = self.HPDraw + 0.5;
		
	end
	
	if( math.abs( self.HPDraw - hp ) < 1 ) then
		
		self.HPDraw = hp;
		
	end
	
	if( self.ARDraw > ar ) then
		
		self.ARDraw = self.ARDraw - 0.5;
		
	elseif( self.ARDraw < ar ) then
		
		self.ARDraw = self.ARDraw + 0.5;
		
	end
	
	if( math.abs( self.ARDraw - ar ) < 1 ) then
		
		self.ARDraw = ar;
		
	end
	
	self.HPDraw = math.Clamp( self.HPDraw, 0, 100 );
	self.ARDraw = math.Clamp( self.ARDraw, 0, 100 );
	
	local w = 220;
	
	local y = ScrH() - 24;
	
	if( self.HPDraw > 0 ) then
	
		local u0 = ScrW() - ( ScrW() / 7.38 );
		local v0 = ScrH() - ( ScrH() / 7.5 );
		local u1 = ScrW() - ( ScrW() / 7.38 ) + ( ScrW() / 9.3 ) * ( self.HPDraw / 100 );
		local v1 = ScrH() - ( ScrH() / 7.5 ) + 8;
	
		surface.SetMaterial( matHints );
		surface.SetDrawColor( 255, 255, 255, 255 );
		render.SetScissorRect( u0, v0, u1, v1, true );
			surface.DrawTexturedRectUV( u0, v0, ScrW() / 9.3, 8, 0.764, 0.151, 0.857, 0.156 );
		render.SetScissorRect( 0, 0, 0, 0, false );
		
	end
	
	if( self.ARDraw > 0 ) then
		
		draw.RoundedBox( 0, 20, y, w, 14, Color( 30, 30, 30, 200 ) );
		draw.RoundedBox( 0, 22, y + 2, ( w - 4 ) * ( math.Clamp( self.ARDraw, 1, 100 ) / 100 ), 10, Color( 37, 84, 158, 255 ) );
		
		if( LocalPlayer():Armor() > 100 ) then
			
			draw.RoundedBox( 0, 22, y + 2, ( w - 4 ) * ( math.Clamp( self.ARDraw - 100, 1, 100 ) / 100 ), 10, Color( 240, 240, 240, 255 ) );
			
		end
		
		y = y - 16;
		
	end

end

function GM:HUDPaintPlayer()

	for _, v in next, player.GetAll() do
		
		if( v != LocalPlayer() ) then
			
			if( !v.HUDA ) then v.HUDA = 0 end
			
			local pos = v:EyePos();
			local ts = ( pos + Vector( 0, 0, 8 ) ):ToScreen();
			
			if( pos:Distance( LocalPlayer():EyePos() ) < 768 and
				LocalPlayer():CanSeePlayer( v ) and
				LocalPlayer():GetEyeTraceNoCursor().Entity == v and
				!v:GetNoDraw() and
				v:Alive() ) then
				
				v.HUDA = math.Approach( v.HUDA, 1, FrameTime() );
				
			else
				
				v.HUDA = math.Approach( v.HUDA, 0, FrameTime() );
				
			end
			
			if( v.HUDA > 0 ) then
				
				draw.DrawText( v:RPName(), "Stalker2-24", ts.x, ts.y, Color( 255, 255, 255, 255 * v.HUDA ), 1 );
				ts.y = ts.y + 24;
				draw.DrawText( v:Overhead1(), "Stalker2-16", ts.x, ts.y, Color( 255, 255, 255, 255 * v.HUDA ), 1 );
				ts.y = ts.y + 16;
				draw.DrawText( v:Overhead2(), "Stalker2-16", ts.x, ts.y, Color( 255, 255, 255, 255 * v.HUDA ), 1 );
				
				if( v:Typing() > 0 ) then
				
					ts.y = ts.y - 48;
					draw.DrawText( "Typing...", "Rundschrift-16", ts.x, ts.y, Color( 255, 255, 255, 255 * v.HUDA ), 1 );
					
				end
				
			end
			
		end
		
	end

end

--[[ function GM:PostDrawTranslucentRenderables() -- for debugging sweps

	render.DrawLine( ply:GetEyeTraceNoCursor().HitPos, ply:EyePos() + Vector( 1, 0, 0 ), Color( 255, 0, 0, 255 ), true );

end ]]

local tooltipBg = Material( "kingston/ui_actor_menu_tooltip.png" );

function GM:PaintItemTip( panel, item )

	if( panel:IsHovered() ) then
	
		if( !panel.NoHover ) then

			if( RealTime() >= panel.HoverStart + 1 ) then
				-- this func might get really big and messy
				local x,y = panel:ScreenToLocal( gui.MouseX() + 15, gui.MouseY() + 15 );
				
				if( ( gui.MouseY() + 15 ) + ( ScrH() / 2.67 ) > ScrH() ) then -- panel is out of bounds

					x,y = panel:ScreenToLocal( gui.MouseX() + 15, ( gui.MouseY() + 15 ) - ( ( ( gui.MouseY() + 15 ) + ( ScrH() / 2.67 ) ) - ScrH() ) );
					
				end
			
				surface.DisableClipping( true );
				
				surface.SetDrawColor( Color( 255, 255, 255, 255 ) );
				surface.SetMaterial( tooltipBg );
				surface.DrawTexturedRect( x, y, ScrW() / 6, ScrH() / 2.67 );
				
				surface.SetTextColor( Color( 240, 240, 240, 196 ) );
				surface.SetFont( "Rundschrift-16-B" );
				local tW,tH = surface.GetTextSize( item:GetName() );
				surface.SetTextPos( x + ( ( ScrW() / 6 ) / 2 ) - ( tW / 2 ) - 4, y + 25 );
				surface.DrawText( item:GetName() );
				
				surface.SetTextColor( Color( 220, 220, 220, 172 ) );
				surface.SetFont( "Rundschrift-16-B" );
				surface.SetTextPos( x + 24, y + 48 );
				surface.DrawText( item:GetWeight().." kg" );
				
				surface.SetTextColor( Color( 180, 180, 180, 148 ) );
				surface.SetFont( "Rundschrift-16-B" );
				local lines, maxW = util.wrapText(item:GetDesc(), ScrW() / 9)
				for k,v in next, lines do
				
					surface.SetTextPos( x + 18, y + 56 + (16*k) );
					surface.DrawText( v );
					
				end
				
				surface.DisableClipping( false );
				
			end
			
		end
		
	end

end