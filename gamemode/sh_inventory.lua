AddCSLuaFile();

local s_Meta = FindMetaTable( "Entity" );
GM.g_InventoryTable = GM.g_InventoryTable or {};

-- first result. instead of GetInventories we use func arg for FindByID? Bool for all inventories?
function s_Meta:GetInventory()

	for k,v in next, GAMEMODE.g_InventoryTable do
	
		if( self:IsPlayer() ) then

			if( v.CharID == self:CharID() and !v.Disabled ) then
			
				return v;
				
			end
			
		else
		
			if( v.owner == self ) then
			
				return v;
				
			end
			
		end
	
	end

end

function s_Meta:GetInventories() -- all results

	local ret = {};
	for k,v in next, GAMEMODE.g_InventoryTable do
	
		if( self:IsPlayer() ) then
	
			if( v.CharID == self:CharID() and !v.Disabled ) then
			
				ret[k] = v;
			
			end
			
		else
		
			if( v.owner == self ) then
			
				ret[k] = v;
				
			end
			
		end
		
	end
	
	return ret;

end

function s_Meta:FindInventoryByID( nID ) -- truth be told, we should know the table structure.

	for k,v in next, GAMEMODE.g_InventoryTable do
	
		if( v.id == nID and !v.Disabled ) then
		
			return v;
			
		end
	
	end

end

function s_Meta:GetWeight()

	local nWeight = 0;
	for k,v in next, GAMEMODE.g_InventoryTable do
	
		if( v.owner == self ) then
		
			for m,n in next, v:GetContents() do
				
				nWeight = nWeight + n:GetWeight();
			
			end
			
		end
		
	end
	
	return nWeight;

end